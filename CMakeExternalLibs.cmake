# External libs include prebuild libs and integrated libs

# ------------------------------------------------------------------------------
# Find Qt4
# ------------------------------------------------------------------------------
IF ( MSVC )
	#We need QtMain to use 'WIN32' mode (/subsystem:windows) with MSVC
	FIND_PACKAGE( Qt4 ${QT_VERSION} COMPONENTS QtMain QtCore QtGui QtOpenGL QtXml QtXmlPatterns REQUIRED )
ELSE()
	FIND_PACKAGE( Qt4 ${QT_VERSION} COMPONENTS QtCore QtGui QtOpenGL QtXml QtXmlPatterns QtHelp REQUIRED )
ENDIF()
IF( NOT QT_FOUND )
	MESSAGE( SEND_ERROR "Qt required, but not found with 'find_package()'" )
ELSE()
	INCLUDE( ${QT_USE_FILE} )
	LIST( APPEND EXTERNAL_LIBS_INCLUDE_DIR ${QT_INCLUDE_DIR} )
	LIST( APPEND EXTERNAL_LIBS_LIBRARIES ${QT_LIBRARIES} )
ENDIF()

# ------------------------------------------------------------------------------
# Find OpenGL
# ------------------------------------------------------------------------------
FIND_PACKAGE( OpenGL REQUIRED )
IF( NOT OPENGL_FOUND )
    message( SEND_ERROR "OpenGL required, but not found with 'find_package()'" )
ELSE()
	LIST( APPEND EXTERNAL_LIBS_INCLUDE_DIR  ${OPENGL_INCLUDE_DIR} )
	LIST( APPEND EXTERNAL_LIBS_LIBRARIES  ${OPENGL_LIBRARIES} )
ENDIF()

# ------------------------------------------------------------------------------
# Find Boost
# ------------------------------------------------------------------------------
set(Boost_USE_STATIC_LIBS   ON)
set(Boost_USE_MULTITHREADED ON)
#set(Boost_USE_STATIC_RUNTIME FALSE)

find_package(Boost 1.38 COMPONENTS system program_options iostreams thread REQUIRED)

if ( Boost_FOUND )
	link_directories( ${Boost_LIBRARY_DIRS} ) 
	LIST( APPEND EXTERNAL_LIBS_INCLUDE_DIR ${Boost_INCLUDE_DIRS} )
	LIST( APPEND EXTERNAL_LIBS_LIBRARIES ${Boost_LIBRARIES} )
endif()

mark_as_advanced( CLEAR Boost_INCLUDE_DIR ) 
mark_as_advanced( CLEAR Boost_LIBRARY_DIRS )

# ------------------------------------------------------------------------------
# Find LibXml2
# ------------------------------------------------------------------------------
#FIND_PACKAGE( LibXml2 REQUIRED )
#IF( NOT LIBXML2_FOUND )
#    message( SEND_ERROR "LibXml2 required, but not found with 'find_package()'" )
#ELSE()
#	list( APPEND EXTERNAL_LIBS_INCLUDE_DIR  ${LIBXML2_INCLUDE_DIR} )
#	list( APPEND EXTERNAL_LIBS_LIBRARIES  ${LIBXML2_LIBRARIES} )
#ENDIF()


# ------------------------------------------------------------------------------
# Find freeglut
# ------------------------------------------------------------------------------
#FIND_PACKAGE(FreeGLUT REQUIRED)
#IF(NOT FREEGLUT_FOUND)
#	MESSAGE( SEND_ERROR "GLUT required, but not found with 'find_package()'" )
#ELSE()
#	INCLUDE_DIRECTORIES(${FREEGLUT_INCLUDE_DIRS})
#ENDIF()

# ------------------------------------------------------------------------------
# Find Qtilities if using prebuild binaries of Qtilities
# ------------------------------------------------------------------------------
#OPTION(OPTION_USE_INTERNAL_QTILITIES "Build with internal Qtilities library" TRUE)
#IF(NOT ${OPTION_USE_INTERNAL_QTILITIES})
#	MESSAGE("Finding Qtilities libraries")
#	FIND_PACKAGE(QTilities REQUIRED)
#	IF(NOT QTILITIES_FOUND)
#		MESSAGE( SEND_ERROR "Qtilities required, but not found with 'find_package()'" )
#	ELSE()
#		MESSAGE(STATUS "Qtilities libraries found!")
#		INCLUDE_DIRECTORIES(${QTILITIES_INCLUDE_DIRS})
		#MESSAGE("Qtilities include dirs:	" ${QTILITIES_INCLUDE_DIRS})
		#MESSAGE("Qtilities libs:	" ${QTILITIES_LIBRARIES})
#	ENDIF()
#ENDIF()


# ------------------------------------------------------------------------------
# Integrated Libs
# ------------------------------------------------------------------------------
LIST( APPEND INTEGRATED_LIBS_INCLUDE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/IntegratedLibs/Eigen/include
${CMAKE_CURRENT_SOURCE_DIR}/IntegratedLibs/glew/include
#${CMAKE_CURRENT_SOURCE_DIR}/IntegratedLibs/QWT/include
${CMAKE_CURRENT_SOURCE_DIR}/IntegratedLibs/propertybrowser
${CMAKE_CURRENT_SOURCE_DIR}/IntegratedLibs/GLCLib/include
#${CMAKE_CURRENT_SOURCE_DIR}/IntegratedLibs/QTilities/include
${CMAKE_CURRENT_SOURCE_DIR}/IntegratedLibs/QGLViewer
${CMAKE_CURRENT_SOURCE_DIR}/IntegratedLibs/
)

#SET( INTEGRATED_LIBS_LIBRARIES GLEW QGLViewer PROPERTYBROWSER QWT QtilitiesLogging QtilitiesCore QtilitiesCoreGui QtilitiesExtensionSystem QtilitiesProjectManagement )
SET( INTEGRATED_LIBS_LIBRARIES GLEW QGLViewer PROPERTYBROWSER GLCLib )
ADD_SUBDIRECTORY(IntegratedLibs/glew)
ADD_SUBDIRECTORY(IntegratedLibs/QGLViewer)
#ADD_SUBDIRECTORY(IntegratedLibs/QWT)
ADD_SUBDIRECTORY(IntegratedLibs/propertybrowser)
ADD_SUBDIRECTORY(IntegratedLibs/GLCLib)
#ADD_SUBDIRECTORY(IntegratedLibs/QTilities/src/Logging)
#ADD_SUBDIRECTORY(IntegratedLibs/QTilities/src/Core)
#ADD_SUBDIRECTORY(IntegratedLibs/QTilities/src/CoreGui)
#ADD_SUBDIRECTORY(IntegratedLibs/QTilities/src/ExtensionSystem)
#ADD_SUBDIRECTORY(IntegratedLibs/QTilities/src/ProjectManagement)

LIST( APPEND EXTERNAL_LIBS_INCLUDE_DIR ${INTEGRATED_LIBS_INCLUDE_DIR} )
LIST( APPEND EXTERNAL_LIBS_LIBRARIES ${INTEGRATED_LIBS_LIBRARIES} )
