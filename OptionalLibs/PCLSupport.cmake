# ------------------------------------------------------------------------------
# add optional pcl support for CloudStudio
# ------------------------------------------------------------------------------
OPTION( OPTION_USE_PCL "Build with pcl support" OFF )

IF( ${OPTION_USE_PCL} )
	find_package(PCL 1.6 REQUIRED COMPONENTS visualization common io)
	include_directories(${PCL_INCLUDE_DIRS})
	link_directories(${PCL_LIBRARY_DIRS})
	add_definitions(${PCL_DEFINITIONS})
ENDIF()

FUNCTION(target_link_pcl)
#1 argument:
#ARGV0: program name
IF( ${OPTION_USE_PCL} )
	TARGET_LINK_LIBRARIES( ${ARGV0} ${PCL_LIBRARIES} )
	set_property( TARGET ${ARGV0} APPEND PROPERTY COMPILE_DEFINITIONS CS_PCL_SUPPORT )
ENDIF()
ENDFUNCTION()
