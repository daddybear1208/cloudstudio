# ------------------------------------------------------------------------------
# add optional LibLas support for CloudStudio
# ------------------------------------------------------------------------------
OPTION( OPTION_USE_LIBLAS "Build with liblas support" OFF )

IF( ${OPTION_USE_LIBLAS} )
		# Boost (using static, multi-threaded libraries)
	if ( WIN32 )
		if ( MSVC )
			set(Boost_USE_STATIC_LIBS   ON)
			set(Boost_USE_MULTITHREADED ON)
		endif() 
	endif()

	find_package(Boost 1.38 COMPONENTS program_options thread REQUIRED)

	if ( Boost_FOUND AND Boost_PROGRAM_OPTIONS_FOUND )
		include_directories( ${Boost_INCLUDE_DIRS} )
		link_directories( ${Boost_LIBRARY_DIRS} ) 
	endif()

	# make these available for the user to set.
	mark_as_advanced(CLEAR Boost_INCLUDE_DIR) 
	mark_as_advanced(CLEAR Boost_LIBRARY_DIRS)
	
	FIND_PACKAGE(LibLAS REQUIRED)
	IF(NOT LIBLAS_FOUND)
		MESSAGE( SEND_ERROR "LibLAS required, but not found with 'find_package()'" )
	ELSE()
		INCLUDE_DIRECTORIES(${LIBLAS_INCLUDE_DIR})
	ENDIF()
	
ENDIF()

FUNCTION(target_link_liblas)
#1 argument:
#ARGV0: program name
IF( ${OPTION_USE_LIBLAS} )
	TARGET_LINK_LIBRARIES( ${ARGV0} ${LIBLAS_LIBRARY} )
	set_property( TARGET ${ARGV0} APPEND PROPERTY COMPILE_DEFINITIONS CS_LAS_SUPPORT )
ENDIF()
ENDFUNCTION()

	