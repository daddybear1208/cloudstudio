#ifndef CSGLVIEWER_H
#define CSGLVIEWER_H

#include <QList>
#include "GLStater.h"
#include <QGLViewer/qglviewer.h>
#include <CS_CORE/csPointCloud.h>
#include <CS_CORE/csGroupObject.h>
#include <QGLViewer/manipulatedCameraFrame.h>
#include "cs_vis_globe.h"

using namespace qglviewer;


class CS_VIS_API CSGLViewer :public QGLViewer
{
	Q_OBJECT
public:	
	
	enum OrientationAxisDrawnArea
	{
		LeftTopArea, LeftBottomArea, RightTopArea, RightBottomArea
	};

	enum BACKGROUND_MODE
	{
		SINGLE_COLOR = 0,
		GRADIENT_COLOR
	};


	CSGLViewer(QWidget* parent = 0, const QGLWidget* shareWidget = 0, Qt::WindowFlags flags = 0);
	CSGLViewer(QGLContext *context, QWidget* parent = 0, const QGLWidget* shareWidget = 0, Qt::WindowFlags flags = 0);
	CSGLViewer(const QGLFormat& format, QWidget* parent = 0, const QGLWidget* shareWidget = 0, Qt::WindowFlags flags = 0);
	~CSGLViewer();

	CSGroupObject* sceneRoot() const;

	// background color options
	BACKGROUND_MODE backgroundColorMode() const;
	void setBackgroundColorMode(BACKGROUND_MODE mode);
	const QColor& gradientBgColor0() const;
	const QColor& gradientBgColor1() const;
	

	enum ViewOrientation
	{
		FREE_VIEW = 0,
		FRONT_VIEW,
		BACK_VIEW,
		LEFT_VIEW,
		RIGHT_VIEW,
		TOP_VIEW,
		BOTTOM_VIEW
	};



public Q_SLOTS:
	void setSceneRoot(CSGroupObject* root);
	void refreshSceneContent();

	void setViewType(ViewOrientation type, float duration);
	void setGradientBgColor(const QColor& c0, const QColor& c1);
	void setGradientBgColor0(const QColor& c);
	void setGradientBgColor1(const QColor& c);

protected:
	virtual void draw();
	virtual void init();

	virtual void preDraw();
	virtual void postDraw();

	virtual void drawGradientBackground();
	virtual void drawCoordinateAxis();
	virtual void drawAxis(float length,
		const QColor &charXColor, const QColor &charYColor, const QColor &charZColor,
		const QColor &axisXColor, const QColor &axisYColor, const QColor &axisZColor,
		float charWidthRatio, float charHeightRatio, float charShiftRatio, float arrowRadiusRatio, int arrowSubdivisions, float alphaRatio = 1.0f);

private:
	void preDrawGLStates();
	void postDrawGLStates();
	void recursiveFindDrawableObjects(CSObject* object);
	CSBoundingBox calculateSceneBoundingBox();
	QList<CSDrawableObject*> drawable_container_;
	CSGroupObject* scene_root_;
	BACKGROUND_MODE background_mode_;
	QColor gradient_bg_color0_;
	QColor gradient_bg_color1_;
	OrientationAxisDrawnArea orientation_axis_draw_area_;
	float orientation_axis_factor_;
	GLStater glstater_;
};

#endif //! CSGLVIEWER_H