#include "csGLViewer.h"
#include <QtGlobal>


CSGLViewer::CSGLViewer(QWidget* parent /*= 0*/, const QGLWidget* shareWidget /*= 0*/, Qt::WindowFlags flags /*= 0*/)
	:QGLViewer(parent, shareWidget, flags), 
	scene_root_(0), 
	background_mode_(GRADIENT_COLOR),
	orientation_axis_draw_area_(LeftBottomArea),
	orientation_axis_factor_(0.08f)
{
	gradient_bg_color0_ = Qt::white;
	gradient_bg_color1_ = Qt::black;
}

CSGLViewer::CSGLViewer(QGLContext *context, QWidget* parent /*= 0*/, const QGLWidget* shareWidget /*= 0*/, Qt::WindowFlags flags /*= 0*/)
	: QGLViewer(context, parent, shareWidget, flags), 
	scene_root_(0), 
	background_mode_(GRADIENT_COLOR),
	orientation_axis_draw_area_(LeftBottomArea),
	orientation_axis_factor_(0.08f)
{
	gradient_bg_color0_ = Qt::white;
	gradient_bg_color1_ = Qt::black;
}

CSGLViewer::CSGLViewer(const QGLFormat& format, QWidget* parent /*= 0*/, const QGLWidget* shareWidget /*= 0*/, Qt::WindowFlags flags /*= 0*/)
	: QGLViewer(format, parent, shareWidget, flags), 
	scene_root_(0), 
	background_mode_(GRADIENT_COLOR),
	orientation_axis_draw_area_(LeftBottomArea),
	orientation_axis_factor_(0.08f)
{
	gradient_bg_color0_ = Qt::white;
	gradient_bg_color1_ = Qt::black;
}

CSGLViewer::~CSGLViewer()
{

}

void CSGLViewer::init()
{
	glClearColor(0.0, 0.0, 0.0, 0.0);
	glDisable(GL_LIGHTING);
}

void CSGLViewer::setSceneRoot(CSGroupObject* root)
{
	scene_root_ = root;
	refreshSceneContent();
	CSBoundingBox bbox = calculateSceneBoundingBox();
	if (!bbox.isEmpty())
	{
		// 		setSceneCenter(qglviewer::Vec(bbox.center().x(), bbox.center().y(), bbox.center().z()));
		// 		setSceneRadius((bbox.upperCorner() - bbox.lowerCorner()).length());
		setSceneBoundingBox(qglviewer::Vec(bbox.lowerCorner().x(), bbox.lowerCorner().y(), bbox.lowerCorner().z()),
		 	qglviewer::Vec(bbox.upperCorner().x(), bbox.upperCorner().y(), bbox.upperCorner().z()));
	}
	showEntireScene();
}

void CSGLViewer::refreshSceneContent()
{
	drawable_container_.clear();
	if (!scene_root_)
		return;
	recursiveFindDrawableObjects(scene_root_);
	CSBoundingBox bbox = calculateSceneBoundingBox();
	if (!bbox.isEmpty())
	{
		// 		setSceneCenter(qglviewer::Vec(bbox.center().x(), bbox.center().y(), bbox.center().z()));
		// 		setSceneRadius((bbox.upperCorner() - bbox.lowerCorner()).length());
		setSceneBoundingBox(qglviewer::Vec(bbox.lowerCorner().x(), bbox.lowerCorner().y(), bbox.lowerCorner().z()),
			qglviewer::Vec(bbox.upperCorner().x(), bbox.upperCorner().y(), bbox.upperCorner().z()));
	}
	this->updateGL();
}

void CSGLViewer::draw()
{
	if (!scene_root_)
	{
		return;
	}
	if (drawable_container_.isEmpty())
		return;

	QList<CSDrawableObject*>::const_iterator i;
	for (i = drawable_container_.constBegin(); i != drawable_container_.constEnd(); ++i)
	{
		if ((*i)->isVisible())
		{
			if (camera()->frame()->isManipulated() || camera()->frame()->isSpinning())
			{
				(*i)->draw(5);
			}else
				(*i)->draw();
		}
	}
}


void CSGLViewer::recursiveFindDrawableObjects(CSObject* object)
{
	if (!object)
		return;
    if (object->isKindOf(CSObject::CS_DRAWABLE))
	{
		CSDrawableObject* drawable = dynamic_cast<CSDrawableObject*>(object);
		if (drawable)
			drawable_container_.push_back(drawable);
	}else if (object->isGroup())
	{
		CSGroupObject* groupObject = dynamic_cast<CSGroupObject*>(object);
		if (groupObject)
		{
			for (int i = 0; i < groupObject->childCount(); i++)
			{
				recursiveFindDrawableObjects(groupObject->childAt(i));
			}
		}
	}
}

CSBoundingBox CSGLViewer::calculateSceneBoundingBox()
{
	QList<CSDrawableObject*>::const_iterator i;
	CSBoundingBox bbox;
	for (i = drawable_container_.constBegin(); i != drawable_container_.constEnd(); ++i)
	{
		bbox.combine((*i)->getBoundingBox());
	}
	return bbox;
}

CSGroupObject* CSGLViewer::sceneRoot() const
{
	return scene_root_;
}


void CSGLViewer::preDraw()
{
	preDrawGLStates();
	if (background_mode_ == SINGLE_COLOR)
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	else
	{
		drawGradientBackground();
	}

	// GL_PROJECTION matrix
	camera()->loadProjectionMatrix();
	// GL_MODELVIEW matrix
	camera()->loadModelViewMatrix();

	Q_EMIT drawNeeded();
}

void CSGLViewer::postDraw()
{
	drawCoordinateAxis();
	QGLViewer::postDraw();
}

void CSGLViewer::setViewType(ViewOrientation type, float duration)
{
	qglviewer::Camera newCam(*camera());

	switch (type)
	{
	case CSGLViewer::FREE_VIEW:
		break;
	case CSGLViewer::FRONT_VIEW:
		newCam.setViewDirection(qglviewer::Vec(0, 0, -1));
		break;
	case CSGLViewer::BACK_VIEW:
		newCam.setViewDirection(qglviewer::Vec(0, 0, 1));
		break;
	case CSGLViewer::LEFT_VIEW:
		newCam.setViewDirection(qglviewer::Vec(1, 0, 0));
		break;
	case CSGLViewer::RIGHT_VIEW:
		newCam.setViewDirection(qglviewer::Vec(-1, 0, 0));
		break;
	case CSGLViewer::TOP_VIEW:
		newCam.setViewDirection(qglviewer::Vec(0, -1, 0));
		break;
	case CSGLViewer::BOTTOM_VIEW:
		newCam.setViewDirection(qglviewer::Vec(0, 1, 0));
		break;
	default:
		break;
	}
	newCam.showEntireScene();
	qglviewer::Frame* frame = dynamic_cast<Frame*>(newCam.frame());
	camera()->interpolateTo(*frame, duration);
	//updateGL();
}

void CSGLViewer::drawGradientBackground()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	GLStater glStater;
	glStater << GL_DEPTH_TEST << GL_ALPHA_TEST << GL_CULL_FACE << GL_LIGHTING << GL_BLEND << GL_TEXTURE_2D << GL_COLOR_MATERIAL;
	glStater.disableAllStates();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glShadeModel(GL_SMOOTH);

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	float side = 1.f;
	gluOrtho2D(-side, side, -side, side);

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();
	glBegin(GL_QUADS);
	glColor3ub(gradient_bg_color0_.red(), gradient_bg_color0_.green(), gradient_bg_color0_.blue());
	glVertex2f(-1.0f, 1.0f);
	glVertex2f(1.0f, 1.0f);
	glColor3ub(gradient_bg_color1_.red(), gradient_bg_color1_.green(), gradient_bg_color1_.blue());
	glVertex2f(1.0f, -1.0f);
	glVertex2f(-1.0f, -1.0f);
	glEnd();
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glStater.restore();
}

void CSGLViewer::drawCoordinateAxis()
{
	int viewport[4];
	int scissor[4];

	// The viewport and the scissor are changed to fit the lower left
	// corner. Original values are saved.
	glGetIntegerv(GL_VIEWPORT, viewport);
	glGetIntegerv(GL_SCISSOR_BOX, scissor);

	// Axis viewport size, in pixels
	uint width = viewport[2];
	uint height = viewport[3];
	int maxSize = qMax<int>(int(qMax<int>(width, height) * orientation_axis_factor_), 50);
	int lbx = 0, lby = 0;
	orientation_axis_draw_area_ = LeftBottomArea;
	switch (orientation_axis_draw_area_)
	{
	case LeftTopArea:    	lby = height - maxSize; break;
	case LeftBottomArea:    break;
	case RightTopArea:      lbx = width - maxSize, lby = height - maxSize; break;
	case RightBottomArea:   lbx = width - maxSize; break;
	}
	glViewport(lbx, lby, maxSize, maxSize);
	glScissor(lbx, lby, maxSize, maxSize);

	// The Z-buffer is cleared to make the axis appear over the
	// original image.
	//glClear(GL_DEPTH_BUFFER_BIT);
	GLStater stater;
	stater << GL_DEPTH_TEST << GL_LIGHTING;
	//glDisable(GL_DEPTH_TEST);

	// Tune for best line rendering
	glDisable(GL_LIGHTING);
	//glLineWidth(3.0);

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	glOrtho(-1, 1, -1, 1, -1, 1);

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();
	glMultMatrixd(camera()->orientation().inverse().matrix());

	drawAxis(1.0*0.9f, QColor(255, 50, 50), QColor(50, 255, 50), QColor(50, 50, 255),
		QColor(255, 0, 0, 200), QColor(0, 255, 0, 200), QColor(0, 0, 255, 200),
		1.0f / 15.0f, 1.0f / 10.0f, 1.05f, 0.045f, 6);

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	//glEnable(GL_LIGHTING);

	stater.restore();
	// The viewport and the scissor are restored.
	//glScissor(scissor[0],scissor[1],scissor[2],scissor[3]);
	glScissor(viewport[0], viewport[1], viewport[2], viewport[3]);
	glViewport(viewport[0], viewport[1], viewport[2], viewport[3]);
}

void CSGLViewer::drawAxis(float length, const QColor &charXColor, const QColor &charYColor, const QColor &charZColor, const QColor &axisXColor, const QColor &axisYColor, const QColor &axisZColor, float charWidthRatio, float charHeightRatio, float charShiftRatio, float arrowRadiusRatio, int arrowSubdivisions, float alphaRatio /*= 1.0f*/)
{
	if (length < 0.0f)
		length = sceneRadius();
	const float charWidth = length * charWidthRatio;
	const float charHeight = length * charHeightRatio;
	const float charShift = charShiftRatio * length;

	GLStater glStater;
	glStater << GL_LIGHTING << GL_COLOR_MATERIAL << GL_LINE_SMOOTH << GL_BLEND;

	glDisable(GL_LIGHTING);

	glEnable(GL_LINE_SMOOTH);
	glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);  // Antialias the lines   
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glBegin(GL_LINES);
	// The X
	glColor4f(charXColor.redF(), charXColor.greenF(), charXColor.blueF(), charXColor.alphaF()*alphaRatio);
	glVertex3f(charShift, charWidth, -charHeight);
	glVertex3f(charShift, -charWidth, charHeight);
	glVertex3f(charShift, -charWidth, -charHeight);
	glVertex3f(charShift, charWidth, charHeight);
	// The Y
	glColor4f(charYColor.redF(), charYColor.greenF(), charYColor.blueF(), charYColor.alphaF()*alphaRatio);
	glVertex3f(charWidth, charShift, charHeight);
	glVertex3f(0.0, charShift, 0.0);
	glVertex3f(-charWidth, charShift, charHeight);
	glVertex3f(0.0, charShift, 0.0);
	glVertex3f(0.0, charShift, 0.0);
	glVertex3f(0.0, charShift, -charHeight);
	// The Z
	glColor4f(charZColor.redF(), charZColor.greenF(), charZColor.blueF(), charZColor.alphaF()*alphaRatio);
	glVertex3f(-charWidth, charHeight, charShift);
	glVertex3f(charWidth, charHeight, charShift);
	glVertex3f(charWidth, charHeight, charShift);
	glVertex3f(-charWidth, -charHeight, charShift);
	glVertex3f(-charWidth, -charHeight, charShift);
	glVertex3f(charWidth, -charHeight, charShift);
	glEnd();

	glEnable(GL_LIGHTING);
	glDisable(GL_COLOR_MATERIAL);

	float color[4];
	color[0] = axisXColor.redF();  color[1] = axisXColor.greenF();  color[2] = axisXColor.blueF();  color[3] = axisXColor.alphaF()*alphaRatio;
	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, color);
	QGLViewer::drawArrow(length, arrowRadiusRatio*length, arrowSubdivisions);

	color[0] = axisYColor.redF();  color[1] = axisYColor.greenF();  color[2] = axisYColor.blueF();  color[3] = axisYColor.alphaF()*alphaRatio;
	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, color);
	glPushMatrix();
	glRotatef(90.0, 0.0, 1.0, 0.0);
	QGLViewer::drawArrow(length, arrowRadiusRatio*length, arrowSubdivisions);
	glPopMatrix();

	color[0] = axisZColor.redF();  color[1] = axisZColor.greenF();  color[2] = axisZColor.blueF();  color[3] = axisZColor.alphaF()*alphaRatio;
	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, color);
	glPushMatrix();
	glRotatef(-90.0, 1.0, 0.0, 0.0);
	QGLViewer::drawArrow(length, arrowRadiusRatio*length, arrowSubdivisions);
	glPopMatrix();

	glStater.restore();
}

void CSGLViewer::postDrawGLStates()
{
	GLStater postStater;
	postStater << GL_DEPTH_TEST << GL_CULL_FACE << GL_LIGHTING << GL_COLOR_MATERIAL << GL_TEXTURE_2D << GL_LIGHT0 << GL_NORMALIZE;
	postStater.disableAllStates();
}

void CSGLViewer::preDrawGLStates()
{
	if (glstater_.isEmpty())
		glstater_ << GL_DEPTH_TEST << GL_CULL_FACE << GL_LIGHTING << GL_COLOR_MATERIAL << GL_TEXTURE_2D << GL_LIGHT0 << GL_NORMALIZE;
	else
		glstater_.restore();
}

CSGLViewer::BACKGROUND_MODE CSGLViewer::backgroundColorMode() const
{
	return background_mode_;
}


const QColor& CSGLViewer::gradientBgColor0() const
{
	return gradient_bg_color0_;
}

const QColor& CSGLViewer::gradientBgColor1() const
{
	return gradient_bg_color1_;
}

void CSGLViewer::setBackgroundColorMode(BACKGROUND_MODE mode)
{
	background_mode_ = mode;
	updateGL();
}

void CSGLViewer::setGradientBgColor(const QColor& c0, const QColor& c1)
{
	gradient_bg_color0_ = c0;
	gradient_bg_color1_ = c1;
	updateGL();
}

void CSGLViewer::setGradientBgColor0(const QColor& c)
{
	gradient_bg_color0_ = c;
	updateGL();
}

void CSGLViewer::setGradientBgColor1(const QColor& c)
{
	gradient_bg_color1_ = c;
	updateGL();
}







