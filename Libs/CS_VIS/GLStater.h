#ifndef GLSTATER_H
#define GLSTATER_H

#ifdef  Q_OS_WIN32
#include <windows.h>
#endif //  _WIN32

#ifdef Q_OS_MAC
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#else
#include <GL/gl.h>
#include <GL/glu.h>
#endif

#include <QMap>

class GLStater
{
public :
	inline GLStater(const GLStater &l) : _glStates(l.currentStates()) {}
	GLStater() {}
	virtual ~GLStater() {}
	
	GLboolean addState(GLenum cap);
	void removeState(GLenum cap);
	void disableAllStates();
	void enableAllStates();
	void saveCurrent();
	void save(GLenum cap);
	void restore();
	bool restore(GLenum cap);
	void clear();
	bool isEmpty() const { return _glStates.isEmpty(); }
	QMap<GLenum,GLboolean> currentStates() const { return _glStates; }

	GLStater &operator=(const GLStater &l)
	{
		_glStates = l.currentStates();
		return *this;
	}

	GLStater &operator+=(const GLStater &l)
	{
		if (!l.isEmpty()) {
			if (isEmpty()) {
				*this = l;
			} else {
				const QMap<GLenum, GLboolean> &lStates = l.currentStates();
				QMap<GLenum, GLboolean>::const_iterator i;
				for (i = lStates.begin(); i != lStates.end(); ++i)
					_glStates[i.key()] = i.value();
			}
		}
		return *this;
	}

	inline GLStater operator+(const GLStater &l) const
	{ GLStater n = *this; n += l; return n; }
	inline GLStater &operator+=(const GLenum &t)
	{ addState(t); return *this; }
	inline GLStater &operator<< (const GLenum &t)
	{ addState(t); return *this; }
	inline GLStater &operator<<(const GLStater &l)
	{ *this += l; return *this; }

protected:
	QMap<GLenum,GLboolean>  _glStates;
};

#endif // GLSTATER_H
