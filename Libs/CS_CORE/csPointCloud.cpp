#include "csPointCloud.h"
#include "csFileReaderHelper.h"
#include "csLogger.h"
#include "csProgressDialog.h"

CSPointCloud::CSPointCloud(const QString& name /*= "PointCloud"*/)
    :CSObject(name),
	CSHierarchicalObject(name), CSDrawableObject(name),
	point_size_(1.0f), color_(0), normals_(0), file_name_("")
{
	points_ = new CSChunkedArray < 3, double >() ;
}

CSPointCloud::CSPointCloud(unsigned int pointCount, const QString& name /*= "PointCloud"*/)
    :CSObject(name),
    CSHierarchicalObject(name), CSDrawableObject(name),
	point_size_(1.0f), color_(0), normals_(0), file_name_("")
{
	points_ = new CSChunkedArray < 3, double >();
	points_->reserve(pointCount);
}

CSPointCloud::CSPointCloud(const QString& fileName, const QString& type, const QString& name /*= "PointCloud"*/)
    :CSObject(name),
    CSHierarchicalObject(name), CSDrawableObject(name),
	point_size_(1.0f), color_(0), normals_(0), file_name_("")
{
	points_ = new CSChunkedArray < 3, double >();
}

void CSPointCloud::draw(int level)
{
	if (!isVisible())
		return;
	glColor3f(1.0, 1.0, 1.0);
	glPointSize((GLfloat)point_size_);
	glPushMatrix();

	glEnableClientState(GL_VERTEX_ARRAY);
	bool drawColor = false;
	if (color_&&color_->isAllocated())
		drawColor = true;
	if (drawColor)
	{
		glEnableClientState(GL_COLOR_ARRAY);
	}
	unsigned int chunksCount = points_->chunksCount();
	for (unsigned int i = 0; i < chunksCount; ++i)
	{
		glVertexPointer(3, GL_DOUBLE, level*3*sizeof(double), points_->chunkStartPtr(i));
		if (drawColor)
			glColorPointer(3, GL_UNSIGNED_BYTE, level*3*sizeof(unsigned char), color_->chunkStartPtr(i));
		unsigned int size = points_->chunkSize(i);
		if (level)
			size = static_cast<unsigned>(floor(static_cast<float>(points_->chunkSize(i)) / level));
		glDrawArrays(GL_POINTS, 0, GLsizei(size));

	}
	glDisableClientState(GL_VERTEX_ARRAY);
	if (drawColor)
	{
		glDisableClientState(GL_COLOR_ARRAY);
	}
	if (selected_)
	{
		drawBoundingBox();
	}

	glPopMatrix();
}

void CSPointCloud::addPoint(const Eigen::Vector3d& pt)
{
	points_->addElement(pt.data());
}

void CSPointCloud::addPoint(const double* pt)
{
	points_->addElement(pt);
}

void CSPointCloud::addPoint(double x, double y, double z)
{
	double pt[3] = { x, y, z };
	points_->addElement(pt);
}

void CSPointCloud::setPointSize(float size)
{
	point_size_ = size;
}

const float& CSPointCloud::pointSize() const
{
	return point_size_;
}

unsigned CSPointCloud::pointsCount() const
{
	return points_->elementCount();
}

CSPointCloud::~CSPointCloud()
{
	delete points_;
}

bool CSPointCloud::reservePoints(unsigned int pointNum)
{
	return points_->reserve(pointNum);
}

bool CSPointCloud::freeRedundantMem()
{
	return points_->resize(points_->elementCount());
}


void CSPointCloud::clear()
{
	points_->clear();
}

QDomElement CSPointCloud::toDomElement(QDomDocument& document) const
{
	QDomElement element = document.createElement("PointCloud");
	QDomElement nameElement = document.createElement("Name");
	nameElement.appendChild(document.createTextNode(this->name()));
	element.appendChild(nameElement);
	QDomElement srcElement = document.createElement("Src");
	srcElement.appendChild(document.createTextNode(file_name_));
	element.appendChild(srcElement);
	return element;
}

bool CSPointCloud::initFromDomElement(const QDomElement& element)
{
	QDomElement nameElement = element.firstChildElement("Name");
	if (nameElement.isNull()) return false;
	this->setName(nameElement.firstChild().toText().data());
	QDomElement srcElement = element.firstChildElement("Src");
	if (srcElement.isNull()) return false;
    QString sourceFileName =srcElement.firstChild().toText().data();
    file_name_ = QFileInfo(sourceFileName).absoluteFilePath();
	CSFileReaderInterface* reader = 
		CSFileReaderHelper::instance()->getFileReader(file_name_);
	if (!reader)
	{
		CSLogHandler::instance()->
			reportError(QString("Cannot find plugin to load point cloud: '%1'").arg(file_name_));
		return false;
	}
	CSProgessDialog* progressDlg = new CSProgessDialog(QApplication::activeWindow());
	progressDlg->setLabelText(QString("Loading point cloud: '%1'").arg(name()));
	QObject::connect(reader, SIGNAL(progressChanged(int)), progressDlg, SLOT(setValue(int)));
	progressDlg->show();
	bool readRst = reader->readFile(this, file_name_);
	reader->disconnect();
	progressDlg->close();
	if (!readRst)
	{
		this->clear();
		CSLogHandler::instance()->reportError("Point cloud '" + name() + "' not load correctly! Error: " + reader->lastError());
		return false;
	}
    CSLogHandler::instance()->reportInfo(QString("Successfully load point cloud, point count: %1").arg(pointsCount()));
	return true;
}


Eigen::Vector3d CSPointCloud::getPoint(unsigned int index)
{
	assert(index < points_->elementCount());
	Eigen::Vector3d pt;
	memcpy(pt.data(), points_->value(index), 3 * sizeof(float));
	return pt;
}

CSObject::CS_OBJECT_TYPE CSPointCloud::objectType() const
{
	return CSObject::CS_POINTCLOUD;
}

void CSPointCloud::calculateBoundingBox()
{
	// calculate point cloud bounding box
	points_->calculateMinMaxVals();
 	Eigen::Vector3f min;
	double* minVals = points_->minVal();
	min << minVals[0], minVals[1], minVals[2];
 	bounding_box_.setLowerCorner(min.cast<double>());
	Eigen::Vector3f max;
	double* maxVals = points_->maxVal();
	max << maxVals[0], maxVals[1], maxVals[2];
	bounding_box_.setUpperCorner(max.cast<double>());
}

void CSPointCloud::addColorField(const uchar* color)
{
	color_->addElement(color);
}

void CSPointCloud::addColorField(uchar r, uchar g, uchar b)
{
	uchar data[3] = { r, g, b };
	addColorField(data);
}

bool CSPointCloud::setupColorField()
{
	if (!points_ || !points_->isAllocated())
		return false;
	if (color_)
	{
		if (color_->isAllocated())
		{
			return true;
		}
	}
	if (!color_)
		color_ = new CSChunkedArray < 3, uchar > ;
	if (!color_->reserve(points_->capacity()))
	{
		return false;
	}
	return true;
}

bool CSPointCloud::setupNormalField()
{
	if (!points_ || !points_->isAllocated())
		return false;
	if (normals_->isAllocated())
	{
		return true;
	}
	if (!normals_)
		normals_ = new CSChunkedArray < 3, float >;
	if (!normals_->reserve(points_->capacity()))
	{
		return false;
	}
	return true;
}

void CSPointCloud::clearColorField()
{
	if (!color_)
		return;
	color_->clear();
	delete color_;
	color_ = 0;
}

double* CSPointCloud::getPointData(unsigned int index)
{
	if (!points_)
		return 0;
	return points_->value(index);
}







