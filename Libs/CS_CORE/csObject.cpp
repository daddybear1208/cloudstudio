#include "csObject.h"

CSObject::CSObject(const QString& name)
	:object_name_(name)
{
	unique_id_ = QUuid::createUuid();
}

// bool CSObject::operator!=(const CSObject& other) const
// {
// 	return !(other.getUuid() == unique_id_);
// }
// 
// bool CSObject::operator==(const CSObject& other) const
// {
// 	return other.getUuid() == unique_id_;
// }

const QString& CSObject::name() const
{
	return object_name_;
}

void CSObject::setName(const QString& name)
{
	object_name_ = name;
}

const QUuid& CSObject::getUuid() const
{
	return unique_id_;
}

CSObject::~CSObject()
{

}

bool CSObject::isA(CS_OBJECT_TYPE objType) const
{
	return (objectType() == objType);
}

bool CSObject::isKindOf(CS_OBJECT_TYPE objType) const
{
	return ((objectType()&objType) == objType);
}

bool CSObject::isGroup() const
{
	return isKindOf(CS_GROUP);
}

bool CSObject::isHierachical() const
{
	return ((objectType() | CS_HIERARCHICAL) != 0);
}

