#ifndef CSCAMERA_H
#define CSCAMERA_H

#include "csHierarchicalObject.h"
#include "cs_core_globe.h"
#include <QGLViewer/camera.h>

//! an adapter class for qglviewer::Camera class

class CS_CORE_API CSCamera: public qglviewer::Camera, public CSHierarchicalObject
{
public:
	CSCamera(const QString& name = "cam");
	~CSCamera();

	CSObject::CS_OBJECT_TYPE objectType() const;

	// inherited from CSHierarchicalObject
	virtual QDomElement toDomElement(QDomDocument& document) const;
	virtual bool initFromDomElement(const QDomElement& element);
    

};


#endif