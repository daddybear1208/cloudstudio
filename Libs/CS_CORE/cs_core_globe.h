#ifdef CS_CORE_LIBRARY
#define CS_CORE_API Q_DECL_EXPORT
#else
#define CS_CORE_API Q_DECL_IMPORT
#endif // CS_CORE_EXPORTS


// solve inline problems on mac os xcode
#include <QObject>
#ifdef Q_OS_MAC
#define CS_INLINE
#elif defined Q_OS_WIN32
#define CS_INLINE inline
#endif

#ifndef CS_INLINE
#define CS_INLINE
#endif
