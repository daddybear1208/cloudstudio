#ifndef	CSFILEREADERHELPER_H
#define CSFILEREADERHELPER_H

#include <QString>
#include <QMap>
#include "cs_core_globe.h"
#include "csFileReaderInterface.h"

class CS_CORE_API CSFileReaderHelper
{
public:

	enum PLUGIN_ADD_POLICY
	{
		ADD_FIRST = 0,
		ADD_LAST
	};

	static CSFileReaderHelper* instance();
	CS_INLINE CS_FILE_TYPES getFileType(const QString& filename) const;
	CS_INLINE QStringList getFileExtension(CS_FILE_TYPES type) const;
	CS_INLINE CSFileReaderInterface* getFileReader(CS_FILE_TYPES fileType) const;
	CS_INLINE CSFileReaderInterface* getFileReader(const QString& filename) const;
	CS_INLINE void addFileReader(CSFileReaderInterface* reader);
	CS_INLINE void setPluginAddPolicy(PLUGIN_ADD_POLICY policy);
	CS_INLINE PLUGIN_ADD_POLICY pluginAddPolicy() const;
	CS_INLINE QList<CSFileReaderInterface*> readers() const;

protected:
	CSFileReaderHelper();
	void initializeFileExtMap();

private:
	static CSFileReaderHelper* instance_;
	QMap<QString, CS_FILE_TYPES> file_ext_map_;
	QMap<CS_FILE_TYPES, CSFileReaderInterface*> file_reader_map_;

	// if there is already a file reader plugin for a specific type of file
	// this policy decide whether to load a new one or just use the old one
	// default: use the last one
	PLUGIN_ADD_POLICY add_plugin_policy_;
};

#endif