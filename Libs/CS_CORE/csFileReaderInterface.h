#ifndef CSFILEREADERINTERFACE_H
#define CSFILEREADERINTERFACE_H

#include "csHierarchicalObject.h"
#include <QtPlugin>
#include "csPluginBase.h"

enum CS_FILE_TYPES {
	UNKNOWN_FILE = 0,		/**< unknown type */
	ASCII,					/**< ASC,NEU, XYZ, TXT, PTS, etc. */
	PLY,					/**< Stanford mesh file */
	OBJ,					/**< Wavefront mesh file */
	STL,					/**< STL mesh file (ascii) */
	PCD,					/**< Point Cloud Library file */
	LAS						/**< LAS lidar point cloud (binary) */
};


const char CS_FILE_TYPES_DESCRIPTIONS[][64] = {
	"Unknown type",
	"ASCII (Ascii point cloud)",
	"PLY (Stanford mesh file)",
	"OBJ (Wavefront mesh file)",
	"STL (STL ascii mesh file)",
	"PCD (Point Cloud Library file)",
	"LAS (Binary lidar point cloud file)"
};



//! Interface for file reader plugins
class CS_CORE_API CSFileReaderInterface:public CSPluginBase
{
	Q_OBJECT
public:
	CSFileReaderInterface() :CSPluginBase(){}

	// load CSHierarchicalObject from file
	virtual bool readFile(CSHierarchicalObject* object, const QString& filename) = 0;

	// write CSHierarchicalObject to file
	virtual bool writeFile(CSHierarchicalObject* object, const QString& filename) = 0;

	// return the supported file type
	virtual CS_FILE_TYPES fileType() const = 0;

	// return the last error
	virtual const QString& lastError() const
	{
		return error_;
	}

	CS_PLUGIN_TYPE pluginType() const
	{
		return CSPluginBase::CS_FILE_READER_PLUGIN;
	}

	virtual CSObject::CS_OBJECT_TYPE objectType() const
	{
		return CSObject::CS_POINTCLOUD;
	}

signals:
	void progressChanged(int value);
protected:
	QString error_;

private:
	Q_DISABLE_COPY(CSFileReaderInterface)
};

QT_BEGIN_NAMESPACE

Q_DECLARE_INTERFACE(CSFileReaderInterface,
"com.cvrs.CloudStudio.CSFileReaderInterface/1.0")

QT_END_NAMESPACE

#endif