#include "csCamera.h"

CSCamera::CSCamera(const QString& name /*= "cam"*/)
	:CSObject(name),
	CSHierarchicalObject(name)
{
}



CSCamera::~CSCamera()
{

}

QDomElement CSCamera::toDomElement(QDomDocument& document) const
{
	return qglviewer::Camera::domElement(name(), document);
}

bool CSCamera::initFromDomElement(const QDomElement& element)
{
	qglviewer::Camera::initFromDOMElement(element);
	return true;
}

CSObject::CS_OBJECT_TYPE CSCamera::objectType() const
{
    return CSObject::CS_CAMERA;
}





