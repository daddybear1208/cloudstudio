#ifndef LOGHANDLER_H
#define LOGHANDLER_H

#include <QObject>
#include <QApplication>
#include <QMutex>
#include <QStringList>
#include <QSettings>
#include <QDir>

#include "csVersion.h"

#include "cs_core_globe.h"

class QTimer;
class QTextStream;
class QFile;

class CS_CORE_API CSLogHandler:public QObject
{
	Q_OBJECT

public:

	enum LogLevel
	{
		LOG_DEBUG = 0,                      /*!< Message only for debugging purpose. */
		LOG_INFO,                           /*!< Information message. */
		LOG_WARNING,                        /*!< Warning, abnormal event. */
		LOG_ERROR                           /*!< Error, invalid file. */
	};

	static CSLogHandler *instance();
	~CSLogHandler();

	void setMessageLevel(LogLevel level)
	{
		_currLevel = level;
	}

	void setLogDirectory(const QString &);
	void setLogToFile(bool);

	void setBufferization(bool);
	void startEmission(bool);

	public slots:

		void reportDebug(const QString &message );
		void reportInfo(const QString &message );
		void reportWarning(const QString &message );
		void reportError(const QString &message );

signals:

		void newMessage(const QString &);
		void newMessages(const QStringList &);

		private slots:

			void unqueueWaitingMessages();

private:

	CSLogHandler();

	CSLogHandler(const CSLogHandler &); // hide copy constructor
	CSLogHandler& operator=(const CSLogHandler &); // hide assign op
	// we leave just the declarations, so the compiler will warn us
	// if we try to use those two functions by accident

	void reportMessage(LogLevel level, const QString &message );
	void unqueueMessages();
	void loadSettings();
	bool fillAppLogFile(const QString &message);

	QMutex _lock;
	static CSLogHandler* _instance;
	LogLevel _currLevel;
	QStringList _buffer;
	bool _bufferized;
	bool _startEmission;
	QTimer *_timer;
	QString _logDirectory;
	QTextStream * _logStream;
	QFile *_logFile;
	bool _saveToLog;
};


#endif  // _LOGHANDLER_H_
