#ifndef CSBOUNDINGBOX_H
#define CSBOUNDINGBOX_H

#include "cs_core_globe.h"
#include <iostream>
#include <math.h>
#include <eigen3/Eigen/Core>
#include <GLC_BoundingBox>

//! Bounding Box class

class CS_CORE_API CSBoundingBox:public GLC_BoundingBox
{
public:
	CSBoundingBox();
	CSBoundingBox(const CSBoundingBox& boundingBox);
	CSBoundingBox(const GLC_Point3d& lower, const GLC_Point3d& upper);
	CSBoundingBox(const Eigen::Vector3d& lower, const Eigen::Vector3d& upper);
	CSBoundingBox(double lowerX, double lowerY, double lowerZ, double upperX, double upperY, double upperZ);

	CSBoundingBox& operator=(const CSBoundingBox& bbox);

    void setLowerCorner(const Eigen::Vector3d& lower);
    void setUpperCorner(const Eigen::Vector3d& upper);

    CSBoundingBox& combine(const Eigen::Vector3d& point);
    CSBoundingBox& combine(const CSBoundingBox& bbox);
    CSBoundingBox& transform(const Eigen::Matrix4d& matrix);
};


#endif