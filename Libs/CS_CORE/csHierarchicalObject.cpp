#include "csHierarchicalObject.h"

CSHierarchicalObject::CSHierarchicalObject(const QString& name /*= QString()*/)
	:CSObject(name),
	parent_(0),
	selected_(false),
    is_valid_(false),
	is_modified_(false)
{
	property_manager_ = new QtVariantPropertyManager;

}

CSHierarchicalObject::~CSHierarchicalObject()
{
	foreach (CSHierarchicalObject* obj,child_objects_)
	{
		delete obj;
	}
}


void CSHierarchicalObject::appendChild(CSHierarchicalObject* obj)
{
	child_objects_.append(obj);
	obj->setParent(this);
}

CSHierarchicalObject* CSHierarchicalObject::childAt(unsigned int index) const
{
	assert(index < child_objects_.size());
	return child_objects_[index];
}

void CSHierarchicalObject::removeChildAt(unsigned int index)
{
	assert(index < child_objects_.size());
	child_objects_[index]->setParent(0);
	child_objects_.removeAt(index);
}

void CSHierarchicalObject::removeChild(CSHierarchicalObject* obj)
{
	removeChildAt(indexOf(obj));
}


bool CSHierarchicalObject::hasParent() const
{
	return (!(parent_ == 0));
}

void CSHierarchicalObject::setParent(CSHierarchicalObject* parent)
{
	parent_ = parent;
}

int CSHierarchicalObject::indexOf(CSHierarchicalObject* const & obj) const
{
	return child_objects_.indexOf(obj);
}

bool CSHierarchicalObject::isEmpty() const
{
	return child_objects_.isEmpty();
}

bool CSHierarchicalObject::isSelected() const
{
	return selected_;
}

void CSHierarchicalObject::setSelected(bool selected)
{
	selected_ = selected;
}

void CSHierarchicalObject::toggleSelected()
{
	selected_ = !selected_;
}

void CSHierarchicalObject::removeAllChilds()
{
	foreach(CSHierarchicalObject* object, child_objects_)
	{
		object->setParent(0);
	}
	child_objects_.clear();
}

CSHierarchicalObject* CSHierarchicalObject::parent() const
{
	return parent_;
}

void CSHierarchicalObject::detachFromParent()
{
	if (parent_)
	{
		parent_->removeChild(this);
	}
}

int CSHierarchicalObject::childCount() const
{
	return child_objects_.size();
}

int CSHierarchicalObject::getIndex() const
{
	if (!parent_)
	{
		return -1;
	}
	else
	{
		
		return parent_->indexOf(const_cast<CSHierarchicalObject*>(this));
	}
}

bool CSHierarchicalObject::isValid() const
{
    return is_valid_;
}

void CSHierarchicalObject::setValidity(bool isValid)
{
    is_valid_ = isValid;
}

void CSHierarchicalObject::toggleValidity()
{
    is_valid_ = !is_valid_;
}

bool CSHierarchicalObject::isModified() const
{
	return is_modified_;
}

void CSHierarchicalObject::setModified(bool isModified)
{
	is_modified_ = isModified;
}

void CSHierarchicalObject::toggleModified()
{
	is_modified_ = !is_modified_;
}




