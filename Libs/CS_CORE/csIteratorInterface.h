#ifndef CSITERATORINTERFACE_H
#define CSITERATORINTERFACE_H

#include "cs_core_globe.h"


class CS_CORE_API CSIteratorInterface
{
public:
	virtual ~CSIteratorInterface(){}
	virtual void moveNext() = 0;
	virtual void reset() = 0;
	virtual unsigned int currentPos() const = 0;
	virtual bool isEnd() const = 0;
};


#endif