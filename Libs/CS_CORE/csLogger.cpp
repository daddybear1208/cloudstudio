#include <QtCore/QTime>
#include <QtCore/QTimer>
#include <QtCore/QFile>
#include <QtCore/QTextStream>
#include <QtCore/QMutexLocker>
#include <QtCore/QFileInfo>

#include <stdlib.h> // for getenv

#include "csLogger.h"


const QString kLogHeaderInformation("CloudStudio Log");

const int kSendMessageInterval = 500; // 500 ms

CSLogHandler* CSLogHandler::_instance = 0;

CSLogHandler *CSLogHandler::instance()
{
	static QMutex mutex;

	if (!_instance)
	{
		mutex.lock();

		if (!_instance)
			_instance = new CSLogHandler;

		mutex.unlock();
	}

	return _instance;
}

CSLogHandler::~CSLogHandler()
{
	static QMutex mutex;
	mutex.lock();
	_instance = 0;
	mutex.unlock();
}

CSLogHandler::CSLogHandler() :
_currLevel(LOG_DEBUG),
	_bufferized(true),
	_startEmission(false),
	_timer(NULL),
	_saveToLog(false)
{
	loadSettings();
	_timer = new QTimer(this);
	_timer->setInterval(kSendMessageInterval);
	connect( _timer,SIGNAL( timeout() ),this,SLOT( unqueueWaitingMessages() ) );
}

void CSLogHandler::reportMessage(LogLevel level, const QString &message )
{
	// message Ignored
	if (level < _currLevel)
		return;

	/* Firstly send message to file log */
	if (_saveToLog)
		fillAppLogFile(message);

	QString msgText( message );
	msgText.replace( "<", "&lt;" );
	msgText.replace( ">", "&gt;" );

	// add the current time
	msgText = QTime::currentTime().toString("[hh:mm:ss]: ") + msgText;
	QString msg;

	switch (level)
	{
	case LOG_DEBUG:
		{
			msg = QString( "<font color=\"blue\"><b>DEBUG</b></font>: " ) + msgText;
			break;
		}
	case LOG_INFO:
		{
			msg = QString( "<font color=\"green\"><b>INFO</b>: </font>" ) + msgText;
			break;
		}
	case LOG_WARNING:
		{
			msg = QString( "<font color=\"orange\"><b>WARNING</b>: </font>" ) + msgText;
			break;
		}
	case LOG_ERROR:
		{
			msg = QString( " <font color=\"red\"><b>ERROR</b>:</font>" ) + msgText;
			break;
		}
	default:
		{
			msg = msgText;
			break;
		}
	}

	if (_bufferized || !_startEmission)
		_buffer << msg;
	else
		emit newMessage(msg);
}

void CSLogHandler::reportDebug(const QString &message )
{
	QMutexLocker locker(&_lock);
	reportMessage(LOG_DEBUG,message);
}

void CSLogHandler::reportInfo(const QString &message )
{
	QMutexLocker locker(&_lock);
	reportMessage(LOG_INFO,message);
}

void CSLogHandler::reportWarning(const QString &message )
{
	QMutexLocker locker(&_lock);
	reportMessage(LOG_WARNING,message);
}

void CSLogHandler::reportError(const QString &message )
{
	QMutexLocker locker(&_lock);
	reportMessage(LOG_ERROR,message);
}

void CSLogHandler::setBufferization(bool val)
{
	_bufferized = val;
	if (!_bufferized) // send all stored messages
	{
		_timer->stop();
		unqueueWaitingMessages();
	}
	else
	{
		_timer->start();
	}
}

void CSLogHandler::startEmission(bool val)
{
	_startEmission = val;
	if (_startEmission && _bufferized)
		_timer->start();
	else
		_timer->stop();
}

void CSLogHandler::unqueueWaitingMessages()
{
	QMutexLocker locker(&_lock);
	if (_startEmission)
	{
		if ( !_buffer.isEmpty() )
		{
			emit newMessages(_buffer);
			_buffer.clear();
		}
	}
}

void CSLogHandler::loadSettings()
{
	QSettings settings(PACKAGE_ORGANIZATION, PACKAGE_NAME);

	settings.beginGroup("Application");
	settings.beginGroup("Log");

	_logDirectory = settings.value("LogDirectory", QApplication::applicationDirPath()).toString();

	settings.endGroup();
	settings.endGroup();
}
void CSLogHandler::setLogDirectory(const QString &path)
{
	if ( _logDirectory == path || !QFileInfo(path).exists() )
		return;

	_logDirectory = path;
	if (_saveToLog && _logFile)
	{
		_logFile->close();
		delete _logStream;
		_logStream = NULL;
		delete _logFile;
		_logFile = NULL;
	}
}

void CSLogHandler::setLogToFile(bool val)
{
	if (_saveToLog == val)
		return;

	_saveToLog = val;
	if (!_saveToLog && _logFile)
	{
		_logFile->close();
		delete _logStream;
		_logStream = NULL;
		delete _logFile;
		_logFile = NULL;
	}
}

bool CSLogHandler::fillAppLogFile(const QString &message)
{
	/* Assure one access on the file by a mutex */
	if(!_logStream)
	{
		/* Product a log error */
		QString cdate = QDate::currentDate().toString("dd-MM-yyyy");
		QString ctime = QTime::currentTime().toString("hh:mm");
		QString username = QString( getenv("USERNAME") );

		// try with USER environment variable
		if ( username.isEmpty() )
			username = QString( getenv("USER") );

		QString logName = QString("%1/%2.log").arg(_logDirectory).arg(PACKAGE_NAME);

		_logFile = new QFile(logName);

		// check the size of the file if exists => limit the log file to 10MBytes
		if (_logFile->size() > 10 * 1024 * 1024)
			_logFile->remove();

		if ( _logFile->open(QFile::WriteOnly | QFile::Append) )
		{
			_logStream = new QTextStream(_logFile);

			*_logStream << "-----------------------------------------------------" << "\n";
			*_logStream << kLogHeaderInformation << QString(" by %1. Begin session at (%2 - %3) \n").arg(username).arg(cdate).arg(ctime);
			*_logStream << "-----------------------------------------------------" << "\n\n";
		}
		else
		{
			emit newMessage( QString("<font color=\"red\"><b>Can't open for writting %1</b></font> <br>").arg(logName) );
		}
	}else{
		*_logStream << message << "\n";
		return true;
	}

	return false;
}

