#ifndef CSALGORITHMINTERFACE_H
#define CSALGORITHMINTERFACE_H

#include "csPluginBase.h"
#include <QtPlugin>
#include <QMap>
#include <QVector>
#include <QAction>

#include "csHierarchicalObject.h"


struct CSAlgorithmParameter
{
	CSAlgorithmParameter()
		:name("unnamed val"), value(0){}

	CSAlgorithmParameter(const QString& n, const QVariant& val)
		:name(n), value(val){}

	CSAlgorithmParameter(const CSAlgorithmParameter& para)
	{
		name = para.name;
		value = para.value;
	}

	CSAlgorithmParameter& operator=(const CSAlgorithmParameter& para)
	{
		name = para.name;
		value = para.value;
		return (*this);
	}

	QString name;
	QVariant value;
};


class CS_CORE_API CSAlgorithmInterface :public CSPluginBase
{
	Q_OBJECT
public:
	CSAlgorithmInterface() :CSPluginBase(), m_output(0), m_error("")
	{
		m_action = new QAction(QIcon(":/res/pluginDefaultIcon.png"), "plugin", 0);
	}

	virtual ~CSAlgorithmInterface()
	{
		while (!m_parameters.isEmpty())
		{
			delete m_parameters.back();
			m_parameters.pop_back();
		}
	}

	virtual CS_PLUGIN_TYPE pluginType() const
	{
		return CS_ALGORITHM_PLUGIN;
	}

	virtual CSObject::CS_OBJECT_TYPE inputType()
	{
		return CSObject::CS_POINTCLOUD;
	}

	virtual QAction* action() const
	{
		return m_action;
	}

	virtual const QVariant& parameterValue(const QString& name)
	{
		foreach(CSAlgorithmParameter* var, m_parameters)
		{
			if (var->name == name)
			{
				return var->value;
			}
		}
		return *(QVariant*)0;
	}

	virtual QStringList parameterNames() const
	{
		QStringList parameters;
		foreach(CSAlgorithmParameter* para, m_parameters)
		{
			parameters << para->name;
		}
		return parameters;
	}

	void reset()
	{
		m_input.clear();
		m_output = 0;
		int i(0);
		foreach(CSAlgorithmParameter* para, m_parameters)
		{
			para->value = m_defaultValues[i++];
		}
	}

	const QString& lastError() const
	{
		return m_error;
	}


public Q_SLOTS:

	virtual void addInput(CSHierarchicalObject* object)
	{
		m_input.append(object);
	}

	virtual void compute() = 0;

	virtual CSHierarchicalObject* getOutput()
	{
		return m_output;
	}

	virtual bool setParameterValue(const QString& name, const QVariant& value)
	{
		foreach(CSAlgorithmParameter* var, m_parameters)
		{
			if (var->name == name)
			{
				var->value = value;
				return true;
			}
		}
		return false;
	}

Q_SIGNALS:
	void computeFinished(bool isSuccessed);
	
protected:
	virtual bool addParameter(const QString& name, const QVariant& defaultVal)
	{
		foreach(CSAlgorithmParameter* var, m_parameters)
		{
			if (var->name == name)
				return false;
		}
		m_parameters.append(new CSAlgorithmParameter(name, defaultVal));
		m_defaultValues.append(defaultVal);
		return true;
	}

	QVector<CSHierarchicalObject*> m_input;
	CSHierarchicalObject* m_output;

	QAction* m_action;
	QString m_error;

private:
	QVector<CSAlgorithmParameter*> m_parameters;
	QVector<QVariant> m_defaultValues;
	Q_DISABLE_COPY(CSAlgorithmInterface)
};


QT_BEGIN_NAMESPACE

Q_DECLARE_INTERFACE(CSAlgorithmInterface,
"com.cvrs.CloudStudio.CSAlgorithmInterface/1.0")

QT_END_NAMESPACE

#endif