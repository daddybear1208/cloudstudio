#ifndef CSUTILITY_H
#define CSUTILITY_H


#include <stdlib.h>
#include <QDomElement>
#include <QColor>
#include <QFileInfo>
#include <QDateTime>

#ifdef _DEBUG
#include <QDebug>
#endif

#include "cs_core_globe.h"
#include <eigen3/Eigen/Core>

#define TIME_FORMAT "MM/dd/yyyy hh:mm:ss"

class CS_CORE_API CSCommonUtility
{
public:
	static void warning(const QString& message);
	static QString getSysUername();
	static QString getFileSuffix(const QString& fileName, bool complete);
};

class CS_CORE_API CSXmlUtility
{
public:
	static double doubleFromDom(const QDomElement& e, const QString& attribute, double defValue);

	static int intFromDom(const QDomElement& e, const QString& attribute, int defValue);

	static bool boolFromDom(const QDomElement& e, const QString& attribute, bool defValue);

	static QDomElement QColorDomElement(const QColor& color, const QString& name, QDomDocument& doc);

	static QColor QColorFromDom(const QDomElement& e);

	static QDateTime QDateTimeFromDom(const QDomElement& e);

	static QString QStringFromDom(const QDomElement& e);

};




#endif // !CSUTILITY_H
