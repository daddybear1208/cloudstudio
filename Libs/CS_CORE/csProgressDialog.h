#ifndef CSPROGRESSDIALOG_H
#define CSPROGRESSDIALOG_H

#include <QProgressDialog>
#include "cs_core_globe.h"

class CS_CORE_API CSProgessDialog :public QProgressDialog
{
public:
	CSProgessDialog(QWidget * parent = 0, Qt::WindowFlags f = 0);
	CSProgessDialog(const QString & labelText, const QString & cancelButtonText, int minimum, int maximum, QWidget * parent = 0, Qt::WindowFlags f = 0);
};


#endif