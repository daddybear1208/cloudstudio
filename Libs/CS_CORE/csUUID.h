#ifndef CSUUID_H
#define CSUUID_H

//#include <string>
//
//#include <stdio.h>
//#include <iostream>
//
//#ifdef WIN32
//#include <objbase.h>
//#else
//#include <uuid/uuid.h>
//#endif
//
//#include "cs_core_globe.h"
//
////typedef struct
////{
////	unsigned long Data1;
////	unsigned short Data2;
////	unsigned short Data3;
////	unsigned char Data4[8];
////}CSGUID,CSUUID;
//
////! generate GUID/UUID
//
//class CS_CORE_API CSUUID
//{
//public:
//	static GUID CreateGuid();
//	static std::string GuidToString(const GUID& guid);
//	//static GUID StringToGuid(std::string str);
//};

#include "cs_core_globe.h"
#include <QUuid>

class CS_CORE_API CSUUID
{
public:
	static QUuid createUuid();
};


#endif