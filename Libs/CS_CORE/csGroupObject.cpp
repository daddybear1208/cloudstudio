#include "csGroupObject.h"

CSGroupObject::CSGroupObject(const QString& name /*= "Unnamed Group"*/, bool persistent /*= false*/)
	:CSObject(name),
    CSHierarchicalObject(name),
    persistent_(persistent)
{

}


CSGroupObject::~CSGroupObject()
{

}


void CSGroupObject::dismissGroup()
{
	if (persistent_ || (!parent_))
	{
		return;
	}
	foreach(CSHierarchicalObject* obj, child_objects_)
	{
		obj->detachFromParent();
		parent_->appendChild(obj);
	}
}

QDomElement CSGroupObject::toDomElement(QDomDocument& document) const
{
	QDomElement element = document.createElement("Group");
	element.setAttribute("name", name());
	foreach(CSHierarchicalObject* obj, child_objects_)
	{
		element.appendChild(obj->toDomElement(document));
	}
	return element;
}

bool CSGroupObject::initFromDomElement(const QDomElement& element)
{
	return true;
}

CSObject::CS_OBJECT_TYPE CSGroupObject::objectType() const
{
	return CSObject::CS_GROUP;
}



