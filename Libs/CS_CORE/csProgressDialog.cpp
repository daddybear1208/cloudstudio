#include <csProgressDialog.h>

CSProgessDialog::CSProgessDialog(QWidget * parent /*= 0*/, Qt::WindowFlags f /*= 0*/)
	:QProgressDialog(parent, f)
{
	// default range: 0-100
	setRange(0, 100);
	// no cancel button
	setCancelButton(0);
	// delete on close
	setAttribute(Qt::WA_DeleteOnClose);
	// show immediately
	setMinimumDuration(0);
	setWindowModality(Qt::WindowModal);
	setAutoClose(false);
}


CSProgessDialog::CSProgessDialog(const QString & labelText, const QString & cancelButtonText, int minimum, int maximum, QWidget * parent /*= 0*/, Qt::WindowFlags f /*= 0*/)
	:QProgressDialog(labelText, cancelButtonText, minimum, maximum, parent, f)
{

}


