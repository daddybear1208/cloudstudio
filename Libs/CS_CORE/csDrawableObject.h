#ifndef CSDRAWABLEOBJECT_H
#define CSDRAWABLEOBJECT_H

#include "csObject.h"
#include "csBoundingBox.h"

#ifdef  Q_OS_WIN32
#include <windows.h>
#endif //  _WIN32

#ifdef Q_OS_MAC
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#else
#include <GL/gl.h>
#include <GL/glu.h>
#endif


class CS_CORE_API CSDrawableObject :virtual public CSObject
{
public:
	CSDrawableObject(const QString& name = QString());
	virtual ~CSDrawableObject();
    
    void setVisible(bool vis);
	bool isVisible() const;
	void toggleVisibility();

    void setBBValidity(bool isValid);
    bool isBBValid() const;
    void toggleBBValidity();
    
    void setBBVisibility(bool isVisible);
    bool isBBVisible() const;
    void toggleBBVisibility();
    
	
	virtual void draw(int level = 0) = 0;
	
	CSBoundingBox getBoundingBox();

	// need to be implemented
	virtual void drawBoundingBox();
	virtual void calculateBoundingBox() = 0;

protected:
	bool visibility_;
	CSBoundingBox bounding_box_;
	bool bb_validity_;
    bool bb_visibility_;
};



#endif // !CSDRAWABLEOBJECT_H
