#ifndef	CSVERSION_H
#define CSVERSION_H

#include <QString>
#include <QStringList>

#include "cs_core_globe.h"

#define PACKAGE_ORGANIZATION "CVRS of Wuhan University"
#define PACKAGE_NAME "CloudStudio"


//!	version control
//	MAJOR version when you make incompatible API changes
//	MINOR version when you add functionality in a backwards - compatible manner
//	PATCH version when you make backwards - compatible bug fixes
class CS_CORE_API CSVersion
{
public:

	enum CSVERSION_CMP_RESULUT
	{
		CS_VERSION_MAJOR_DIFF,
		CSVERSION_MINOR_DIFF,
		CSVERSION_COMPATIBLE
	};

	// default construct current CloudStudio version
	CSVersion(int major = 0, int minor = 0, int patch = 0);
	CSVersion(const QString& version);
	CSVersion(const CSVersion& version);
	CSVersion& operator=(const CSVersion& version);

	QString toString() const;
    bool isValid() const;
    int majorVersion() const;
    int minorVersion() const;
    int patchVersion() const;
    CSVERSION_CMP_RESULUT compare(const CSVersion& version);
    void clear();

	friend std::ostream& operator<<(std::ostream& os, const CSVersion& version);

private:

	int major_ver_num_;
	int minor_ver_num_;
	int patch_ver_num_;
};

#endif