#include "csFileReaderHelper.h"
#include "csLogger.h"

#include <QFileInfo>

CSFileReaderHelper* CSFileReaderHelper::instance()
{
	if (!instance_)
		instance_ = new CSFileReaderHelper;
	return instance_;
}

CS_FILE_TYPES CSFileReaderHelper::getFileType(const QString& filename) const
{
	QFileInfo fi(filename);
	QString suffix = fi.completeSuffix();
	if (file_ext_map_.contains(suffix))
	{
		return file_ext_map_.value(suffix);
	}
	return UNKNOWN_FILE;
}

CSFileReaderInterface* CSFileReaderHelper::getFileReader(CS_FILE_TYPES fileType) const
{
	if (!file_reader_map_.contains(fileType))
	{
		return 0;
	}
	else
		return file_reader_map_.value(fileType);
}

CSFileReaderInterface* CSFileReaderHelper::getFileReader(const QString& filename) const
{
	return getFileReader(getFileType(filename));
}

void CSFileReaderHelper::addFileReader(CSFileReaderInterface* reader)
{
	//if (file_reader_map_.contains(reader->fileType()))
	//{
	//	if (add_plugin_policy_ == ADD_FIRST)
	//	{
	//		CSLogHandler::instance()->reportWarning("File reader helper add_policy has been set to 'ADD_FIRST', plugin: '" +
	//			reader->description() + "' is not activated");
	//	}
	//	else
	//	{
	//		file_reader_map_[reader->fileType()] = reader;
	//	}
	//}
	//else file_reader_map_.insert(reader->fileType(), reader);
	
	file_reader_map_[reader->fileType()] = reader;
}

CSFileReaderHelper::CSFileReaderHelper()
{
	initializeFileExtMap();
	add_plugin_policy_ = ADD_LAST;
}

void CSFileReaderHelper::initializeFileExtMap()
{
	file_ext_map_.insert("txt", ASCII);
	file_ext_map_.insert("xyz", ASCII);
	file_ext_map_.insert("las", LAS);
	file_ext_map_.insert("ply", PLY);
}

void CSFileReaderHelper::setPluginAddPolicy(PLUGIN_ADD_POLICY policy)
{
	add_plugin_policy_ = policy;
}

CSFileReaderHelper::PLUGIN_ADD_POLICY CSFileReaderHelper::pluginAddPolicy() const
{
	return add_plugin_policy_;
}

QStringList CSFileReaderHelper::getFileExtension(CS_FILE_TYPES type) const
{
	QStringList fileExt;
	switch (type)
	{
	case UNKNOWN_FILE:
		break;
	case ASCII:
		fileExt << "txt" << "xyz";
		break;
	case PLY:
		fileExt << "ply";
		break;
	case OBJ:
		fileExt << "obj";
		break;
	case STL:
		fileExt << "stl";
		break;
	case PCD:
		fileExt << "pcd";
		break;
	case LAS:
		fileExt << "las";
		break;
	default:
		break;
	}
	return fileExt;
}

QList<CSFileReaderInterface*> CSFileReaderHelper::readers() const
{
	QList<CSFileReaderInterface*> readers;
	QMapIterator<CS_FILE_TYPES, CSFileReaderInterface*> iter(file_reader_map_);
	while (iter.hasNext())
	{
		iter.next();
		readers.append(iter.value());
	}
	return readers;
}

CSFileReaderHelper* CSFileReaderHelper::instance_ = 0;

