#ifndef POINTCLOUD_H
#define POINTCLOUD_H

#include <eigen3/Eigen/Core>

#include "cs_core_globe.h"
#include "csChunkedArray.h"
#include "csDrawableObject.h"
#include "csHierarchicalObject.h"


class CSOctreePointCloud;
class CSGridPointCloud;

//typedef CSChunkedArrayIterator<3, double> CSPointIterator;

class CS_CORE_API CSPointCloud: public CSHierarchicalObject, public CSDrawableObject
{
	friend class CSGridPointCloud;
	friend class CSOctreePointCloud;
public:

	CSPointCloud(const QString& name = "PointCloud");
	CSPointCloud(unsigned int pointCount, const QString& name = "PointCloud");
	CSPointCloud(const QString& fileName, const QString& type, const QString& name = "PointCloud");

	~CSPointCloud();


	CS_INLINE const QString& fileSource() const;
	CS_INLINE void setFileSource(const QString& file);

	// get points count
	CS_INLINE unsigned pointsCount() const;
	// get point size
	CS_INLINE const float& pointSize() const;
	// set point size
	CS_INLINE void setPointSize(float size);
	// add point
	CS_INLINE void addPoint(const Eigen::Vector3d& pt);
	CS_INLINE void addPoint(const double* pt);
	CS_INLINE void addPoint(double x, double y, double z);
	
	CS_INLINE void addColorField(const uchar* color);
	CS_INLINE void addColorField(uchar r, uchar g, uchar b);
	// get point data
	CS_INLINE Eigen::Vector3d getPoint(unsigned int index);
	CS_INLINE double* getPointData(unsigned int index);
	// reserve memory space for points
	CS_INLINE bool reservePoints(unsigned int pointNum);

	CS_INLINE bool setupColorField();
	CS_INLINE bool setupNormalField();

	CS_INLINE void clearColorField();
	// free redundant memory
	bool freeRedundantMem();
	// free memory
	CS_INLINE void clear();

	void draw(int level = 0);

	CS_INLINE virtual CSObject::CS_OBJECT_TYPE objectType() const;

	// inherited from CSHierarchicalObject
	virtual QDomElement toDomElement(QDomDocument& document) const;
	virtual bool initFromDomElement(const QDomElement& element);

	virtual void calculateBoundingBox();

// 	CS_INLINE CSPointIterator* getPointIterator() const
// 	{
// 		return points_->iterator();
// 	}

	
private:
	CSChunkedArray<3, double>* points_;
	CSChunkedArray<3, uchar>* color_;
	CSChunkedArray<3, float>* normals_;
	float point_size_;

	// source file name
	QString file_name_;
};


#endif // POINTCLOUD_H
