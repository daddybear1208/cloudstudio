#ifndef CSOCTREEPOINTCLOUD_H
#define CSOCTREEPOINTCLOUD_H

#include "csPointCloud.h"
#include "csOctreeNode.h"

class CS_CORE_API CSOctreePointCloud :public CSHierarchicalObject, public CSDrawableObject
{
public:
	explicit CSOctreePointCloud(const QString& name = "Octree");
	~CSOctreePointCloud();

	CSOctreeNode* rootNode() const;

	virtual void calculateBoundingBox();
	virtual void draw(int level = 0);

	virtual QDomElement toDomElement(QDomDocument& document) const;
	virtual bool initFromDomElement(const QDomElement& element);

	virtual CSObject::CS_OBJECT_TYPE objectType() const;

	void setAssociatePointCloud(CSPointCloud* pointCloud);
	CSPointCloud* associatePointCloud() const;

	void setRootNode(CSOctreeNode* root);

	unsigned int maxLeafPointCount();
	void setMaxLeafPointCount(unsigned int count);


protected:


private:
	CSOctreeNode* root_node_;
	CSPointCloud* associate_pointcloud_;
	unsigned int max_leaf_points_;
};

#endif