#ifndef CSPLUGINBASE_H
#define CSPLUGINBASE_H

#include <QObject>
#include <QString>
#include "cs_core_globe.h"
#include "csVersion.h"

class CSPluginBase : public QObject
{
public:
	
	enum CS_PLUGIN_TYPE
	{
		CS_FILE_READER_PLUGIN,
		CS_ALGORITHM_PLUGIN
	};


	CSPluginBase()
		:author_(""),
		description_(""),
		detailed_description_(""),
		version_(1,0,0){}

	const QString& author() const
	{
		return author_;
	}

	const QString& description() const
	{
		return description_;
	}

	const QString& detailedDescription() const
	{
		return detailed_description_;
	}


	virtual ~CSPluginBase()
	{

	}

	virtual CS_PLUGIN_TYPE pluginType() const = 0;

	const CSVersion& version() const
	{
		return version_;
	}

protected:

	QString author_;
	QString description_;
	QString detailed_description_;
	CSVersion version_;


private:

	Q_DISABLE_COPY(CSPluginBase)

};

#endif