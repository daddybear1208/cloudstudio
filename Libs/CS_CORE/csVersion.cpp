#include "csVersion.h"

CSVersion::CSVersion(int major /*= 0*/, int minor /*= 0*/, int patch /*= 0*/)
	:major_ver_num_(major),
	minor_ver_num_(minor),
	patch_ver_num_(patch)
{

}

CSVersion::CSVersion(const CSVersion& version)
{
	major_ver_num_ = version.major_ver_num_;
	minor_ver_num_ = version.minor_ver_num_;
	patch_ver_num_ = version.patch_ver_num_;
}

CSVersion::CSVersion(const QString& version)
{
	QStringList verNums = version.split('.');
	if (verNums.size() == 3)
	{
		major_ver_num_ = verNums.at(0).toInt();
		minor_ver_num_ = verNums.at(1).toInt();
		patch_ver_num_ = verNums.at(2).toInt();
		return;
	}
		
	major_ver_num_ = 0;
	minor_ver_num_ = 0;
	patch_ver_num_ = 0;
}


QString CSVersion::toString() const
{
	return QString("%1.%2.%3").arg(major_ver_num_).arg(minor_ver_num_)
		.arg(patch_ver_num_);
}

int CSVersion::majorVersion() const
{
	return major_ver_num_;
}

int CSVersion::minorVersion() const
{
	return minor_ver_num_;
}

int CSVersion::patchVersion() const
{
	return patch_ver_num_;
}

CSVersion::CSVERSION_CMP_RESULUT CSVersion::compare(const CSVersion& version)
{
	CSVERSION_CMP_RESULUT rst = CSVERSION_COMPATIBLE;
	if (major_ver_num_< version.majorVersion())
	{
		rst = CS_VERSION_MAJOR_DIFF;
	}else if (minor_ver_num_<version.minorVersion())
	{
		rst = CSVERSION_MINOR_DIFF;
	}

	return rst;
}


CSVersion& CSVersion::operator=(const CSVersion& version)
{
	if (this!=&version)
	{
		major_ver_num_ = version.major_ver_num_;
		minor_ver_num_ = version.minor_ver_num_;
		patch_ver_num_ = version.patch_ver_num_;
	}
	return (*this);
}


bool CSVersion::isValid() const
{
	return !(major_ver_num_ == 0 && 
		minor_ver_num_ == 0 && 
		patch_ver_num_ == 0);
}

void CSVersion::clear()
{
	major_ver_num_ = 0;
	minor_ver_num_ = 0;
	patch_ver_num_ = 0;
}


std::ostream& operator<<(std::ostream& os, const CSVersion& version)
{
	//os << version.major_ver_num_ << "." << version.minor_ver_num_ << "." << version.patch_ver_num_;
    //os<<version.major_ver_num_<<".";
	return os;
}
