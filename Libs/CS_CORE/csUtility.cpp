#include "csUtility.h"


void CSCommonUtility::warning(const QString& message)
{
	qWarning("%s", message.toLatin1().constData());
}

QString CSCommonUtility::getSysUername()
{
#if (defined Q_OS_LINUX) || (defined Q_OS_MAC)
	return QString(getenv("USER"));
#else
	return QString(getenv("USERNAME"));
#endif
}

QString CSCommonUtility::getFileSuffix(const QString& fileName, bool complete)
{
	QFileInfo fi(fileName);
	if (complete)
	{
		return fi.completeSuffix();
	}
	else
	{
		return fi.suffix();
	}
}


QColor CSXmlUtility::QColorFromDom(const QDomElement& e)
{
	int color[3];
	QStringList attribute;
	attribute << "red" << "green" << "blue";
	for (int i = 0; i < attribute.count(); ++i)
		color[i] = CSXmlUtility::intFromDom(e, attribute[i], 0);
	return QColor(color[0], color[1], color[2]);
}

QDomElement CSXmlUtility::QColorDomElement(const QColor& color, const QString& name, QDomDocument& doc)
{
	QDomElement de = doc.createElement(name);
	de.setAttribute("red", QString::number(color.red()));
	de.setAttribute("green", QString::number(color.green()));
	de.setAttribute("blue", QString::number(color.blue()));
	return de;
}

bool CSXmlUtility::boolFromDom(const QDomElement& e, const QString& attribute, bool defValue)
{
	bool value = defValue;
	if (e.hasAttribute(attribute))
	{
		const QString s = e.attribute(attribute);
		if (s.toLower() == QString("true"))
			value = true;
		else if (s.toLower() == QString("false"))
			value = false;
		else
		{
			CSCommonUtility::warning("Bad boolean syntax for attribute \"" + attribute + "\" in initialization of \"" + e.tagName() + "\" (should be \"true\" or \"false\").");
			CSCommonUtility::warning("Setting value to " + (value ? QString("true.") : QString("false.")));
		}
	}
	else
		CSCommonUtility::warning("\"" + attribute + "\" attribute missing in initialization of \"" + e.tagName() + "\". Setting value to " + (value ? QString("true.") : QString("false.")));
	return value;
}

int CSXmlUtility::intFromDom(const QDomElement& e, const QString& attribute, int defValue)
{
	int value = defValue;
	if (e.hasAttribute(attribute))
	{
		const QString s = e.attribute(attribute);
		bool ok;
		s.toInt(&ok);
		if (ok)
			value = s.toInt();
		else
			CSCommonUtility::warning("Bad integer syntax for attribute \"" + attribute + "\" in initialization of \"" + e.tagName() + "\". Setting value to " + QString::number(value) + ".");
	}
	else
		CSCommonUtility::warning("\"" + attribute + "\" attribute missing in initialization of \"" + e.tagName() + "\". Setting value to " + QString::number(value) + ".");
	return value;
}

double CSXmlUtility::doubleFromDom(const QDomElement& e, const QString& attribute, double defValue)
{
	double value = defValue;
	if (e.hasAttribute(attribute)) {
		const QString s = e.attribute(attribute);
		bool ok;
		value = s.toDouble(&ok);
		if (!ok) {
			CSCommonUtility::warning("Bad double syntax for attribute \"" + attribute + "\" in initialization of \"" + e.tagName() + "\". Setting value to " + QString::number(value) + ".");
			value = defValue;
		}
	}
	else
		CSCommonUtility::warning("\"" + attribute + "\" attribute missing in initialization of \"" + e.tagName() + "\". Setting value to " + QString::number(value) + ".");

	return value;
}

QDateTime CSXmlUtility::QDateTimeFromDom(const QDomElement& e)
{
	QDateTime dt;
	if (e.childNodes().count()==1 && e.firstChild().isText())
	{
		#ifdef _DEBUG
		qDebug() << e.tagName();
		qDebug() << e.firstChild().toText().data();
		#endif // _DEBUG
		dt.fromString(e.firstChild().toText().data(), TIME_FORMAT);
	}
	if (dt.isNull())
	{
		CSCommonUtility::warning("unable to convert element to a QDateTime");
	}
	return dt;
}

QString CSXmlUtility::QStringFromDom(const QDomElement& e)
{
	if (e.childNodes().count() == 1 && e.firstChild().isText())
	{
		return e.firstChild().toText().data();
	}
	return QString();
}


