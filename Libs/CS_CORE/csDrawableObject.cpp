#include "csDrawableObject.h"

CSDrawableObject::CSDrawableObject(const QString& name /*= QString()*/)
	:CSObject(name),
	visibility_(true),
	bb_validity_(false),
    bb_visibility_(false)
{

}

CSDrawableObject::~CSDrawableObject()
{

}

void CSDrawableObject::setVisible(bool vis)
{
	visibility_ = vis;
}

bool CSDrawableObject::isVisible() const 
{
	return visibility_;
}

void CSDrawableObject::toggleVisibility()
{
	visibility_ = !visibility_;
}

void CSDrawableObject::setBBValidity(bool isValid)
{
	bb_validity_ = isValid;
}

bool CSDrawableObject::isBBValid() const
{
	return bb_validity_;
}

void CSDrawableObject::toggleBBValidity()
{
	bb_validity_ = !bb_validity_;
}

void CSDrawableObject::setBBVisibility(bool isVisible)
{
    bb_visibility_ = isVisible;
}

bool CSDrawableObject::isBBVisible() const
{
    return bb_visibility_;
}

void CSDrawableObject::toggleBBVisibility()
{
    bb_visibility_ = !bb_visibility_;
}

CSBoundingBox CSDrawableObject::getBoundingBox()
{
	if (!bb_validity_)
	{
		calculateBoundingBox();
		bb_validity_ = true;
	}
	return bounding_box_;
}


void CSDrawableObject::drawBoundingBox()
{
	if (!bb_validity_) return;
	glColor3f(1.0, 0.0, 0.0);
	if (!bb_validity_ | bounding_box_.isEmpty())
	{
		calculateBoundingBox();
		bb_validity_ = true;
	}

	// draw bounding box using lower and upper corner
	GLC_Point3d lower = bounding_box_.lowerCorner();
	GLC_Point3d upper = bounding_box_.upperCorner();

	glBegin(GL_LINE_LOOP);
	glVertex3f((GLfloat)lower.x(), (GLfloat)lower.y(), (GLfloat)lower.z());
	glVertex3f((GLfloat)lower.x(), (GLfloat)lower.y(), (GLfloat)upper.z());
	glVertex3f((GLfloat)lower.x(), (GLfloat)upper.y(), (GLfloat)upper.z());
	glVertex3f((GLfloat)lower.x(), (GLfloat)upper.y(), (GLfloat)lower.z());
	glEnd();

	glBegin(GL_LINE_LOOP);
	glVertex3f((GLfloat)upper.x(), (GLfloat)upper.y(), (GLfloat)upper.z());
	glVertex3f((GLfloat)upper.x(), (GLfloat)lower.y(), (GLfloat)upper.z());
	glVertex3f((GLfloat)upper.x(), (GLfloat)lower.y(), (GLfloat)lower.z());
	glVertex3f((GLfloat)upper.x(), (GLfloat)upper.y(), (GLfloat)lower.z());
	glEnd();

	glBegin(GL_LINES);
	glVertex3f((GLfloat)lower.x(), (GLfloat)lower.y(), (GLfloat)lower.z());
	glVertex3f((GLfloat)upper.x(), (GLfloat)lower.y(), (GLfloat)lower.z());

	glVertex3f((GLfloat)lower.x(), (GLfloat)lower.y(), (GLfloat)upper.z());
	glVertex3f((GLfloat)upper.x(), (GLfloat)lower.y(), (GLfloat)upper.z());

	glVertex3f((GLfloat)lower.x(), (GLfloat)upper.y(), (GLfloat)upper.z());
	glVertex3f((GLfloat)upper.x(), (GLfloat)upper.y(), (GLfloat)upper.z());

	glVertex3f((GLfloat)lower.x(), (GLfloat)upper.y(), (GLfloat)lower.z());
	glVertex3f((GLfloat)upper.x(), (GLfloat)upper.y(), (GLfloat)lower.z());
	glEnd();
}

