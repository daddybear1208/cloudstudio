#ifndef CSGROUPOBJECT
#define CSGROUPOBJECT

#include "csHierarchicalObject.h"
#include "csDrawableObject.h"

class CS_CORE_API CSGroupObject:public CSHierarchicalObject
{
public:
	CSGroupObject(const QString& name = "Unnamed Group", bool persistent = false);
	~CSGroupObject();
	
	void dismissGroup();
	virtual inline CSObject::CS_OBJECT_TYPE objectType() const;

	// inherited from CSHierarchicalObject
	QDomElement toDomElement(QDomDocument& document) const;

	// do nothing but return true
	bool initFromDomElement(const QDomElement& element);

private:
	// a persistent group means it cannot be dismissed.
	bool persistent_;
};


#endif // !CSGROUPOBJECT
