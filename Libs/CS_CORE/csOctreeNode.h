#ifndef CSOCTREENODE_H
#define CSOCTREENODE_H

#include <cs_core_globe.h>
#include <assert.h>
#include <vector>
#include <csBoundingBox.h>
#include <csPointCloud.h>

typedef unsigned char uchar;

using std::vector;
class CSOctreePointCloud;

class CS_CORE_API CSOctreeNode
{
	friend class CSOctreePointCloud;

public:
    enum OCTREE_NODE_TYPE{
		BRANCH = 0,
		LEAF
	};


	explicit CSOctreeNode()
	{

	}

	virtual ~CSOctreeNode()
	{

	}

	virtual OCTREE_NODE_TYPE type() const = 0;

	const CSBoundingBox& boundingBox() const
	{
		return bounding_box_;
	}

	void setBoundingBox(const CSBoundingBox& box)
	{
		bounding_box_ = box;
	}

	virtual void drawSelf() = 0;


protected:
	CSBoundingBox bounding_box_;
};

class CS_CORE_API CSOctreeLeafNode : public CSOctreeNode
{
	friend class CSOctreePointCloud;
public:
	explicit CSOctreeLeafNode()
		:CSOctreeNode()
	{

	}
	virtual ~CSOctreeLeafNode()
	{

	}

	virtual void drawSelf()
	{
		glColor3f(1.0, 0.0, 0.0);
		// draw bounding box using lower and upper corner
		GLC_Point3d lower = bounding_box_.lowerCorner();
		GLC_Point3d upper = bounding_box_.upperCorner();

		glBegin(GL_LINE_LOOP);
		glVertex3d((GLdouble)lower.x(), (GLdouble)lower.y(), (GLdouble)lower.z());
		glVertex3d((GLdouble)lower.x(), (GLdouble)lower.y(), (GLdouble)upper.z());
		glVertex3d((GLdouble)lower.x(), (GLdouble)upper.y(), (GLdouble)upper.z());
		glVertex3d((GLdouble)lower.x(), (GLdouble)upper.y(), (GLdouble)lower.z());
		glEnd();

		glBegin(GL_LINE_LOOP);
		glVertex3d((GLdouble)upper.x(), (GLdouble)upper.y(), (GLdouble)upper.z());
		glVertex3d((GLdouble)upper.x(), (GLdouble)lower.y(), (GLdouble)upper.z());
		glVertex3d((GLdouble)upper.x(), (GLdouble)lower.y(), (GLdouble)lower.z());
		glVertex3d((GLdouble)upper.x(), (GLdouble)upper.y(), (GLdouble)lower.z());
		glEnd();

		glBegin(GL_LINES);
		glVertex3d((GLdouble)lower.x(), (GLdouble)lower.y(), (GLdouble)lower.z());
		glVertex3d((GLdouble)upper.x(), (GLdouble)lower.y(), (GLdouble)lower.z());

		glVertex3d((GLdouble)lower.x(), (GLdouble)lower.y(), (GLdouble)upper.z());
		glVertex3d((GLdouble)upper.x(), (GLdouble)lower.y(), (GLdouble)upper.z());

		glVertex3d((GLdouble)lower.x(), (GLdouble)upper.y(), (GLdouble)upper.z());
		glVertex3d((GLdouble)upper.x(), (GLdouble)upper.y(), (GLdouble)upper.z());

		glVertex3d((GLdouble)lower.x(), (GLdouble)upper.y(), (GLdouble)lower.z());
		glVertex3d((GLdouble)upper.x(), (GLdouble)upper.y(), (GLdouble)lower.z());
		glEnd();
	}
	

	OCTREE_NODE_TYPE type() const
	{
		return CSOctreeNode::LEAF;
	}

	unsigned int* getPointIndices()
	{
		return point_indices_vector_.data();
	}

	// const version of getPointIndices()
	CS_INLINE const unsigned int* getPointIndices() const
	{
		return point_indices_vector_.data();
	}

	CS_INLINE void addPointIndex(unsigned int index)
	{
		point_indices_vector_.push_back(index);
	}

	CS_INLINE int pointCount() const
	{
		return point_indices_vector_.size();
	}

	CS_INLINE void clear()
	{
		point_indices_vector_.clear();
	}

	std::vector<unsigned int>& pointIndexVector()
	{
		return point_indices_vector_;
	}

private:
	std::vector<unsigned int> point_indices_vector_;
};

class CS_CORE_API CSOctreeBranchNode : public CSOctreeNode
{
	friend class CSOctreePointCloud;
public:
	explicit CSOctreeBranchNode()
		:CSOctreeNode()
	{
		memset(child_nodes_, 0, sizeof(child_nodes_));
	}

	virtual ~CSOctreeBranchNode()
	{
		for (uchar i = 0; i < 8;i++)
		{
			if (child_nodes_[i])
			{
				delete child_nodes_[i];
				child_nodes_[i] = 0;
			}

		}
	}

	virtual void drawSelf()
	{
		for (uchar i = 0; i < 8; i++)
		{
			if (child_nodes_[i])
				child_nodes_[i]->drawSelf();
		}
	}

    OCTREE_NODE_TYPE type() const
	{
		return CSOctreeNode::BRANCH;
	}

	inline CSOctreeNode* childAt(uchar index) const
	{
		assert(index < 8);
		return child_nodes_[index];
	}

	inline void setChild(uchar index, CSOctreeNode* node)
	{
		assert(index < 8);
		child_nodes_[index] = node;
	}

	inline bool hasChild(uchar index)
	{
		assert(index < 8);
		return (child_nodes_[index] != 0);
	}

private:
	CSOctreeNode* child_nodes_[8];
};





#endif
