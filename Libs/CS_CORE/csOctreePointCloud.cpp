#include "csOctreePointCloud.h"


CSOctreePointCloud::CSOctreePointCloud(const QString& name)
	:CSObject(name),
	CSHierarchicalObject(name), CSDrawableObject(name),
	root_node_(0), associate_pointcloud_(0), max_leaf_points_(1000)
{

}

CSOctreePointCloud::~CSOctreePointCloud()
{
	if (root_node_)
		delete root_node_;
}

CSOctreeNode* CSOctreePointCloud::rootNode() const
{
	return root_node_;
}

QDomElement CSOctreePointCloud::toDomElement(QDomDocument& document) const
{
	return QDomElement();
}

bool CSOctreePointCloud::initFromDomElement(const QDomElement& element)
{
	return true;
}

CSObject::CS_OBJECT_TYPE CSOctreePointCloud::objectType() const
{
	return CSObject::CS_OCTREE_POINTCLOUD;
}

void CSOctreePointCloud::setAssociatePointCloud(CSPointCloud* pointCloud)
{
	associate_pointcloud_ = pointCloud;
}

CSPointCloud* CSOctreePointCloud::associatePointCloud() const
{
	return associate_pointcloud_;
}

void CSOctreePointCloud::calculateBoundingBox()
{
	if (!root_node_)
		return;
	bounding_box_ = root_node_->boundingBox();
}

void CSOctreePointCloud::draw(int level)
{
	if (!root_node_ || !isVisible())
		return;
	root_node_->drawSelf();
}

void CSOctreePointCloud::setRootNode(CSOctreeNode* root)
{
	root_node_ = root;
}

void CSOctreePointCloud::setMaxLeafPointCount(unsigned int count)
{
	max_leaf_points_ = count;
}

unsigned int CSOctreePointCloud::maxLeafPointCount()
{
	return max_leaf_points_;
}

