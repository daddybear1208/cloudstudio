#include "csBoundingBox.h"

#ifdef  Q_OS_WIN32
#include <windows.h>
#endif //  _WIN32

#ifdef Q_OS_MAC
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#else
#include <GL/gl.h>
#include <GL/glu.h>
#endif


CSBoundingBox::CSBoundingBox()
	:GLC_BoundingBox()
{

}

CSBoundingBox::CSBoundingBox(const CSBoundingBox& boundingBox)
	: GLC_BoundingBox(boundingBox)
{

}

CSBoundingBox::CSBoundingBox(const Eigen::Vector3d& lower, const Eigen::Vector3d& upper)
	: GLC_BoundingBox()
{
	setLowerCorner(lower);
	setUpperCorner(upper);
}

CSBoundingBox::CSBoundingBox(const GLC_Point3d& lower, const GLC_Point3d& upper)
	:GLC_BoundingBox(lower, upper)
{
	
}

CSBoundingBox::CSBoundingBox(double lowerX, double lowerY, double lowerZ, double upperX, double upperY, double upperZ)
{
	m_Lower = GLC_Point3d(lowerX, lowerY, lowerZ);
	m_Upper = GLC_Point3d(upperX, upperY, upperZ);
}



void CSBoundingBox::setLowerCorner(const Eigen::Vector3d& lower)
{
	if (m_IsEmpty)
	{
		m_IsEmpty = false;
	}
	memcpy(m_Lower.data(), lower.data(), 3 * sizeof(double));
}

void CSBoundingBox::setUpperCorner(const Eigen::Vector3d& upper)
{
	if (m_IsEmpty)
	{
		m_IsEmpty = false;
	}
	memcpy(m_Upper.data(), upper.data(), 3 * sizeof(double));
}

CSBoundingBox& CSBoundingBox::combine(const Eigen::Vector3d& point)
{
	GLC_Point3d glcPt(point[0], point[1], point[2]);
	GLC_BoundingBox::combine(glcPt);
	return (*this);
}

CSBoundingBox& CSBoundingBox::combine(const CSBoundingBox& bbox)
{
	GLC_BoundingBox::combine(bbox);
	return (*this);
}

CSBoundingBox& CSBoundingBox::transform(const Eigen::Matrix4d& matrix)
{
	// convert column major matrix to row major
	Eigen::Matrix<double, 4, 4, Eigen::RowMajor> rowMatrix = matrix;
	GLC_Matrix4x4 glcMatrix(rowMatrix.data());
	GLC_BoundingBox::transform(glcMatrix);
	return (*this);
}

CSBoundingBox& CSBoundingBox::operator=(const CSBoundingBox& bbox)
{
	GLC_BoundingBox::operator=(bbox);
	return (*this);
}
