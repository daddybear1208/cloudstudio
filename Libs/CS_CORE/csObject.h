#ifndef CSOBJECT_H
#define CSOBJECT_H

#include "cs_core_globe.h"
#include <assert.h>

#include <QObject>

QT_BEGIN_NAMESPACE
#include <QUuid>
#include <QString>
QT_END_NAMESPACE




class CS_CORE_API CSObject
{
public:
	explicit CSObject(const QString& name = QString());
	virtual ~CSObject();
    const QString& name() const;
    void setName(const QString& name);
	//virtual CSObject* clone() = 0;
    const QUuid& getUuid() const;

	enum CS_OBJECT_TYPE
	{
		/******************************base type*********************************/
		CS_OBJECT = 0,
		CS_HIERARCHICAL = 0x0000000000000001,
		CS_DRAWABLE = 0x0000000000000002,
		/************************************************************************/
		CS_GROUP = CS_HIERARCHICAL | 0x0000000000000004,
		CS_POINTCLOUD = CS_HIERARCHICAL | CS_DRAWABLE | 0x0000000000000008,
		CS_CAMERA = CS_HIERARCHICAL | CS_DRAWABLE | 0x0000000000000010,
		CS_OCTREE_POINTCLOUD = CS_HIERARCHICAL | CS_DRAWABLE | 0x0000000000000020,
		CS_GRID_POINTCLOUD = CS_HIERARCHICAL | CS_DRAWABLE | 0x0000000000000040
	};

	
	virtual CS_OBJECT_TYPE objectType() const = 0;
	bool isA(CS_OBJECT_TYPE objType) const;
	bool isKindOf(CS_OBJECT_TYPE objType) const;
	bool isGroup() const;
	bool isHierachical() const;

// 	bool operator== (const CSObject& other) const;
// 	bool operator!= (const CSObject& other) const;

private:
	QString object_name_;
	QUuid unique_id_;
};


#endif // !CSOBJECT_H
