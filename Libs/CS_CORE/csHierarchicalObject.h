#ifndef CSHIERARCHICALOBJECT_H
#define CSHIERARCHICALOBJECT_H

#include <QList>
#include <QDomNode>

#include "csObject.h"
#include "csUtility.h"

#include <qtpropertymanager.h>
#include <qtvariantproperty.h>



class CS_CORE_API CSHierarchicalObject : virtual public CSObject
{
public:
	explicit CSHierarchicalObject(const QString& name = QString());
	virtual ~CSHierarchicalObject();

public:
	// when appending a child, this function simply append the object into
	// the "child_objects_", so if the object already had a parent, make sure
	// to call the child's detachFromParent function first.
    void appendChild(CSHierarchicalObject* obj);

    int childCount() const;

	// when removing a child object, remove functions also set the child
	// object's parent to null using setParent() function.
    void removeChildAt(unsigned int index);
    void removeChild(CSHierarchicalObject* obj);
    void removeAllChilds();

    bool hasParent() const;
    CSHierarchicalObject* parent() const;
	// remove itself from the parent and set "parent_" to null.
    void detachFromParent();

    CSHierarchicalObject* childAt(unsigned int index) const;
    int indexOf(CSHierarchicalObject* const & obj) const;

    int getIndex() const;
    bool isEmpty() const;

    virtual bool isSelected() const;
    virtual void setSelected(bool selected);
    virtual void toggleSelected();
	
	// must be implemented in inherited class
	virtual QDomElement toDomElement(QDomDocument& document) const = 0;
	virtual bool initFromDomElement(const QDomElement& element) = 0;
    
    bool isValid() const;
    void setValidity(bool isValid);
    void toggleValidity();

	bool isModified() const;
	void setModified(bool isModified);
	void toggleModified();
    

protected:

	// only set the "parent_" property of this object.
	// not modifying the original parent's content.
    void setParent(CSHierarchicalObject* parent);
	QList<CSHierarchicalObject*> child_objects_;
	CSHierarchicalObject* parent_;
	bool selected_;
    bool is_valid_;
	QtVariantPropertyManager* property_manager_;
	bool is_modified_;
};


#endif // CSHIERARCHICALOBJECT_H