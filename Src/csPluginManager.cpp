#include "csPluginManager.h"
#include <CS_CORE/csLogger.h>
#include "csMainWindow.h"

CSPluginManager* CSPluginManager::instance()
{
	if (!instance_)
		instance_ = new CSPluginManager;
	return instance_;
}

void CSPluginManager::registerPlugin(const QString& pluginFileName)
{
	if (load_history_.contains(pluginFileName))
		return;
	load_history_ << pluginFileName;
	QPluginLoader loader(pluginFileName);
	QObject* plugin = loader.instance();
	if (!plugin) return;
	CSPluginBase* basePlugin = reinterpret_cast<CSPluginBase*>(plugin);
	if (!basePlugin)
		return;

	switch (basePlugin->pluginType())
	{
	case CSPluginBase::CS_FILE_READER_PLUGIN:
	{
		CSFileReaderInterface* fileReader =
			qobject_cast<CSFileReaderInterface*>(plugin);
		if (fileReader)
		{
			CSFileReaderInterface* existedReader = CSFileReaderHelper::instance()->getFileReader(fileReader->fileType());
			if (existedReader)
			{
				if (CSFileReaderHelper::instance()->pluginAddPolicy() == CSFileReaderHelper::ADD_FIRST)
				{
					failed_plugin_map_.insert(basePlugin, pluginFileName);
					CSLogHandler::instance()->reportWarning("File reader helper add_policy has been set to 'ADD_FIRST', plugin: '" +
						fileReader->description() + "' is not activated");
				}
				else
				{
					CSLogHandler::instance()->reportWarning("File reader helper add_policy has been set to 'ADD_LAST', plugin: '" +
						existedReader->description() + "' has been deactivated");
					QString existedReaderPath = loaded_plugin_map_.value(existedReader);

					loaded_plugin_map_.remove(existedReader);
					failed_plugin_map_.insert(existedReader, existedReaderPath);

					CSFileReaderHelper::instance()->addFileReader(fileReader);
					loaded_plugin_map_.insert(basePlugin, pluginFileName);

					CSLogHandler::instance()->
						reportInfo("Successfully load plugin: " + fileReader->description() + ": " + fileReader->detailedDescription());
				}
			}
			else
			{
				loaded_plugin_map_.insert(basePlugin, pluginFileName);
				CSFileReaderHelper::instance()->addFileReader(fileReader);
				CSLogHandler::instance()->
					reportInfo("Successfully load plugin: " + fileReader->description() + ": "+fileReader->detailedDescription());

			}

		}
		else
		{
			failed_plugin_map_.insert(basePlugin, pluginFileName);
			CSLogHandler::instance()->
				reportError(QString("Plugin '%1' not load correctly").arg(pluginFileName));
		}
		break;
	}
	case CSPluginBase::CS_ALGORITHM_PLUGIN:
	{
		CSAlgorithmInterface* alg = qobject_cast<CSAlgorithmInterface*>(plugin);
		if (alg)
		{
			CSMainWindow::instance()->menuAlgorithm->addAction(alg->action());
			CSMainWindow::instance()->toolBar->addAction(alg->action());
			CSMainWindow::instance()->algorithm_map_.insert(alg->action(), alg);
			QObject::connect(alg->action(), SIGNAL(triggered()), CSMainWindow::instance(), SLOT(processAlgorithm()));
			loaded_plugin_map_.insert(basePlugin, pluginFileName);


			CSLogHandler::instance()->
				reportInfo("Successfully load plugin: " + alg->description() + ": " + alg->detailedDescription());
		}
		else
		{
			failed_plugin_map_.insert(basePlugin, pluginFileName);
			CSLogHandler::instance()->
				reportError(QString("Plugin '%1' not load correctly").arg(pluginFileName));
		}
		break;
	}
	default:
		break;
	}

}

void CSPluginManager::registerPluginPath(const QString& filePath)
{
	QSettings setting("Application/Plugin");
	QStringList currentPluginPaths = setting.value("PluginDirs").toStringList();
	if (!currentPluginPaths.contains(filePath))
	{
		currentPluginPaths << filePath;
		setting.setValue("PluginDirs", currentPluginPaths);
	}

	QStringList candidates = findPlugins(filePath);
	foreach(const QString& file, candidates)
	{
		registerPlugin(file);
	}
}

CSPluginManager::CSPluginManager()
{

}

QStringList CSPluginManager::findPlugins(const QString& path)
{
	const QDir dir(path);
	if (!dir.exists())
		return QStringList();

	const QFileInfoList infoList = dir.entryInfoList(QDir::Files);
	if (infoList.empty())
		return QStringList();

	QStringList result;
	const QFileInfoList::const_iterator icend = infoList.constEnd();
	for (QFileInfoList::const_iterator it = infoList.constBegin(); it != icend; ++it) 
	{
		QString fileName;
		if (it->isSymLink())
		{
			const QFileInfo linkTarget = QFileInfo(it->symLinkTarget());
			if (linkTarget.exists() && linkTarget.isFile())
				fileName = linkTarget.absoluteFilePath();
		}
		else 
		{
			fileName = it->absoluteFilePath();
		}
		if (!fileName.isEmpty() && QLibrary::isLibrary(fileName) && !result.contains(fileName))
			result += fileName;
	}
	return result;
}

CSPluginManager* CSPluginManager::instance_ = 0;

