#include "csColorButton.h"
#include "csColorGrid.h"
#include "csPopupWidget.h"

#include <QPainter>
#include <QMouseEvent>
#include <QColorDialog>



CSColorButton::CSColorButton(QWidget *parent)
    : QToolButton(parent),
    m_modeLeft(PM_COLORGRID),
    m_modeRight(PM_COLORDIALOG),
    m_cellSize(12),
    m_colors(0)
{
    setColor(Qt::white);
}

CSColorButton::~CSColorButton()
{
}

void CSColorButton::setColor(const QColor& color)
{
    m_color = color;

    int w = qMax(size().width(), size().height());
    QPixmap pm(w,w);
    drawColorItem(pm, color);
    setIcon(QIcon(pm));

    setText(color.name());
}

void CSColorButton::drawColorItem(QPixmap &pm, const QColor& color)
{
    QPainter p(&pm);
    p.setBrush(color);
    p.setPen(palette().color(QPalette::Shadow));
    p.drawRect(pm.rect().adjusted(0,0,-1,-1));
}

void CSColorButton::setPickModeLeft(const PickMode& mode)
{
    m_modeLeft = mode;
}

void CSColorButton::setPickModeRight(const PickMode& mode)
{
    m_modeRight = mode;
}

void CSColorButton::resizeEvent ( QResizeEvent * event )
{
    setColor(m_color);
}

void CSColorButton::mousePressEvent ( QMouseEvent * event )
{
    QToolButton::mousePressEvent(event);

    event->accept();
    setDown(false);

    int mod;

    switch (event->button()) {
        case Qt::LeftButton:
            mod = m_modeLeft;
            break;
        case Qt::RightButton:
            mod = m_modeRight;
            break;
        default:
            return;
    }

    switch (mod)
    {
        case PM_COLORDIALOG:
        {
            QColor c = QColorDialog::getColor(m_color, this);
            if (c.isValid()) {
                setColor(c);
                emit colorChanged(c);
            }
        }
        break;

        case PM_COLORGRID:
        {
            CSColorGrid *grid = new CSColorGrid();
            grid->setPickByDrag(false);
            grid->setClickMode(CSColorGrid::CM_RELEASE);
            grid->setAutoSize(true);
            grid->setScheme(m_colors);
            grid->setCellSize(m_cellSize);
            connect(grid, SIGNAL(picked(const QColor &)), this, SLOT(setColor(const QColor&)));
            connect(grid, SIGNAL(picked(const QColor &)), this, SIGNAL(colorChanged(const QColor&)));

            CSPopupWidget *popup = new CSPopupWidget();
            popup->setWidget(grid);
            popup->show(mapToGlobal(rect().bottomLeft()));

            connect(grid, SIGNAL(accepted()), popup, SLOT(close()));
        }
        break;

        default:;
    }
}

void CSColorButton::setScheme(ColorList *scheme)
{
    m_colors = scheme;
}

void CSColorButton::setCellSize(int size)
{
    m_cellSize = size;
}


