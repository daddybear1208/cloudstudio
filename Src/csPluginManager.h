#ifndef CSPLUGINMANAGER_H
#define CSPLUGINMANAGER_H

#include <CS_CORE/csPluginBase.h>
#include <CS_CORE/csAlgorithmInterface.h>
#include <CS_CORE/csFileReaderHelper.h>
#include <QMap>
#include <QPluginLoader>
#include <QDir>

class CSPluginDialog;

class CSPluginManager
{
	friend class CSPluginDialog;
public:
	static CSPluginManager* instance();

	// try to register a plugin with plugin's filename
	void registerPlugin(const QString& pluginFileName);

	// register a new path will automatically register plugin inside the path
	void registerPluginPath(const QString& filePath);

	QStringList findPlugins(const QString &path);

protected:
	CSPluginManager();

private:
	static CSPluginManager* instance_;
	typedef QMap<CSPluginBase*, QString> PluginPathMap;

	PluginPathMap failed_plugin_map_;
	PluginPathMap loaded_plugin_map_;
	QStringList load_history_;
};

#endif