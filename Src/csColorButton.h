#ifndef CSCOLORBUTTON_H
#define CSCOLORBUTTON_H

#include <QList>
#include <QColor>
#include <QToolButton>

typedef QList<QColor> ColorList;


class CSColorGrid;

class CSColorButton : public QToolButton
{
    Q_OBJECT

    Q_PROPERTY(int cellSize READ cellSize WRITE setCellSize)
    Q_PROPERTY(QColor color READ color WRITE setColor)
    Q_PROPERTY(PickMode pickModeLeft READ pickModeLeft WRITE setPickModeLeft)
    Q_PROPERTY(PickMode pickModeRight READ pickModeRight WRITE setPickModeRight)

public:

    enum PickMode 
	{
        PM_NONE,
        PM_COLORDIALOG,
        PM_COLORGRID
    };

    CSColorButton(QWidget *parent = 0);
    virtual ~CSColorButton();
    inline const QColor& color() const { return m_color; }
    inline const PickMode& pickModeLeft() const { return m_modeLeft; }
    inline const PickMode& pickModeRight() const { return m_modeRight; }
    void setPickModeLeft(const PickMode& mode);
    void setPickModeRight(const PickMode& mode);
    inline ColorList* scheme() const { return m_colors; }
    void setScheme(ColorList *scheme);
    inline int cellSize() const { return m_cellSize; }
    void setCellSize(int size);

public slots:
    void setColor(const QColor& color);

signals:
    void colorChanged(const QColor &color);

protected:
    virtual void drawColorItem(QPixmap &pm, const QColor& color);

    virtual void resizeEvent(QResizeEvent *event);
    virtual void mousePressEvent(QMouseEvent *event);

    QColor m_color;
    PickMode m_modeLeft, m_modeRight;

    int m_cellSize;
    ColorList *m_colors;
};



#endif // COLORBUTTON_H
