#ifndef CSWIDGETPOPUP_H
#define CSWIDGETPOPUP_H

#include <QFrame>

class CSPopupWidget : public QFrame
{
    Q_OBJECT

public:
    CSPopupWidget(QWidget *parent = 0);
    void setWidget(QWidget *widget, bool own = true);

    inline QWidget* widget() const { return m_widget; }
    inline bool isOwned() const { return m_own; }

public slots:
    void show(QPoint coord);

protected:
    QWidget *m_widget;
    bool m_own;
    QWidget *m_oldParent;
};

#endif
