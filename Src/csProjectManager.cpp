#include "csProjectManager.h"


CSProjectManager* CSProjectManager::instance()
{
	if (!instance_)
		instance_ = new CSProjectManager();
	return instance_;
}

CSProjectManager::CSProjectManager()
	:current_project_(0)
{

}

CSProject* CSProjectManager::currentProject() const
{
	return current_project_;
}

CSProject* CSProjectManager::findProject(const QString& projectFileName) const
{
	if (project_map_.contains(projectFileName))
		return project_map_.value(projectFileName);
	return 0;
}

CSGLViewer* CSProjectManager::findGLViewer(CSProject* project) const
{
	if (!project)
		return 0;
	if (project_glviewer_map_.contains(project))
	{
		return project_glviewer_map_.value(project);
	}
	return 0;
}

CSGLViewer* CSProjectManager::findGLViewer(const QString& projectFileName) const
{
	CSProject* project = findProject(projectFileName);
	if (project)
	{
		return findGLViewer(project);
	}
	return 0;
}

CSProject* CSProjectManager::getProjectFromGLViewer(CSGLViewer* glviewer) const
{
	return glviewer_project_map_.value(glviewer);
}

CSGLViewer* CSProjectManager::currentGLViewer() const
{
	return findGLViewer(current_project_);
}

void CSProjectManager::setTreeViewDockWidget(QDockWidget* dockWidget)
{
	dockwidget_ = dockWidget;
}

void CSProjectManager::removeProject(CSProject* project)
{
	project_glviewer_map_.remove(project);
	project_model_map_.remove(project);
	project_treeview_map_.remove(project);
	glviewer_project_map_.remove(findGLViewer(project));
	project_map_.remove(project->getProjectFileName());
}

void CSProjectManager::addProject(CSProject* project, CSGLViewer* glviewer, CSProjectModel* model, CSProjectTreeView* treeview)
{
	if (project_glviewer_map_.contains(project))
	{
		return;
	}
	project_glviewer_map_.insert(project, glviewer);
	project_model_map_.insert(project, model);
	project_treeview_map_.insert(project, treeview);
	glviewer_project_map_.insert(glviewer, project);
	project_map_.insert(project->getProjectFileName(), project);
}

void CSProjectManager::setCurrentProject(CSProject* project)
{
	if (!project || project == current_project_)
		return;
	if (current_project_)
	{
		CSProjectTreeView* treeview = project_treeview_map_.value(current_project_);
		treeview->setVisible(false);
	}
	
	current_project_ = project;
	CSProjectTreeView* treeview = project_treeview_map_.value(current_project_);
	treeview->setVisible(true);
	dockwidget_->setWidget(treeview);
}

CSProjectModel* CSProjectManager::projectModel(CSProject* const & project)
{
	if (project_model_map_.contains(project))
	{
		return project_model_map_.value(project);
	}
	return 0;
}

CSProjectTreeView* CSProjectManager::currentProjectTreeView() const
{
	if (!current_project_)
	{
		return 0;
	}
	return project_treeview_map_.value(current_project_);
}

CSProjectModel* CSProjectManager::currentProjectModel() const
{
	if (!current_project_) return 0;
	return project_model_map_.value(current_project_);
}

CSProjectManager* CSProjectManager::instance_ = 0;

