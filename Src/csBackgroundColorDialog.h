#ifndef CSBACKGROUNDCOLORDIALOG_H
#define CSBACKGROUNDCOLORDIALOG_H

#include "ui_csBackgroundColorDialog.h"
#include <CS_VIS/csGLViewer.h>
#include <QButtonGroup>
#include <QColorDialog>

#include "csColorButton.h"

class CSBackgroundColorDialog : public QDialog, public Ui::CSBackgroundColorDialog
{
	Q_OBJECT
public:
	CSBackgroundColorDialog(CSGLViewer* viewer, QWidget* parent = 0);
	~CSBackgroundColorDialog();

public Q_SLOTS:
	void singleColorRadioBtnToggled(bool checked);

	virtual void reject();

private:
	CSGLViewer* glviewer_;
	QButtonGroup* btn_group_;


	CSColorButton* btnSingleColor;
	CSColorButton* btnGradientColor0;
	CSColorButton* btnGradientColor1;

	// original settings
	QColor single_color_;
	QColor gradient_color0_;
	QColor gradient_color1_;
	int color_mode_;
};

#endif