#ifndef CSPROJECT_H
#define CSPROJECT_H

#include <QDateTime>
#include <QtXml>
#include <QDomDocument>

#include <CS_CORE/csHierarchicalObject.h>
#include <CS_CORE/csGroupObject.h>
#include <CS_CORE/csUtility.h>
#include <CS_CORE/csVersion.h>
#include <CS_CORE/csLogger.h>
#include <CS_CORE/csPointCloud.h>
#include "csCommon.h"

class CSProject
{
public:
	CSProject(const QString& name = QString("New Project"));
	~CSProject();

	enum CS_PROJECT_LOAD_RST
	{
		CS_PROJECT_LOAD_OK = 0,
		CS_PROJECT_FILE_OPEN_ERR,
		CS_PROJECT_FILE_NOT_COMPATIBLE,
		CS_PROJECT_VERSION_NOT_COMPATIBLE
	};

	struct CSProjectGeneralInfo
	{
		QString creator;
		CSVersion cs_version;
		QDateTime creation_time;
	};
	
	const QString& getProjectName() const;
	void setProjectFileName(const QString& filename);
	void setProjectName(const QString& projectName);
	const QString& getCreator() const;
	const CSVersion& getCSVersion() const;
	const QDateTime& getCreationTime() const;
	const QString& getProjectFileName() const;
    CSGroupObject* projectRoot() const;
	void setModified(bool isMod);
	CS_PROJECT_LOAD_RST load(const QString& projectFileName);
	bool save();
	bool isModified() const;
	
private:
	CSHierarchicalObject* recursiveReadElement(const QDomElement& element);

private:
	CSProjectGeneralInfo general_info_;
	QString project_name_;
	QString project_file_name_;	
	bool modified_;
	CSGroupObject* project_root_;
};

#endif // !CSPROJECT_H
