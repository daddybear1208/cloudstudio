#ifndef CSPROJECTMODEL_H
#define CSPROJECTMODEL_H
 
#include <QAbstractItemModel>
#include "csProject.h"
#include "csProjectTreeView.h"
 
class CSProjectModel :public QAbstractItemModel
{
public:
	CSProjectModel(CSProject* project,CSProjectTreeView* treeView, QWidget* parent = 0);

	CSProject* getProject() const;
	CSGroupObject* modelRoot() const;

	void addObject(CSHierarchicalObject* object);
	void removeObject(CSHierarchicalObject* object, bool autoDelete = false);
 
	QModelIndex index(CSHierarchicalObject* object);
	QModelIndex index(int row, int column, const QModelIndex & parent = QModelIndex()) const;
	QModelIndex parent(const QModelIndex & index) const;
	int rowCount(const QModelIndex & parent = QModelIndex()) const;
	int columnCount(const QModelIndex & parent = QModelIndex()) const;
	QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;
	//virtual QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;
	Qt::ItemFlags flags(const QModelIndex & index) const;
	bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole);
	QModelIndexList getPersistentIndexList() const;

	QModelIndex indexFromUuid(const QUuid& uuid);
 
private:
	CSGroupObject* model_root_;
	CSProject* project_;
};
 
 
#endif // !CSPROJECTMODEL
