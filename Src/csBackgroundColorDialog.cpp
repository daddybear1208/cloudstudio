#include "csBackgroundColorDialog.h"

CSBackgroundColorDialog::CSBackgroundColorDialog(CSGLViewer* viewer, QWidget* parent)
	:QDialog(parent), glviewer_(viewer)
{
	setupUi(this);
	btnSingleColor = new CSColorButton;
	btnGradientColor0 = new CSColorButton;
	btnGradientColor1 = new CSColorButton;

	this->horizontalLayout->addWidget(btnSingleColor);
	this->horizontalLayout_2->addWidget(btnGradientColor0);
	this->horizontalLayout_3->addWidget(btnGradientColor1);


	setAttribute(Qt::WA_DeleteOnClose);

	btn_group_ = new QButtonGroup;
	btn_group_->addButton(this->radioButtonSingleColor);
	btn_group_->addButton(this->radioButtonGradientColor);


	connect(radioButtonSingleColor, SIGNAL(toggled(bool)), this, SLOT(singleColorRadioBtnToggled(bool)));
	color_mode_ = viewer->backgroundColorMode();
	single_color_ = viewer->backgroundColor();
	gradient_color0_ = viewer->gradientBgColor0();
	gradient_color1_ = viewer->gradientBgColor1();

	btnSingleColor->setColor(single_color_);
	btnGradientColor0->setColor(gradient_color0_);
	btnGradientColor1->setColor(gradient_color1_);

	connect(this->btnSingleColor, SIGNAL(colorChanged(const QColor&)), viewer, SLOT(setBackgroundColor(const QColor&)));
	connect(this->btnGradientColor0, SIGNAL(colorChanged(const QColor&)), viewer, SLOT(setGradientBgColor0(const QColor&)));
	connect(this->btnGradientColor1, SIGNAL(colorChanged(const QColor&)), viewer, SLOT(setGradientBgColor1(const QColor&)));


	if (viewer->backgroundColorMode() == CSGLViewer::SINGLE_COLOR)
	{
		this->radioButtonGradientColor->setChecked(true);
		frameSingleColor->setEnabled(true);
		frameGradientColor->setEnabled(false);
	}
	else
	{
		this->radioButtonGradientColor->setChecked(true);
		frameSingleColor->setEnabled(false);
		frameGradientColor->setEnabled(true);
	}

	
}

CSBackgroundColorDialog::~CSBackgroundColorDialog()
{
	delete btn_group_;
	delete btnGradientColor0;
	delete btnGradientColor1;
	delete btnSingleColor;
}

void CSBackgroundColorDialog::singleColorRadioBtnToggled(bool checked)
{
	if (checked)
	{
		frameSingleColor->setEnabled(true);
		frameGradientColor->setEnabled(false);
		glviewer_->setBackgroundColorMode(CSGLViewer::SINGLE_COLOR);
	}
	else
	{
		frameSingleColor->setEnabled(false);
		frameGradientColor->setEnabled(true);
		glviewer_->setBackgroundColorMode(CSGLViewer::GRADIENT_COLOR);
	}
}

void CSBackgroundColorDialog::reject()
{
	glviewer_->setBackgroundColorMode(CSGLViewer::BACKGROUND_MODE(color_mode_));
	glviewer_->setBackgroundColor(single_color_);
	glviewer_->setGradientBgColor(gradient_color0_, gradient_color1_);
	QDialog::reject();
}


