#ifndef CSALGORITHMCUSTOMIZER_H
#define CSALGORITHMCUSTOMIZER_H

#include <QWidget>
#include "csProject.h"
#include <CS_CORE/csAlgorithmInterface.h>
#include <propertybrowser/qtvariantproperty.h>
#include <propertybrowser/qttreepropertybrowser.h>

#include "ui_csAlgorithmCustomizer.h"

class CSAlgorithmCustomizer:public QDialog, public Ui::CSAlgorithmCustomizer
{
	Q_OBJECT
	
public:
	CSAlgorithmCustomizer(CSProject* project, CSAlgorithmInterface* alg, QWidget* parent = 0);
	~CSAlgorithmCustomizer();

protected:
	void recursiveAddInput(CSHierarchicalObject* object, CSObject::CS_OBJECT_TYPE type);
	void initParameters();
	

public Q_SLOTS:
	void algorithmFinished(bool successed);
	void onPropertyValueChanged(QtProperty *property, const QVariant &value);
	void addInputObjects();

private:
	CSProject* project_;
	CSAlgorithmInterface* alg_;
	QtTreePropertyBrowser* property_browser_;
	QtVariantPropertyManager* property_manager_;
	QMap<QString, CSHierarchicalObject*> object_map_;
};

#endif