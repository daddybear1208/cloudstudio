#ifndef CSSTYLESHEETEDITOR_H
#define CSSTYLESHEETEDITOR_H

#include <QDialog>

#include "ui_csStyleSheetEditor.h"

class CSStyleSheetEditor : public QDialog
{
    Q_OBJECT

public:
    CSStyleSheetEditor(QWidget *parent = 0);

private slots:
    void on_styleCombo_activated(const QString &styleName);
    void on_styleSheetCombo_activated(const QString &styleSheetName);
    void on_styleTextEdit_textChanged();
    void on_applyButton_clicked();

private:
    void loadStyleSheet(const QString &sheetName);
    Ui::CSStyleSheetEditor ui;
};

#endif
