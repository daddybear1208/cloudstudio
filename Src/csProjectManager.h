#ifndef CSPROJECTMANAGER_H
#define CSPROJECTMANAGER_H

#include <QMap>
#include <QDockWidget>
#include <CS_VIS/csGLViewer.h>

#include "csProject.h"
#include "csProjectTreeView.h"
#include "csProjectModel.h"

class CSProjectManager
{
public:
	static CSProjectManager* instance();

	void setTreeViewDockWidget(QDockWidget* dockWidget);

	void addProject(CSProject* project, CSGLViewer* glviewer, CSProjectModel* model, CSProjectTreeView* treeview);

	// get current activated project
	CSProject* currentProject() const;
	// find opened project
	CSProject* findProject(const QString& projectFileName) const;
	// find glviewer
	CSGLViewer* findGLViewer(CSProject* project) const;
	CSGLViewer* findGLViewer(const QString& projectFileName) const;

	CSProject* getProjectFromGLViewer(CSGLViewer* glviewer) const;

	CSGLViewer* currentGLViewer() const;

	void removeProject(CSProject* project);

	void setCurrentProject(CSProject* project);

	CSProjectModel* projectModel(CSProject* const & project);

	CSProjectTreeView* currentProjectTreeView() const;

	CSProjectModel* currentProjectModel() const;


protected:
	CSProjectManager();
private:
	static CSProjectManager* instance_;
	QMap<QString, CSProject*> project_map_;
	QMap<CSProject*, CSGLViewer*> project_glviewer_map_;
	QMap<CSGLViewer*, CSProject*> glviewer_project_map_;
	QMap<CSProject*, CSProjectTreeView*> project_treeview_map_;
	QMap<CSProject*, CSProjectModel*> project_model_map_;
	CSProject* current_project_;
	QDockWidget* dockwidget_;

};


#endif