#ifndef CSPLUGINDIALOG_H
#define CSPLUGINDIALOG_H
#include "ui_csPluginDialog.h"
#include "csPluginManager.h"

class CSPluginDialog :public QDialog, public Ui::CSPluginDialog
{
	Q_OBJECT
public:
	CSPluginDialog(QWidget* parent = 0);
	~CSPluginDialog();

public Q_SLOTS:
	void addPluginFile();
	void addPluginPath();
	void refresh();

protected:
	void initializePlugins();
};


#endif // !CSPLUGINDIALOG_H
