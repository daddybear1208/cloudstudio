#include "csAlgorithmCustomizer.h"
#include "csProjectManager.h"

CSAlgorithmCustomizer::CSAlgorithmCustomizer(CSProject* project, CSAlgorithmInterface* alg, QWidget* parent)
	:QDialog(parent),
	project_(project),
	alg_(alg)
{
	setupUi(this);
	setWindowTitle(windowTitle() + "--" + alg->action()->text());
	setAttribute(Qt::WA_DeleteOnClose);
	connect(btnClose, SIGNAL(clicked()), this, SLOT(close()));
	connect(btnCompute, SIGNAL(clicked()), alg, SLOT(compute()));

	recursiveAddInput(project->projectRoot(), alg->inputType());
	property_browser_ = new QtTreePropertyBrowser();
	this->horizontalLayout_6->addWidget(property_browser_);


	QStringList labels;
	labels.append(QApplication::translate("QtTreePropertyBrowser", "Parameter", 0, QApplication::UnicodeUTF8));
	labels.append(QApplication::translate("QtTreePropertyBrowser", "Value", 0, QApplication::UnicodeUTF8));
	property_browser_->setHeaderLabels(labels);
	property_browser_->setHeaderVisible(true);
	property_browser_->setRootIsDecorated(false);
	property_browser_->setPropertiesWithoutValueMarked(true);

	property_manager_ = new QtVariantPropertyManager;
	QtVariantEditorFactory* factory = new QtVariantEditorFactory;
	property_browser_->setFactoryForManager(property_manager_, factory);

	initParameters();

	connect(property_manager_, SIGNAL(valueChanged(QtProperty*, const QVariant &)),
		this, SLOT(onPropertyValueChanged(QtProperty*, const QVariant&)));

	connect(alg_, SIGNAL(computeFinished(bool)), this, SLOT(algorithmFinished(bool)));

	connect(btnAddInput, SIGNAL(clicked()), this, SLOT(addInputObjects()));
}


CSAlgorithmCustomizer::~CSAlgorithmCustomizer()
{
	alg_->reset();
	delete property_browser_;
	delete property_manager_;
}

void CSAlgorithmCustomizer::recursiveAddInput(CSHierarchicalObject* object, CSObject::CS_OBJECT_TYPE type)
{
	if (object->isKindOf(type))
	{
		this->comboBoxInput->addItem(object->name());
		object_map_.insert(object->name(), object);
	}
	else if (object->isGroup())
	{
		CSGroupObject* group = dynamic_cast<CSGroupObject*>(object);
		for (int i(0); i < group->childCount(); ++i)
		{
			recursiveAddInput(group->childAt(i), type);
		}
	}
}

void CSAlgorithmCustomizer::algorithmFinished(bool successed)
{
	if (!successed)
	{
		CSLogHandler::instance()->reportError("Algorithm process failed with message:" + alg_->lastError());
		return;
	}
	else
	{
		CSLogHandler::instance()->reportInfo("Algorithm process successed");
		CSHierarchicalObject* output = alg_->getOutput();
		if (output)
		{
			CSProjectManager::instance()->projectModel(project_)->addObject(output);
			CSProjectManager::instance()->findGLViewer(project_)->refreshSceneContent();
		}
	}
}

void CSAlgorithmCustomizer::initParameters()
{
	QtVariantProperty* prop = 0;
	foreach(QString paraName, alg_->parameterNames())
	{
		QVariant para = alg_->parameterValue(paraName);
		prop = property_manager_->addProperty(para.type(), paraName);
		if (!prop)
		{
			CSLogHandler::instance()->reportError(alg_->action()->text() + ": parameters [" + paraName +"] initialization faild, type not supported!");
			continue;
		}
		prop->setValue(para);
		prop->setEnabled(true);
		property_browser_->addProperty(prop);
	}
	
}

void CSAlgorithmCustomizer::onPropertyValueChanged(QtProperty *property, const QVariant &value)
{
	alg_->setParameterValue(property->propertyName(), value);
}

void CSAlgorithmCustomizer::addInputObjects()
{
	if (this->inputListWidget->findItems(this->comboBoxInput->currentText(), 0).size())
		return;

	inputListWidget->addItem(new QListWidgetItem(this->comboBoxInput->currentText()));
	alg_->addInput(object_map_.value(this->comboBoxInput->currentText()));
}

