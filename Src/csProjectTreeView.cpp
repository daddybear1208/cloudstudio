#include "csProjectTreeView.h"
#include "csProjectModel.h"

CSProjectTreeView::CSProjectTreeView(QtTreePropertyBrowser* propertyBrowser, QWidget* parent /*= 0*/)
	:QTreeView(parent),
	property_browser_(propertyBrowser),
	selected_object_(0)
{
	setHeaderHidden(true);
	property_manager_ = new QtVariantPropertyManager;
	QtVariantEditorFactory* factory = new QtVariantEditorFactory;
	property_browser_->setFactoryForManager(property_manager_, factory);
	// add context menu
	setContextMenuPolicy(Qt::CustomContextMenu);
	connect(this, SIGNAL(customContextMenuRequested(const QPoint&)), 
		this, SLOT(onCustomContextMenuRequested(const QPoint&)));
}

CSProjectTreeView::~CSProjectTreeView()
{
	if (property_manager_)
		delete property_manager_;
}

void CSProjectTreeView::selectionChanged(const QItemSelection& selected, const QItemSelection& deselected)
{
	//QTreeView::selectionChanged(selected, deselected);
	QModelIndexList deselectedIndexes = deselected.indexes();
	foreach(QModelIndex index, deselectedIndexes)
	{
		CSHierarchicalObject* object = static_cast<CSHierarchicalObject*>(index.internalPointer());
		object->setSelected(false);
	}

	property_browser_->clear();
	property_manager_->clear();
	QItemSelectionModel* selection = this->selectionModel();
	QModelIndexList selectedIndexs = selection->selectedIndexes();

	if (selectedIndexs.size() == 1)
	{
		CSProjectModel* projectModel = dynamic_cast<CSProjectModel*>(this->model());
		QModelIndex objectIndex = selectedIndexs.at(0);
		CSProject* project = projectModel->getProject();
		if (!project)
			return;
		selected_object_ = static_cast<CSHierarchicalObject*>(objectIndex.internalPointer());
		selected_object_->setSelected(true);
		if (selected_object_ == project->projectRoot())
		{
			// show project properties
			QtVariantProperty* prop = property_manager_->addProperty(QVariant::String, "Creator");
			prop->setValue(project->getCreator());
			prop->setEnabled(false);
			property_browser_->addProperty(prop);

			prop = property_manager_->addProperty(QVariant::DateTime, "Creation Time");
			prop->setEnabled(false);
			prop->setValue(project->getCreationTime());
			property_browser_->addProperty(prop);

			prop = property_manager_->addProperty(QVariant::String, "CloudStudio Version");
			prop->setEnabled(false);
			prop->setValue(project->getCSVersion().toString());
			property_browser_->addProperty(prop);

			// Future: change to file path property editor
			prop = property_manager_->addProperty(QVariant::String, "Project Source");
			prop->setValue(project->getProjectFileName());
			property_browser_->addProperty(prop);
		}
		else
		{
			// show object properties
			QtVariantProperty* prop = property_manager_->addProperty(QVariant::String, "Name");
			prop->setValue(selected_object_->name());
			property_browser_->addProperty(prop);

			prop = property_manager_->addProperty(QVariant::String, "UUID");
			prop->setValue(selected_object_->getUuid().toString());
			prop->setEnabled(false);
			property_browser_->addProperty(prop);

			prop = property_manager_->addProperty(QVariant::String, "Valid");
			prop->setValue(selected_object_->isValid() ? "True" : "False");
			prop->setEnabled(false);
			property_browser_->addProperty(prop);

			prop = property_manager_->addProperty(QtVariantPropertyManager::enumTypeId(), "Selected");
			QStringList selectedEnum;
			selectedEnum << "True" << "False";
			prop->setAttribute(QLatin1String("enumNames"), selectedEnum);
			prop->setValue(selected_object_->isSelected() ? 0 : 1);
			property_browser_->addProperty(prop);

			if (selected_object_->isKindOf(CSObject::CS_DRAWABLE))
			{
				CSDrawableObject* drawable = reinterpret_cast<CSDrawableObject*>(selected_object_);
				if (drawable)
				{
					prop = property_manager_->addProperty(QVariant::Bool, "Visible");
					prop->setEnabled(true);
					prop->setValue(drawable->isVisible());
					property_browser_->addProperty(prop);


					//CSBoundingBox bbox = drawable->getBoundingBox();
					//prop = property_manager_->addProperty(QVariant::Vector3D, "BB Min");
					//prop->setEnabled(false);
					//prop->setValue(QVector3D(bbox.lowerCorner().x(), bbox.lowerCorner().y(), bbox.lowerCorner().z()));
					//property_browser_->addProperty(prop);

					//prop = property_manager_->addProperty(QVariant::Vector3D, "BB Max");
					//prop->setEnabled(false);
					//prop->setValue(QVector3D(bbox.upperCorner().x(), bbox.upperCorner().y(), bbox.upperCorner().z()));
					//property_browser_->addProperty(prop);
				}
			}

			if (selected_object_->isA(CSObject::CS_POINTCLOUD))
			{
				CSPointCloud* pc = dynamic_cast<CSPointCloud*>(selected_object_);
				prop = property_manager_->addProperty(QVariant::Int, "Point Count");
				prop->setValue(pc->pointsCount());
				prop->setEnabled(false);
				property_browser_->addProperty(prop);

				prop = property_manager_->addProperty(QVariant::Double, "Point Size");
				prop->setValue(pc->pointSize());
				property_browser_->addProperty(prop);

			}



		}
	}
	else
	{
		foreach(QModelIndex objectIndex, selectedIndexs)
		{
			CSHierarchicalObject* object = static_cast<CSHierarchicalObject*>(objectIndex.internalPointer());
			object->setSelected(true);
		}
	}
	emit glviewerRefreshSignal();
}

void CSProjectTreeView::onPropertyValueChanged(QtProperty *property, const QVariant &value)
{
	if (!selected_object_)
		return;
    if(selected_object_->isA(CSObject::CS_POINTCLOUD))
    {
        CSPointCloud* pointCloud = dynamic_cast<CSPointCloud*>(selected_object_);
        if (property->propertyName() == "Point Size")
        {
            pointCloud->setPointSize(value.toFloat());
        }
		if (property->propertyName() == "Visible")
		{
			pointCloud->setVisible(value.toBool());
		}
    }
    
    emit glviewerRefreshSignal();

}



void CSProjectTreeView::onCustomContextMenuRequested(const QPoint & pos)
{

}



void CSProjectTreeView::setVisible(bool visible)
{
	QTreeView::setVisible(visible);
	if (visible)
	{
		connect(property_manager_, SIGNAL(valueChanged(QtProperty*, const QVariant &)),
			this, SLOT(onPropertyValueChanged(QtProperty*, const QVariant&)));
	}
	else
	{
		disconnect(property_manager_, SIGNAL(valueChanged(QtProperty *, const QVariant &)),
			this, SLOT(onPropertyValueChanged(QtProperty*, const QVariant&)));
		property_browser_->clear();
	}
}

CSHierarchicalObject* CSProjectTreeView::selectedObject() const
{
	return selected_object_;
}
