#include "csProjectModel.h"


CSProjectModel::CSProjectModel(CSProject* project, CSProjectTreeView* treeView, QWidget* parent /*= 0*/)
:QAbstractItemModel(parent),
project_(project)

{
	model_root_ = new CSGroupObject("Root", true);
	treeView->setModel(this);
	if (project)
	{
		model_root_->appendChild(project->projectRoot());
		emit layoutChanged();
	}
}


QModelIndex CSProjectModel::index(int row, int column, const QModelIndex & parent /*= QModelIndex()*/) const
{
	if (!model_root_)
	{
		return QModelIndex();
	}
	if (!hasIndex(row, column, parent))
		return QModelIndex();
	CSHierarchicalObject* parentItem = 0;
	if (!parent.isValid())
	{
		parentItem = model_root_;
	}
	else
	{
		parentItem = static_cast<CSHierarchicalObject*>(parent.internalPointer());
	}
	if (!parentItem)
	{
		return QModelIndex();
	}
	CSHierarchicalObject* childItem = parentItem->childAt(row);
	if (childItem)
	{
		return createIndex(row, column, childItem);
	}
	else
	{
		return QModelIndex();
	}
}

QModelIndex CSProjectModel::index(CSHierarchicalObject* object)
{
	if (object == model_root_)
	{
		return QModelIndex();
	}
	CSHierarchicalObject* parentObj = object->parent();
	if (!parentObj) return QModelIndex();
	int row = parentObj->indexOf(object);
	return createIndex(row, 0, object);
}


QModelIndex CSProjectModel::parent(const QModelIndex & index) const
{
	if (!model_root_)
 		return QModelIndex();
	if (!index.isValid())
	{
 		return QModelIndex();
	}
	CSHierarchicalObject* hObj = static_cast<CSHierarchicalObject*>(index.internalPointer());
	if (!hObj)
	{
 		return QModelIndex();
	}
	else
	{
 		CSHierarchicalObject* parent = hObj->parent();
 		if ((!parent) || (parent==model_root_))
 		{
 			return QModelIndex();
 		}
 		else
 		{
 			return createIndex(parent->getIndex(), 0, parent);
 		}
	}
}
 
int CSProjectModel::rowCount(const QModelIndex & parent /*= QModelIndex()*/) const
{
	if (!model_root_)
	{
		return 0;
	}
	CSHierarchicalObject* parentItem = 0;
	if (!parent.isValid())
		parentItem = model_root_;
	else
		parentItem = static_cast<CSHierarchicalObject*>(parent.internalPointer());
	if (!parentItem)
	{
		return 0;
	}
	else
	{
		return parentItem->childCount();
	}
}
 
 
int CSProjectModel::columnCount(const QModelIndex & parent /*= QModelIndex()*/) const
{
	return 1;
}
 
QVariant CSProjectModel::data(const QModelIndex & index, int role /*= Qt::DisplayRole*/) const
{
	if (!model_root_)
	{
		return QVariant();
	}
	if (!index.isValid())
	{
		return QVariant();
	}
	if (!project_)
	{
		return QVariant();
	}
	
	CSHierarchicalObject* item = static_cast<CSHierarchicalObject*>(index.internalPointer());
	if (!item)
	{
		return QVariant();
	}

	switch (role)
	{
		case Qt::DisplayRole :
		{
			return item->name();
			break;
		}

		case Qt::EditRole:
		{
			return item->name();
			break;
		}
//
//        case Qt::UserRole:
//        {
//            return item->getUuid();
//            break;
//        }

		case Qt::ForegroundRole:
		{
			if (item->isValid())
			{
				return QColor(Qt::green);
			}
			else
				return QColor(Qt::red);
		}

		case Qt::DecorationRole:
		{
			if (item == project_->projectRoot())
			{
				return QIcon(":/ProjectTree/ProjectTreeUI/projectIcon.png");
			}
            else if (item->isA(CSObject::CS_POINTCLOUD))
			{
				return QIcon(":/ProjectTree/ProjectTreeUI/pointCloudIcon.png");
			}
			else if (item->isA(CSObject::CS_CAMERA))
			{
				return QIcon(":/ProjectTree/ProjectTreeUI/cameraIcon.png");
			}
			else if (item->isA(CSObject::CS_GROUP))
			{
				return QIcon(":/ProjectTree/ProjectTreeUI/groupIcon.png");
			}
			
			break;
		}

	}
	return QVariant();
}

 
Qt::ItemFlags CSProjectModel::flags(const QModelIndex & index) const
{
	if (!index.isValid())
		return Qt::ItemIsEnabled;

	return QAbstractItemModel::flags(index) | Qt::ItemIsEditable;
}

CSProject* CSProjectModel::getProject() const
{
	return project_;
}

CSGroupObject* CSProjectModel::modelRoot() const
{
	return model_root_;
}

QModelIndexList CSProjectModel::getPersistentIndexList() const
{
	return persistentIndexList();
}

QModelIndex CSProjectModel::indexFromUuid(const QUuid& uuid)
{
	QModelIndexList indexes = persistentIndexList();
	foreach(QModelIndex index, indexes)
	{
		CSHierarchicalObject* object = static_cast<CSHierarchicalObject*>(index.internalPointer());
		if (object->getUuid() == uuid)
		{
			return index;
		}
	}
	return QModelIndex();

}

void CSProjectModel::removeObject(CSHierarchicalObject* object, bool autoDelete /*= false*/)
{
	if (object == project_->projectRoot())
		return;
	if (!object->hasParent())
		return;
	CSHierarchicalObject* parentObj = object->parent();
	QModelIndex parentIndex = index(parentObj);
	int index = parentObj->indexOf(object);
	beginRemoveRows(parentIndex, index, index);
	parentObj->removeChildAt(index);
	endInsertRows();
	if (autoDelete)
		delete object;
}

void CSProjectModel::addObject(CSHierarchicalObject* object)
{
	CSHierarchicalObject* parent = 0;
	if (!object->hasParent())
	{
		parent = project_->projectRoot();
		parent->appendChild(object);
	}
	else
	{
		parent = object->parent();
	}
	
	int row = parent->indexOf(object);
	QModelIndex parentIndex = index(parent);
	beginInsertRows(parentIndex, row, row);
	endInsertRows();
}

bool CSProjectModel::setData(const QModelIndex &index, const QVariant &value, int role /*= Qt::EditRole*/)
{
	if (!index.isValid())
	{
		return false;
	}
	switch (role)
	{
	case Qt::EditRole:
	{
		CSHierarchicalObject* object = static_cast<CSHierarchicalObject*>(index.internalPointer());
		if (!object)
			return false;
		if (object == project_->projectRoot())
		{
			project_->setProjectName(value.toString());
		}
		else
		{
			object->setName(value.toString());
		}
		emit dataChanged(index, index);
		return true;
	}
	default:
		return false;
		break;
	}
	return false;
}

