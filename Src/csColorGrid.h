#ifndef CSCOLORGRID_H
#define CSCOLORGRID_H

#include <QWidget>

#include <QList>
#include <QColor>

typedef QList<QColor> ColorList;

class CSColorGrid : public QWidget
{
    Q_OBJECT

    Q_PROPERTY(int cellSize READ cellSize WRITE setCellSize)
    Q_PROPERTY(int widthInCells READ widthInCells WRITE setWidthInCells)
    Q_PROPERTY(bool autoSize READ autoSize WRITE setAutoSize)
    Q_PROPERTY(bool pickByDrag READ pickByDrag WRITE setPickByDrag)
    Q_PROPERTY(ClickMode clickMode READ clickMode WRITE setClickMode)

public:
    enum ClickMode 
	{
        CM_PRESS = 0,
        CM_RELEASE
    };


    CSColorGrid(QWidget *parent = 0);

    virtual ~CSColorGrid();

    virtual QSize minimumSizeHint() const;
    virtual QSize sizeHint() const;



    inline int cellSize() const { return m_cellSize; }

    void setCellSize(int size);

    inline int widthInCells() const { return m_widthInCells; }

    void setWidthInCells(int width);

    int heightInCells() const;

    inline bool autoSize() const { return m_autoSize; }

    void setAutoSize(bool autosize);

    inline const QColor &lastHighlighted() const { return m_hlColor; }
    inline const QColor &lastPicked() const { return m_selColor; }

    inline bool pickByDrag() const { return m_pickDrag; }

    inline void setPickByDrag(bool set) { m_pickDrag = set; }

    inline ClickMode clickMode() const { return m_clickMode; }

    inline void setClickMode(ClickMode mode) { m_clickMode = mode; }

    inline ColorList* scheme() const { return m_colors; }

    void setScheme(ColorList *scheme);

    static ColorList* defaultColors();

    static ColorList* defaultColors2();

    static ColorList* baseColors();

    static ColorList* namedColors();

signals:

    void highlighted(const QColor &color);
    void picked(const QColor &color);
    void accepted();
    void rejected();

protected:
    virtual void paintEvent ( QPaintEvent * event );
    virtual bool event ( QEvent * event );
    virtual void mouseMoveEvent ( QMouseEvent * event );
    virtual void mousePressEvent ( QMouseEvent * event );
    virtual void mouseReleaseEvent ( QMouseEvent * event );
    virtual void leaveEvent ( QEvent * event );
    virtual void keyPressEvent ( QKeyEvent * event );

    void redraw();

    int index() const;

    int m_cellSize;
    int m_widthInCells;
    bool m_autoSize;
    int m_row, m_col, m_idx;
    QPixmap m_pix;
    bool m_pickDrag;
    ClickMode m_clickMode;
    QPoint m_pos;

    QColor m_hlColor, m_selColor;

    ColorList *m_colors;
};



#endif // CSCOLORGRID_H
