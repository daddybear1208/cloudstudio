#include <iostream>
#include <QtGui>
#include <QApplication>

#include "csMainWindow.h"

class CSApplication :public QApplication
{
public:
	CSApplication(int &argc, char **argv)
		: QApplication(argc, argv)
	{
		setOrganizationName(PACKAGE_ORGANIZATION);
		setApplicationName(PACKAGE_NAME);
	}
};

int main(int argc, char* argv[])
{
	CSApplication app(argc, argv);
	app.setQuitOnLastWindowClosed(true);
	app.setStyle(new QPlastiqueStyle);

	QSplashScreen* splash = new QSplashScreen;
	splash->setPixmap(QPixmap(":/UI/UI/splash.png"));
	splash->show();
	//check OpenGL support
	if (!QGLFormat::hasOpenGL())
	{
		QMessageBox::critical(0, "Error", "This application needs OpenGL to run!");
		return EXIT_FAILURE;
	}

// 	QFile styleSheet(":qdarkstyle/style.qss");
// 	if (!styleSheet.exists())
// 	{
// 		printf("Unable to set stylesheet, file not found\n");
// 	}
// 	else
// 	{
// 		styleSheet.open(QFile::ReadOnly | QFile::Text);
// 		QTextStream ts(&styleSheet);
// 		qApp->setStyleSheet(ts.readAll());
// 	}

	CSMainWindow* mainWindow = CSMainWindow::instance();
	if (!mainWindow)
	{
		QMessageBox::critical(0, "Error", "Failed to initialize the main application window!");
		return EXIT_FAILURE;
	}
	mainWindow->loadPlugins();
	mainWindow->showMaximized();
	splash->finish(mainWindow);
	delete splash;

	int result=0;
	try
	{
		result = app.exec();
	}
	catch (...)
	{
		QMessageBox::warning(0, "CloudStudio crashed!", "Hum, it seems that CloudStudio has crashed... Sorry about that :)");
	}

	return result;
}

