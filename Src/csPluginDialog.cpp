#include "csPluginDialog.h"
#include <QFileDialog>


CSPluginDialog::CSPluginDialog(QWidget* parent /*= 0*/)
	:QDialog(parent)
{
	setupUi(this);
	QStringList headers;
	headers << "Plugin" << "Author" << "Version";
	this->treeWidgetPluginView->setHeaderLabels(headers);
	this->treeWidgetPluginView->header()->setResizeMode(QHeaderView::ResizeToContents);
	this->treeWidgetPluginView->setColumnCount(3);
	setAttribute(Qt::WA_DeleteOnClose);
	initializePlugins();
	this->treeWidgetPluginView->expandAll();

	connect(this->btnAddPath, SIGNAL(clicked()), this, SLOT(addPluginPath()));
	connect(this->btnAddPlugin, SIGNAL(clicked()), this, SLOT(addPluginFile()));
	connect(this->btnRefresh, SIGNAL(clicked()), this, SLOT(refresh()));
}

CSPluginDialog::~CSPluginDialog()
{

}

void CSPluginDialog::initializePlugins()
{
	QList<QTreeWidgetItem*> loadedPluginsList;
	QMapIterator<CSPluginBase*, QString> iter(CSPluginManager::instance()->loaded_plugin_map_);
	while (iter.hasNext())
	{
		iter.next();
		bool exist = false;
		foreach(QTreeWidgetItem* item, loadedPluginsList)
		{
			if (item->text(0) == iter.value())
			{
				exist = true;
				QTreeWidgetItem* pluginItem = new QTreeWidgetItem();
				pluginItem->setText(0, iter.key()->description());
				pluginItem->setText(1, iter.key()->author());
				pluginItem->setText(2, iter.key()->version().toString());
				pluginItem->setToolTip(0, iter.key()->detailedDescription());
				pluginItem->setIcon(0, QIcon(":/UI/UI/pluginIcon.png"));
				item->addChild(pluginItem);
			}
		}
		if (!exist)
		{
			QTreeWidgetItem* dllItem = new QTreeWidgetItem();
			dllItem->setIcon(0, QIcon(":/UI/UI/pluginDll.png"));
			dllItem->setText(0, iter.value());
			QTreeWidgetItem* pluginItem = new QTreeWidgetItem();
			pluginItem->setText(0, iter.key()->description());
			pluginItem->setText(1, iter.key()->author());
			pluginItem->setText(2, iter.key()->version().toString());
			pluginItem->setToolTip(0, iter.key()->detailedDescription());
			pluginItem->setIcon(0, QIcon(":/UI/UI/pluginIcon.png"));
			dllItem->addChild(pluginItem);
			loadedPluginsList << dllItem;
		}
	}

	if (loadedPluginsList.size())
	{
		QTreeWidgetItem* loadedPluginsItem = new QTreeWidgetItem;
		loadedPluginsItem->setText(0, "Loaded Plugins");
		loadedPluginsItem->addChildren(loadedPluginsList);
		this->treeWidgetPluginView->addTopLevelItem(loadedPluginsItem);
	}


	QList<QTreeWidgetItem*> failedPluginsList;
	QMapIterator<CSPluginBase*, QString> failedIter(CSPluginManager::instance()->failed_plugin_map_);
	while (failedIter.hasNext())
	{
		failedIter.next();
		bool exist = false;
		foreach(QTreeWidgetItem* item, failedPluginsList)
		{
			if (item->text(0) == iter.value())
			{
				exist = true;
				QTreeWidgetItem* pluginItem = new QTreeWidgetItem();
				pluginItem->setText(0, iter.key()->description());
				pluginItem->setText(1, iter.key()->author());
				pluginItem->setText(2, iter.key()->version().toString());
				pluginItem->setToolTip(0, iter.key()->detailedDescription());
				pluginItem->setIcon(0, QIcon(":/UI/UI/pluginIcon.png"));
				item->addChild(pluginItem);
			}

		}
		if (!exist)
		{
			QTreeWidgetItem* dllItem = new QTreeWidgetItem();
			dllItem->setIcon(0, QIcon(":/UI/UI/pluginDll.png"));
			dllItem->setText(0, iter.value());
			QTreeWidgetItem* pluginItem = new QTreeWidgetItem();
			pluginItem->setText(0, iter.key()->description());
			pluginItem->setText(1, iter.key()->author());
			pluginItem->setText(2, iter.key()->version().toString());
			pluginItem->setToolTip(0, iter.key()->detailedDescription());
			pluginItem->setIcon(0, QIcon(":/UI/UI/pluginIcon.png"));
			dllItem->addChild(pluginItem);
			failedPluginsList << dllItem;
		}
	}
	if (failedPluginsList.size())
	{
		QTreeWidgetItem* failedPluginsItem = new QTreeWidgetItem;
		failedPluginsItem->setText(0, "Loaded Plugins");
		failedPluginsItem->addChildren(failedPluginsList);
		this->treeWidgetPluginView->addTopLevelItem(failedPluginsItem);
	}
}

void CSPluginDialog::addPluginFile()
{
	QString filter;
#ifdef Q_OS_WIN
	filter = "Plugin File (*.dll)";
#elif defined Q_OS_MAC
	filter = "Plugin File (*.dylib)";
#elif defined Q_OS_LINUX
	filter = "Plugin File (*.so)";
#endif
	QStringList files =
		QFileDialog::getOpenFileNames(this, "Add Plugin", "", filter);
	
	foreach(const QString& file, files)
	{
		CSPluginManager::instance()->registerPlugin(file);
	}
}

void CSPluginDialog::addPluginPath()
{
	QString path = QFileDialog::getExistingDirectory(this, "Add Path", "");
	if (!path.isEmpty())
		CSPluginManager::instance()->registerPluginPath(path);
}

void CSPluginDialog::refresh()
{
	this->treeWidgetPluginView->clear();
	initializePlugins();
	this->treeWidgetPluginView->expandAll();
}

