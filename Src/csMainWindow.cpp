#include "csMainWindow.h"


CSMainWindow::CSMainWindow()
{
	setAttribute(Qt::WA_DeleteOnClose);
	setupUi(this);
	is_fullscreen_ = false;
	CSProjectManager::instance()->setTreeViewDockWidget(dockWidgetProjectTree);
	mdi_area_->setViewMode(QMdiArea::TabbedView);
	mdi_area_->setTabsClosable(true);
	mdi_area_->setTabShape(QTabWidget::Triangular);

	property_browser_ = new QtTreePropertyBrowser;
	property_browser_->setRootIsDecorated(false);
	property_browser_->setPropertiesWithoutValueMarked(true);
	dockWidgetObjectProperty->setWidget(property_browser_);
	
	connect(actionNewProject, SIGNAL(triggered()), this, SLOT(newProject()));
	connect(actionOpenProject, SIGNAL(triggered()), this, SLOT(openProject()));
	connect(actionSaveProject, SIGNAL(triggered()), this, SLOT(saveProject()));
	connect(actionCloseProject, SIGNAL(triggered()), this, SLOT(closeProject()));
	connect(actionAddData, SIGNAL(triggered()), this, SLOT(addPointCloudData()));
	connect(actionTileProject, SIGNAL(triggered()), mdi_area_, SLOT(tileSubWindows()));
	connect(actionCascadeProject, SIGNAL(triggered()), mdi_area_, SLOT(cascadeSubWindows()));
	connect(actionPreviousProject, SIGNAL(triggered()), mdi_area_, SLOT(activatePreviousSubWindow()));
	connect(actionPreviousProject, SIGNAL(triggered()), mdi_area_, SLOT(activateNextSubWindow()));
	connect(actionToggle_Axis, SIGNAL(triggered()), this, SLOT(toggleAxis()));
	connect(actionToggle_Fps, SIGNAL(triggered()), this, SLOT(toggleFps()));
	connect(actionFull_Scene, SIGNAL(triggered()), this, SLOT(showFullScene()));
	connect(actionPlugin_Dialog, SIGNAL(triggered()), this, SLOT(showPluginDialog()));
	connect(actionTopView, SIGNAL(triggered()), this, SLOT(setCurrentViewTop()));
	connect(actionBottomView, SIGNAL(triggered()), this, SLOT(setCurrentViewBottom()));
	connect(actionFrontView, SIGNAL(triggered()), this, SLOT(setCurrentViewFront()));
	connect(actionBackView, SIGNAL(triggered()), this, SLOT(setCurrentViewBack()));
	connect(actionLeftView, SIGNAL(triggered()), this, SLOT(setCurrentViewLeft()));
	connect(actionRightView, SIGNAL(triggered()), this, SLOT(setCurrentViewRight()));
	connect(actionSaveSnapshot, SIGNAL(triggered()), this, SLOT(saveCurrentViewSnapshot()));
	connect(actionPerspective, SIGNAL(triggered()), this, SLOT(setCurrentViewPerspective()));
	connect(actionOrtho, SIGNAL(triggered()), this, SLOT(setCurrentViewOrtho()));
	connect(actionViewBackground, SIGNAL(triggered()), this, SLOT(setCurrentViewBackgroundColor()));
	connect(actionChangeStyle, SIGNAL(triggered()), this, SLOT(changeStyle()));
	connect(actionFullscreen, SIGNAL(triggered()), this, SLOT(toggleFullscreen()));
	updateWindowMenus();
	connect(menuWindow, SIGNAL(aboutToShow()), this, SLOT(updateWindowMenus()));
	window_mapper_ = new QSignalMapper;
	connect(mdi_area_, SIGNAL(subWindowActivated(QMdiSubWindow*)), this, SLOT(onSubWindowActivated(QMdiSubWindow*)));
	connect(window_mapper_, SIGNAL(mapped(QWidget*)), this, SLOT(setActiveGLViewer(QWidget*)));


	for (int i = 0; i < MaxRecentFiles; ++i) {
		recent_projects_acts[i] = new QAction(this);
		recent_projects_acts[i]->setVisible(false);
		connect(recent_projects_acts[i], SIGNAL(triggered()),
			this, SLOT(openRecentProject()));
		menuRecentProjects->addAction(recent_projects_acts[i]);
	}
	updateRecentFileActions();


	connect(CSLogHandler::instance(), SIGNAL(newMessage(const QString&)), this, SLOT(printLog(const QString&)));
	connect(CSLogHandler::instance(), SIGNAL(newMessages(const QStringList&)), this, SLOT(printLog(const QStringList&)));
	CSLogHandler::instance()->startEmission(true);

	CSPluginManager::instance();
// 	CSLogHandler::instance()->reportDebug("This is a 'DEBUG' log");
// 	CSLogHandler::instance()->reportInfo("This is a 'INFO' log");
// 	CSLogHandler::instance()->reportWarning("This is a 'WARNING' log");
// 	CSLogHandler::instance()->reportError("This is a 'ERROR' log");
}

CSMainWindow::~CSMainWindow()
{
	
}


void CSMainWindow::closeEvent(QCloseEvent* event)
{
	
}

void CSMainWindow::newProject()
{
	CSProject* csProj = new CSProject("New Project");
	csProj->setModified(true);
	CSGLViewer* glViewer = new CSGLViewer;
	glViewer->setWindowTitle(csProj->getProjectName());
	glViewer->setSceneRoot(csProj->projectRoot());
	
	CSProjectTreeView* treeView = new CSProjectTreeView(property_browser_);
	connect(treeView, SIGNAL(glviewerRefreshSignal()), glViewer, SLOT(updateGL()));
	CSProjectModel* model = new CSProjectModel(csProj, treeView);
	CSProjectManager::instance()->addProject(csProj, glViewer, model, treeView);
	QMdiSubWindow* subWnd = mdi_area_->addSubWindow(glViewer);
	glViewer->show();
}

void CSMainWindow::openProject()
{
	QString projFileName = QFileDialog::getOpenFileName(this,
		"Open CloudStudio Project", QString(), "CloudStudio Project (*.csproj)",
		0, QFileDialog::DontUseNativeDialog);
	if (projFileName.isEmpty())
		return;
	QMdiSubWindow* subWnd = findExistMdiSubWindow(projFileName);
	if (subWnd)
	{
		mdi_area_->setActiveSubWindow(subWnd);
		return;
	}

	CSProject* csProj = new CSProject("New Project");
	QApplication::setOverrideCursor(Qt::WaitCursor);
	CSProject::CS_PROJECT_LOAD_RST rst = csProj->load(projFileName);
	QApplication::restoreOverrideCursor();
	if (rst == CSProject::CS_PROJECT_LOAD_OK)
	{
		QApplication::setOverrideCursor(Qt::WaitCursor);
		// update recent projects
		QSettings settings(PACKAGE_ORGANIZATION, PACKAGE_NAME);
		settings.beginGroup("Application");
		settings.beginGroup("RecentProjects");
		QStringList files = settings.value("RecentProjectsList").toStringList();
		files.removeAll(projFileName);
		files.prepend(projFileName);
		while (files.size() > MaxRecentFiles)
			files.removeLast();
		settings.setValue("RecentProjectsList", files);
		updateRecentFileActions();
		settings.endGroup();
		settings.endGroup();

		CSGLViewer* glViewer = new CSGLViewer;
		glViewer->setWindowTitle(csProj->getProjectName());
		glViewer->setSceneRoot(csProj->projectRoot());
		CSProjectTreeView* treeView = new CSProjectTreeView(property_browser_);
		connect(treeView, SIGNAL(glviewerRefreshSignal()), glViewer, SLOT(updateGL()));
		CSProjectModel* model = new CSProjectModel(csProj, treeView);
		CSProjectManager::instance()->addProject(csProj, glViewer, model, treeView);
		CSLogHandler::instance()->reportInfo("Successfully loaded project: " + projFileName);
		subWnd = mdi_area_->addSubWindow(glViewer);
		glViewer->show();
		QApplication::restoreOverrideCursor();
		return;
	}
	else if (rst == CSProject::CS_PROJECT_FILE_OPEN_ERR)
	{
		CSLogHandler::instance()->reportError("Cannot open file for reading");
		delete csProj;
		return;
	}
	else if (rst == CSProject::CS_PROJECT_FILE_NOT_COMPATIBLE)
	{
		CSLogHandler::instance()->reportError("Invalid CloudStudio project file");
		delete csProj;
		return;
	}

}

void CSMainWindow::openProject(const QString& filename)
{
	if (filename.isEmpty())
		return;
	QMdiSubWindow* subWnd = findExistMdiSubWindow(filename);
	if (subWnd)
	{
		mdi_area_->setActiveSubWindow(subWnd);
		return;
	}

	CSProject* csProj = new CSProject("New Project");
	QApplication::setOverrideCursor(Qt::WaitCursor);
	CSProject::CS_PROJECT_LOAD_RST rst = csProj->load(filename);
	QApplication::restoreOverrideCursor();
	if (rst == CSProject::CS_PROJECT_LOAD_OK)
	{
		QApplication::setOverrideCursor(Qt::WaitCursor);
		// update recent projects
		QSettings settings(PACKAGE_ORGANIZATION, PACKAGE_NAME);
		settings.beginGroup("Application");
		settings.beginGroup("RecentProjects");
		QStringList files = settings.value("RecentProjectsList").toStringList();
		files.removeAll(filename);
		files.prepend(filename);
		while (files.size() > MaxRecentFiles)
			files.removeLast();
		settings.setValue("RecentProjectsList", files);
		updateRecentFileActions();
		settings.endGroup();
		settings.endGroup();

		CSGLViewer* glViewer = new CSGLViewer;
		glViewer->setWindowTitle(csProj->getProjectName());
		glViewer->setSceneRoot(csProj->projectRoot());
		CSProjectTreeView* treeView = new CSProjectTreeView(property_browser_);
		connect(treeView, SIGNAL(glviewerRefreshSignal()), glViewer, SLOT(updateGL()));
		CSProjectModel* model = new CSProjectModel(csProj, treeView);
		CSProjectManager::instance()->addProject(csProj, glViewer, model, treeView);
		CSLogHandler::instance()->reportInfo("Successfully loaded project: " + filename);
		subWnd = mdi_area_->addSubWindow(glViewer);
		glViewer->show();
		QApplication::restoreOverrideCursor();
		return;
	}
	else if (rst == CSProject::CS_PROJECT_FILE_OPEN_ERR)
	{
		CSLogHandler::instance()->reportError("Cannot open file for reading");
		delete csProj;
		return;
	}
	else if (rst == CSProject::CS_PROJECT_FILE_NOT_COMPATIBLE)
	{
		CSLogHandler::instance()->reportError("Invalid CloudStudio project file");
		delete csProj;
		return;
	}
}

void CSMainWindow::closeProject()
{
	CSProject* project = CSProjectManager::instance()->currentProject();
	if (!project)
		return;
	CSProjectManager::instance()->currentGLViewer()->setStateFileName("D:/state.xml");
	CSProjectManager::instance()->currentGLViewer()->saveStateToFile();
	
}

void CSMainWindow::closeProject(CSProject* proj)
{

}

QMdiSubWindow* CSMainWindow::findExistMdiSubWindow(const QString& projectFileName)
{
	QString canonicalFilePath = QFileInfo(projectFileName).canonicalFilePath();
	CSProject* project = CSProjectManager::instance()->findProject(canonicalFilePath);
	if (project)
	{
		CSGLViewer* glviewer = CSProjectManager::instance()->findGLViewer(project);
		Q_FOREACH(QMdiSubWindow* subWindow, mdi_area_->subWindowList())
		{
			if (subWindow->widget() == glviewer)
			{
				return subWindow;
			}
		}
	}
	return 0;
}

void CSMainWindow::setActiveGLViewer(QWidget* wnd)
{
	QMdiSubWindow* sub = qobject_cast<QMdiSubWindow*>(wnd);
	if (!sub)
		return;
	mdi_area_->setActiveSubWindow(sub);
}

void CSMainWindow::loadPlugins()
{
	QSettings settings(PACKAGE_ORGANIZATION, PACKAGE_NAME);
	settings.beginGroup("Application");
	settings.beginGroup("Plugin");

	QStringList defaultPluginDirs;
	defaultPluginDirs << QApplication::applicationDirPath() + "/Plugin";
	QStringList pluginDirs = settings.value("PluginDirs", defaultPluginDirs).toStringList();

	foreach(const QString& dir, pluginDirs)
	{
		CSPluginManager::instance()->registerPluginPath(dir);
	}
	settings.endGroup();
	settings.endGroup();
}

void CSMainWindow::printLog(const QString& message)
{
	log_viewer_->append(message);
}

void CSMainWindow::printLog(const QStringList& messages)
{
	log_viewer_->append(messages.join("<br>"));
}

CSMainWindow* CSMainWindow::instance()
{
	if (!instance_)
	{
		instance_ = new CSMainWindow();
	}
	return instance_;
}

void CSMainWindow::updateWindowMenus()
{
	menuWindow->clear();
	menuWindow->addAction(actionTileProject);
	menuWindow->addAction(actionCascadeProject);
	menuWindow->addSeparator();
	menuWindow->addAction(actionPreviousProject);
	menuWindow->addAction(actionNextProject);

	QList<QMdiSubWindow*> subWnds = mdi_area_->subWindowList();
	if (subWnds.isEmpty())
		return;
	menuWindow->addSeparator();
	foreach(QMdiSubWindow* subWnd, subWnds)
	{
		CSGLViewer* glViewer = qobject_cast<CSGLViewer*>(subWnd->widget());
		QAction* action =
			menuWindow->addAction(CSProjectManager::instance()->getProjectFromGLViewer(glViewer)->getProjectName() + ".csproj");
		action->setCheckable(true);
		action->setChecked(glViewer == CSProjectManager::instance()->currentGLViewer());
		connect(action, SIGNAL(triggered()), window_mapper_, SLOT(map()));
		window_mapper_->setMapping(action, subWnd);
	}

}

void CSMainWindow::onSubWindowActivated(QMdiSubWindow* window)
{
	if (!window)
		return;

	CSGLViewer* glViewer = qobject_cast<CSGLViewer*>(window->widget());
	CSProject* project = CSProjectManager::instance()->getProjectFromGLViewer(glViewer);
	CSProjectManager::instance()->setCurrentProject(project);
}

void CSMainWindow::updateRecentFileActions()
{
	QSettings settings(PACKAGE_ORGANIZATION, PACKAGE_NAME);
	settings.beginGroup("Application");
	settings.beginGroup("RecentProjects");
	QStringList files = settings.value("RecentProjectsList").toStringList();
	int numRecentFiles = qMin(files.size(), (int)MaxRecentFiles);
	for (int i = 0; i < numRecentFiles; ++i) 
	{
		QString text = tr("&%1 %2").arg(i + 1).arg(QFileInfo(files[i]).fileName());
		recent_projects_acts[i]->setText(text);
		recent_projects_acts[i]->setData(files[i]);
		recent_projects_acts[i]->setVisible(true);
	}

	settings.endGroup();
	settings.endGroup();
}

void CSMainWindow::openRecentProject()
{
	QAction *action = qobject_cast<QAction *>(sender());
	if (action)
		openProject(action->data().toString());
}

void CSMainWindow::showFullScene()
{
	CSGLViewer* viewer = CSProjectManager::instance()->currentGLViewer();
	if (viewer)
	{
		viewer->showEntireScene();
	}

}

void CSMainWindow::toggleFps()
{
	CSGLViewer* viewer = CSProjectManager::instance()->currentGLViewer();
	if (viewer)
	{
		viewer->toggleFPSIsDisplayed();
	}
}

void CSMainWindow::toggleAxis()
{

	CSGLViewer* viewer = CSProjectManager::instance()->currentGLViewer();
	if (viewer)
	{
		viewer->toggleAxisIsDrawn();
	}
}

void CSMainWindow::processAlgorithm()
{
	QAction* action = qobject_cast<QAction*>(sender());
	if (!action)
	{
		return;
	}
	CSProject* project = CSProjectManager::instance()->currentProject();
	if (!project)
	{
		return;
	}
	CSAlgorithmInterface* alg = algorithm_map_.value(action);
	if (!alg) return;
	CSAlgorithmCustomizer* customizer = new CSAlgorithmCustomizer(project, alg, this);
	customizer->show();
}

void CSMainWindow::showPluginDialog()
{
	CSPluginDialog* dlg = new CSPluginDialog(this);
	dlg->show();
}

void CSMainWindow::addPointCloudData()
{
	CSProject* project = CSProjectManager::instance()->currentProject();
	if (!project)
		return;
	QStringList exts;
	foreach(CSFileReaderInterface* reader, CSFileReaderHelper::instance()->readers())
	{
		exts << CSFileReaderHelper::instance()->getFileExtension(reader->fileType());
	}
	QString filter;
	foreach(const QString& ext, exts)
	{
		filter += QString("*.%1 ").arg(ext);
	}
	
	QStringList files = 
		QFileDialog::getOpenFileNames(this, "Add data", "", tr("Point Cloud Data (%1)").arg(filter));
	if (!files.size()) return;

	CSProjectTreeView* treeview = CSProjectManager::instance()->currentProjectTreeView();
	CSHierarchicalObject* selectedObject = treeview->selectedObject();
	if (!selectedObject || selectedObject == CSProjectManager::instance()->currentProject()->projectRoot())
	{
		foreach(const QString& file, files)
		{
			CSFileReaderInterface* reader = CSFileReaderHelper::instance()->getFileReader(file);
			if (!reader)
			{
				CSLogHandler::instance()->reportError("Cannot find reader plugin to load '"+file+"'");
				continue;
			}
			switch (reader->objectType())
			{
			case CSObject::CS_POINTCLOUD:
			{
				CSPointCloud* pointcloud = new CSPointCloud(QFileInfo(file).baseName());
				CSProgessDialog* progressDlg = new CSProgessDialog(this);
				progressDlg->setLabelText(QString("Loading point cloud: '%1'").arg(pointcloud->name()));
				QObject::connect(reader, SIGNAL(progressChanged(int)), progressDlg, SLOT(setValue(int)));
				progressDlg->show();
				if (!reader->readFile(pointcloud, file))
				{
					CSLogHandler::instance()->reportError("Point cloud '" +file+ "' not load correctly! Error: " + reader->lastError());
					delete pointcloud;
					continue;
				}
				else
				{
					CSProjectManager::instance()->currentProjectModel()->addObject(pointcloud);
					CSProjectManager::instance()->currentGLViewer()->refreshSceneContent();
					CSLogHandler::instance()->reportInfo(QString("Successfully load point cloud, point count: %1").arg(pointcloud->pointsCount()));
				}
				reader->disconnect();
				progressDlg->close();
			}
			default:
				break;
			}

		}
	}
	else if (selectedObject->isA(CSObject::CS_GROUP))
	{
		foreach(const QString& file, files)
		{
			CSFileReaderInterface* reader = CSFileReaderHelper::instance()->getFileReader(file);
			if (!reader)
			{
				CSLogHandler::instance()->reportError("Cannot find reader plugin to load '" + file + "'");
				continue;
			}
			switch (reader->objectType())
			{
			case CSObject::CS_POINTCLOUD:
			{
				CSPointCloud* pointcloud = new CSPointCloud(QFileInfo(file).baseName());
				selectedObject->appendChild(pointcloud);
				CSProgessDialog* progressDlg = new CSProgessDialog(this);
				progressDlg->setLabelText(QString("Loading point cloud: '%1'").arg(pointcloud->name()));
				QObject::connect(reader, SIGNAL(progressChanged(int)), progressDlg, SLOT(setValue(int)));
				progressDlg->show();
				if (!reader->readFile(pointcloud, file))
				{
					CSLogHandler::instance()->reportError("Point cloud '" + file + "' not load correctly! Error: " + reader->lastError());
					delete pointcloud;
					continue;
				}
				else
				{
					CSProjectManager::instance()->currentProjectModel()->addObject(pointcloud);
					CSProjectManager::instance()->currentGLViewer()->refreshSceneContent();
					CSLogHandler::instance()->reportInfo(QString("Successfully load point cloud, point count: %1").arg(pointcloud->pointsCount()));
				}
				reader->disconnect();
				progressDlg->close();

			}
			default:
				break;
			}

		}
	}
}

void CSMainWindow::saveProject()
{
	CSProject* proj = CSProjectManager::instance()->currentProject();
	if (!proj || !proj->isModified())
		return;
	
	if (CSProjectManager::instance()->currentProject()->getProjectFileName().isEmpty())
	{
		QString path = QFileDialog::getExistingDirectory(this, "Choose path", "");
		if (path.isEmpty()) return;
		proj->setProjectFileName(path + proj->getProjectName() + ".csproj");
	}
	proj->save();
}

void CSMainWindow::setCurrentViewTop()
{
		CSGLViewer* viewer = CSProjectManager::instance()->currentGLViewer();
		if (!viewer)
			return;
		viewer->setViewType(CSGLViewer::TOP_VIEW, 1);
}

void CSMainWindow::setCurrentViewBottom()
{
	CSGLViewer* viewer = CSProjectManager::instance()->currentGLViewer();
	if (!viewer)
		return;
	viewer->setViewType(CSGLViewer::BOTTOM_VIEW, 1);
}

void CSMainWindow::setCurrentViewFront()
{
	CSGLViewer* viewer = CSProjectManager::instance()->currentGLViewer();
	if (!viewer)
		return;
	viewer->setViewType(CSGLViewer::FRONT_VIEW, 1);
}

void CSMainWindow::setCurrentViewBack()
{
	CSGLViewer* viewer = CSProjectManager::instance()->currentGLViewer();
	if (!viewer)
		return;
	viewer->setViewType(CSGLViewer::BACK_VIEW, 1);
}

void CSMainWindow::setCurrentViewLeft()
{
	CSGLViewer* viewer = CSProjectManager::instance()->currentGLViewer();
	if (!viewer)
		return;
	viewer->setViewType(CSGLViewer::LEFT_VIEW, 1);
}

void CSMainWindow::setCurrentViewRight()
{
	CSGLViewer* viewer = CSProjectManager::instance()->currentGLViewer();
	if (!viewer)
		return;
	viewer->setViewType(CSGLViewer::RIGHT_VIEW, 1);
}

void CSMainWindow::saveCurrentViewSnapshot()
{
	CSGLViewer* viewer = CSProjectManager::instance()->currentGLViewer();
	if (!viewer)
		return;
	bool saveOK = false;
	viewer->saveSnapshot(saveOK, false, false );
	if (!saveOK)
		CSLogHandler::instance()->reportError("Snapshot save failed");
	else
		CSLogHandler::instance()->reportInfo("Snapshot successfully saved: " + viewer->snapshotFileName());
}


void CSMainWindow::setCurrentViewOrtho()
{
	CSGLViewer* viewer = CSProjectManager::instance()->currentGLViewer();
	if (!viewer)
		return;
	viewer->camera()->setType(Camera::ORTHOGRAPHIC);
}

void CSMainWindow::setCurrentViewPerspective()
{
	CSGLViewer* viewer = CSProjectManager::instance()->currentGLViewer();
	if (!viewer)
		return;
	viewer->camera()->setType(Camera::PERSPECTIVE);
}

void CSMainWindow::setCurrentViewBackgroundColor()
{
	CSGLViewer* viewer = CSProjectManager::instance()->currentGLViewer();
	if (!viewer)
		return;
	CSBackgroundColorDialog* dlg = new CSBackgroundColorDialog(viewer, this);
	dlg->show();
}

void CSMainWindow::changeStyle()
{
	CSStyleSheetEditor* styleEditor = new CSStyleSheetEditor(this);
	styleEditor->show();
}

void CSMainWindow::toggleFullscreen()
{
	if (!is_fullscreen_)
	{
		this->dockWidgetObjectProperty->hide();
		this->dockWidgetProjectTree->hide();
		this->dockWidgetLog->hide();
		this->toolBar->hide();
		this->menubar->hide();
		this->statusbar->hide();
		is_fullscreen_ = true;
	}
	else
	{
		this->dockWidgetObjectProperty->show();
		this->dockWidgetProjectTree->show();
		this->dockWidgetLog->show();
		this->toolBar->show();
		this->menubar->show();
		this->statusbar->show();
		is_fullscreen_ = false;
	}
}

void CSMainWindow::keyPressEvent(QKeyEvent *event)
{
	switch (event->key())
	{
	case Qt::Key_F:
	{
		if (event->modifiers() == Qt::ControlModifier)
		{
			toggleFullscreen();
		}
	}
	default:
		break;
	}
}


CSMainWindow* CSMainWindow::instance_ = 0;

