#include "csProject.h"

CSProject::CSProject(const QString& name /*= QString("New Project")*/)
	:project_name_(name), modified_(false), project_file_name_("")
{
	general_info_.creator = CSCommonUtility::getSysUername();
	general_info_.creation_time = QDateTime::currentDateTimeUtc();
	general_info_.cs_version = CSVersion(kCSMajorVersion, kCSMinorVersion, kCSPatchVersion);
	project_root_ = new CSGroupObject(name, true);
	project_root_->setValidity(true);
}

const QString& CSProject::getCreator() const
{
	return general_info_.creator;
}

const CSVersion& CSProject::getCSVersion() const
{
	return general_info_.cs_version;
}

bool CSProject::save()
{
	if (project_file_name_.isEmpty())
		return false;



	return true;
}

bool CSProject::isModified() const
{
	return modified_;
}

const QDateTime& CSProject::getCreationTime() const
{
	return general_info_.creation_time;
}

const QString& CSProject::getProjectFileName() const
{
	return project_file_name_;
}


CSProject::CS_PROJECT_LOAD_RST CSProject::load(const QString& projectFileName)
{
	CS_PROJECT_LOAD_RST loadRst = CS_PROJECT_LOAD_OK;
	// load the project from a specified file
	QFile file(projectFileName);
	if (!file.open(QIODevice::ReadOnly))
	{
		loadRst = CS_PROJECT_FILE_OPEN_ERR;
	}
	else
	{
		QDomDocument projXmlDoc;
		if (!projXmlDoc.setContent(&file))
		{
			file.close();
			loadRst = CS_PROJECT_FILE_NOT_COMPATIBLE;
		}
		else
		{
			file.close();
			// initialize project from QDomDocument 
			this->project_file_name_ = projectFileName;
            // set the working directory to current project file path
            QDir::setCurrent(QFileInfo(projectFileName).path());
			this->setProjectName(QFileInfo(projectFileName).baseName());
			QDomElement projectElement = projXmlDoc.documentElement();

			if (projectElement.isNull() || projectElement.tagName() != "CloudStudioProject")
			{
				loadRst = CS_PROJECT_FILE_NOT_COMPATIBLE;
			}
			else
			{
				// read project attributes

				// read project file version
				if (!projectElement.hasAttribute("version"))
				{
					CSLogHandler::instance()
						->reportWarning("missing project version, the loading project maybe incompatible with current version of CloudStudio");
				}

				general_info_.cs_version = CSVersion(projectElement.attribute("version"));
				// check version
				CSVersion currentVersion(kCSMajorVersion, kCSMinorVersion, kCSPatchVersion);
				CSVersion::CSVERSION_CMP_RESULUT rst = currentVersion.compare(general_info_.cs_version);

				if (rst == CSVersion::CS_VERSION_MAJOR_DIFF)
					return CS_PROJECT_VERSION_NOT_COMPATIBLE;
				else if (rst == CSVersion::CSVERSION_MINOR_DIFF)
				{
					CSLogHandler::instance()->
						reportInfo("The loading project maybe incompatible with current version of CloudStudio");
				}

				// read project creator and creation time
				if (projectElement.hasAttribute("creator"))
				{
					general_info_.creator = projectElement.attribute("creator");
				}
				else
				{
					CSLogHandler::instance()->reportWarning("missing project creator, set to current system user");
				}

				if (projectElement.hasAttribute("time"))
				{
					general_info_.creation_time = QDateTime::fromString(projectElement.attribute("time"), TIME_FORMAT);
				}
				else
				{
					CSLogHandler::instance()->reportWarning("missing project creation time, set to current time");
				}


				// traverse all child elements in the project

 				QDomElement element = projectElement.firstChildElement("Scene").firstChildElement();

 
 				while (!element.isNull())
 				{
 					// reading this element and its child elements recursively
 					CSHierarchicalObject* childObject = recursiveReadElement(element);
 					if (childObject)
 						project_root_->appendChild(childObject);
 					element = element.nextSiblingElement();
 				}
			}

		}
	}

	return loadRst;
}

CSHierarchicalObject* CSProject::recursiveReadElement(const QDomElement& element)
{
	CSHierarchicalObject* object = 0;

	// handle this element
	if (element.tagName()=="Group")
	{
		object = new CSGroupObject(element.attribute("name", QString("Group")));
		object->setValidity(true);
	}
	else if (element.tagName() == "PointCloud")
	{
		CSPointCloud* pc = new CSPointCloud;
        bool initRst = pc->initFromDomElement(element);
        if (initRst) {
            pc->setValidity(true);
        }
        
		object = pc;
	}

	// handle children recursively
	if (object)
	{
		QDomElement childElement = element.firstChildElement();
		while (!childElement.isNull())
		{
			CSHierarchicalObject* childObject = recursiveReadElement(childElement);
			if (childObject)
			{
				object->appendChild(childObject);
			}
			childElement = childElement.nextSiblingElement();
		}
	}
	return object;

}

CSProject::~CSProject()
{
	delete project_root_;
}

CSGroupObject* CSProject::projectRoot() const
{
	return project_root_;
}

const QString& CSProject::getProjectName() const
{
	return project_name_;
}

void CSProject::setProjectName(const QString& projectName)
{
	project_name_ = projectName;
	project_root_->setName(project_name_);
}

void CSProject::setProjectFileName(const QString& projectName)
{
	project_file_name_ = projectName;
}

void CSProject::setModified(bool isMod)
{
	modified_ = isMod;
}




