#include <QtGui>

#include "csStyleSheetEditor.h"

CSStyleSheetEditor::CSStyleSheetEditor(QWidget *parent)
    : QDialog(parent)
{
    ui.setupUi(this);
    setModal(true);
    QRegExp regExp(".(.*)\\+?Style");
    QString defaultStyle = QApplication::style()->metaObject()->className();

    if (regExp.exactMatch(defaultStyle))
        defaultStyle = regExp.cap(1);

    ui.styleCombo->addItems(QStyleFactory::keys());
    ui.styleCombo->setCurrentIndex(ui.styleCombo->findText(defaultStyle, Qt::MatchContains));
    ui.styleSheetCombo->setCurrentIndex(ui.styleSheetCombo->findText("Default"));
    loadStyleSheet("Default");
}

void CSStyleSheetEditor::on_styleCombo_activated(const QString &styleName)
{
    qApp->setStyle(styleName);
    ui.applyButton->setEnabled(false);
}

void CSStyleSheetEditor::on_styleSheetCombo_activated(const QString &sheetName)
{
    loadStyleSheet(sheetName);
}

void CSStyleSheetEditor::on_styleTextEdit_textChanged()
{
    ui.applyButton->setEnabled(true);
}

void CSStyleSheetEditor::on_applyButton_clicked()
{
    qApp->setStyleSheet(ui.styleTextEdit->toPlainText());
    ui.applyButton->setEnabled(false);
}

void CSStyleSheetEditor::loadStyleSheet(const QString &sheetName)
{
    QFile file(":/StyleSheet/" + sheetName.toLower() + ".qss");
    file.open(QFile::ReadOnly);
    QString styleSheet = QLatin1String(file.readAll());

    ui.styleTextEdit->setPlainText(styleSheet);
    qApp->setStyleSheet(styleSheet);
    ui.applyButton->setEnabled(false);
}
