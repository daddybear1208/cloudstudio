#ifndef CSPROJECTTREEEVIEW_H
#define CSPROJECTTREEEVIEW_H
#include <QTreeView>
#include <QMenu>
#include <QAction>
#include <QMap>
#include <QPersistentModelIndex>
#include <propertybrowser/qtvariantproperty.h>
#include <propertybrowser/qttreepropertybrowser.h>
#include <CS_CORE/csPointCloud.h>
#include "csProject.h"
#include <csUUID.h>

#define QUuidList QList<QUuid>


class CSProjectTreeView : public QTreeView
{
	Q_OBJECT
public:
    CSProjectTreeView(QtTreePropertyBrowser* propertyBrowser, QWidget* parent = 0);
    ~CSProjectTreeView();

	virtual void setVisible(bool visible);

	CSHierarchicalObject* selectedObject() const;
    
protected Q_SLOTS:
    virtual void selectionChanged(const QItemSelection& selected, const QItemSelection& deselected);
	
Q_SIGNALS:
	void glviewerRefreshSignal();

public Q_SLOTS:
	void onPropertyValueChanged(QtProperty *property, const QVariant &value);
	void onCustomContextMenuRequested(const QPoint & pos);
	
private:
	QMap<CSProject*, QUuidList*> expand_state_map_;

	QtTreePropertyBrowser* property_browser_;
	QtVariantPropertyManager* property_manager_;
	
	// this pointed will be set when only one object is selected
	CSHierarchicalObject* selected_object_;

	QAction* action_set_visible_;


};

#endif