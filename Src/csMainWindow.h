#ifndef MAINWINDOW_H
#define	MAINWINDOW_H

#include <QMainWindow>
QT_BEGIN_NAMESPACE
#include <QFileDialog>
#include <QMdiArea>
#include <QMdiSubWindow>
#include <QVector>
#include <QMap>
QT_END_NAMESPACE


 
#include <CS_CORE/csFileReaderHelper.h>
#include <CS_CORE/csLogger.h>
#include <CS_CORE/csAlgorithmInterface.h>
#include <CS_CORE/csProgressDialog.h>

#include "csPluginManager.h"
#include "csProjectManager.h"
#include "csAlgorithmCustomizer.h"
#include <qttreepropertybrowser.h>
#include "ui_MainWindow.h"
#include "csPluginDialog.h"
#include "csBackgroundColorDialog.h"
#include "csStyleSheetEditor.h"

class CSMainWindow :public QMainWindow, public Ui::MainWindow
{
	Q_OBJECT

	friend class CSPluginManager;
public:

	static CSMainWindow* instance();
	~CSMainWindow();

	void loadPlugins();

public Q_SLOTS:
	void setCurrentViewTop();
	void setCurrentViewBottom();
	void setCurrentViewFront();
	void setCurrentViewBack();
	void setCurrentViewLeft();
	void setCurrentViewRight();
	void setCurrentViewPerspective();
	void setCurrentViewOrtho();
	void setCurrentViewBackgroundColor();


	void setActiveGLViewer(QWidget* wnd);
	void processAlgorithm();
	void updateWindowMenus();
	void onSubWindowActivated(QMdiSubWindow* window);
	void newProject();
	void openProject();
	void openProject(const QString& filename);
	void openRecentProject();
	void closeProject();
	void closeProject(CSProject* proj);
	void saveProject();
	void printLog(const QString& message);
	void printLog(const QStringList& messages);
	void toggleAxis();
	void toggleFps();
	void showFullScene();
	void showPluginDialog();
	void addPointCloudData();
	void changeStyle();
	void saveCurrentViewSnapshot();
	void toggleFullscreen();


protected:
	CSMainWindow();
	virtual void closeEvent(QCloseEvent* event);
	virtual void keyPressEvent(QKeyEvent *event);
private:
	
	void updateRecentFileActions();
	QMdiSubWindow* findExistMdiSubWindow(const QString& projectFileName);
	static CSMainWindow* instance_;
	QtTreePropertyBrowser* property_browser_;
	QSignalMapper* window_mapper_;
	enum { MaxRecentFiles  = 5};
	QAction* recent_projects_acts[MaxRecentFiles];
	QMap<QAction*, CSAlgorithmInterface*> algorithm_map_;
	bool is_fullscreen_;
};

#endif // !MAINWINDOW_H



