// This is unit test for CSChunkedArrary and CSChunkedArrayIterator


#include <CS_CORE/csChunkedArray.h>
#include <iostream>
#include <time.h>
using std::cout;
using std::endl;

#define TEST_ELEMENT_COUNT 100000

int main(int argc, char* argv[])
{
	CSChunkedArray<3, int>* array = new CSChunkedArray<3, int>();
	array->reserve(TEST_ELEMENT_COUNT);
	for (int i = 0; i < TEST_ELEMENT_COUNT; ++i	)
	{
		//int randNum = std::rand();
		//int data[3] = { randNum, randNum + 1, randNum + 2 };
		int data[3] = { 1000, 1001, 1002 };
		array->addElement(data);
	}
	std::cout << "current element count: " << array->elementCount() << std::endl;



	//use iterator to access the array
	CSChunkedArrayIterator<3, int>* iter = array->iterator();
	int i = 1;
	while (!iter->isEnd())
	{
		int* const data = iter->internalPointer();
		cout << i++ <<": " << *data << "  " << *(data + 1) << "  " << *(data + 2) << endl;
		iter->moveNext();
	}

	iter->reset();
	i = 1;

	while (!iter->isEnd())
	{
		int* const data = iter->internalPointer();
		*data = 2000;
		//cout << i++ <<": " << *data << "  " << *(data + 1) << "  " << *(data + 2) << endl;
		iter->moveNext();
	}

	iter->reset();
	i = 1;
	while (!iter->isEnd())
	{
		int* const data = iter->internalPointer();
		cout << i++ << ": " << *data << "  " << *(data + 1) << "  " << *(data + 2) << endl;
		iter->moveNext();
	}






// 	start = clock();
 	//for (int i = 0; i < array->elementCount(); ++i)
 	//{
 	//	int* data = array->value(i);
 	//	*data = *(data + 1) = *(data + 2) = 1000;
 	//}
// 	end = clock();

	//start = clock();
	//iter->setBegin();
	//while (!iter->isEnd())
	//{
	//	int* data = iter->internalPointer();
	//	*data = *(data + 1) = *(data + 2) = 1000;
	//	iter->moveNext();
	//}
	//end = clock();

	//start = clock();
	//iter->setBegin();
	//while (!iter->isEnd())
	//{
	//	int* data = iter->internalPointer();
	//	cout << *data << *(data + 1) << (*data + 2) << endl;
	//	iter->moveNext();
	//}
	//end = clock();

// 	start = clock();
// 	for (int i = 0; i < array->elementCount(); ++i)
// 	{
// 		int* data = array->value(i);
// 		cout << *data << "  " << *(data + 1) << "  " << (*data + 2) << endl;
// 	}
// 	end = clock();

	delete array;
	delete iter;
	system("pause");
	return 0;
}