if( WIN32 )
	set( SHARED_LIB_TYPE RUNTIME ) # CMake considers Dlls as RUNTIME on Windows!
else()
	set( SHARED_LIB_TYPE LIBRARY )
endif()

# Find mocable files (simply look for Q_OBJECT in all files!)
function( find_mocable_files __out_var_name )   # + input list
    set( local_list )
    foreach( one_file ${ARGN} )
        file( READ ${one_file} stream )
        if( stream MATCHES "Q_OBJECT" )
            list( APPEND local_list ${one_file} )
        endif()
    endforeach()
    set( ${__out_var_name} ${local_list} PARENT_SCOPE )
endfunction()

# Default preprocessors
function( set_default_cs_preproc ) # 1 argument: ARGV0 = target
set( CS_DEFAULT_PREPROCESSORS NOMINMAX _CRT_SECURE_NO_WARNINGS ) #all configurations
set( CS_DEFAULT_PREPROCESSORS_RELEASE NDEBUG ) #release specific
set( CS_DEFAULT_PREPROCESSORS_DEBUG _DEBUG ) #debug specific

if (MSVC)
	#disable SECURE_SCL (see http://channel9.msdn.com/shows/Going+Deep/STL-Iterator-Debugging-and-Secure-SCL/)
	list( APPEND CS_DEFAULT_PREPROCESSORS_RELEASE _SECURE_SCL=0 )

	#use VLD for mem leak checking
	OPTION( OPTION_USE_VISUAL_LEAK_DETECTOR "Check to activate compilation (in debug) with Visual Leak Detector" OFF )
    if( ${OPTION_USE_VISUAL_LEAK_DETECTOR} )
		list( APPEND CS_DEFAULT_PREPROCESSORS_DEBUG USE_VLD )
    endif()
endif()

set_property( TARGET ${ARGV0} APPEND PROPERTY COMPILE_DEFINITIONS ${CS_DEFAULT_PREPROCESSORS} )
if( NOT CMAKE_CONFIGURATION_TYPES )
	set_property( TARGET ${ARGV0} APPEND PROPERTY COMPILE_DEFINITIONS ${CS_DEFAULT_PREPROCESSORS_RELEASE} )
else()
	set_property( TARGET ${ARGV0} APPEND PROPERTY COMPILE_DEFINITIONS_RELEASE ${CS_DEFAULT_PREPROCESSORS_RELEASE} )
	set_property( TARGET ${ARGV0} APPEND PROPERTY COMPILE_DEFINITIONS_DEBUG ${CS_DEFAULT_PREPROCESSORS_DEBUG} )
endif()
endfunction()