#include "PointCLoud.h"



OctNode* OctreeBuilder::build(Array3d*& points, const BoundingBox& bbox)
{
	if (points->elementCount() == 0)
		return 0;
	if (points->elementCount() <= m_maxLeafPointCount)
	{
		points->freeRedundantMemory();
		OctLeafNode* leaf = new OctLeafNode(points);
		leaf->m_boundingBox = bbox;
		return leaf;
	}
	else
	{
		OctBranchNode* branch = new OctBranchNode();
		branch->m_boundingBox = bbox;
		// separate into 8 parts
		Array3d* subPoints[8];
		for (int i(0); i < 8;++i)
		{
			subPoints[i] = new Array3d();
			subPoints[i]->reserve(points->elementCount());
		}

		BoundingBox* subBoundingBox[8];
		// calculate sub bounding boxes

		Point lowerPoint = bbox.minCorner;
		Point upperPoint = bbox.maxCorner;
		Point center = bbox.center();

		subBoundingBox[0] = new BoundingBox(lowerPoint, center);

		subBoundingBox[1] = new BoundingBox(center.x, lowerPoint.y, lowerPoint.z, upperPoint.x, center.y, center.z);

		subBoundingBox[2] = new BoundingBox(lowerPoint.x, center.y, lowerPoint.z, center.x, upperPoint.y, center.z);

		subBoundingBox[3] = new BoundingBox(center.x, center.y, lowerPoint.z, upperPoint.x, upperPoint.y, center.z);

		subBoundingBox[4] = new BoundingBox(lowerPoint.x, lowerPoint.y, center.z, center.x, center.y, upperPoint.z);

		subBoundingBox[5] = new BoundingBox(center.x, lowerPoint.y, center.z, upperPoint.x, center.y, upperPoint.z);

		subBoundingBox[6] = new BoundingBox(lowerPoint.x, center.y, center.z, center.x, upperPoint.y, upperPoint.z);

		subBoundingBox[7] = new BoundingBox(center, upperPoint);
		
		for (int i(0); i < points->elementCount(); ++i)
		{
			uchar bboxIndex = 0;
			double* data = points->value(i);
			if (*data > center.x)
			{
				bboxIndex |= 1;
			}
			if (*(data+1) > center.y)
			{
				bboxIndex |= 2;
			}
			if (*(data + 2) > center.z)
			{
				bboxIndex |= 4;
			}
			subPoints[bboxIndex]->addElement(data);
		}
		delete points;

		for (int i(0); i < 8;++i)
		{
			branch->setChild(i, build(subPoints[i], *subBoundingBox[i]));
		}
		return branch;
	}

}

bool OctNode::traverse(bool(*proc)(OctNode&, void*), void* data)
{
	if (!proc(*this, data))
	{
		return false;
	}

	if (this->type() == BRANCH)
	{
		OctBranchNode* branch = dynamic_cast<OctBranchNode*>(this);
		for (int i(0); i < 8; ++i)
		{
			if (!branch->childAt(i))
			{
				continue;
			}
			if (!branch->childAt(i)->traverse(proc, data))
			{
				return false;
			}
		}
	}
	return true;
}
