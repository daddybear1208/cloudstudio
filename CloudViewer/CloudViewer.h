#ifndef CLOUDVIEWER_H
#define CLOUDVIEWER_H
#include "PointCLoud.h"

#include <qglviewer.h>
#include <camera.h>
#include <manipulatedCameraFrame.h>

#include <QDragEnterEvent>
#include <QApplication>
#include <QUrl>
#include "GLStater.h"




class CloudViewer :public QGLViewer
{
	Q_OBJECT
public:

	enum OrientationAxisDrawnArea
	{
		LeftTopArea, LeftBottomArea, RightTopArea, RightBottomArea
	};

	CloudViewer(QWidget* parent = 0, const QGLWidget* shareWidget = 0, Qt::WindowFlags flags = 0);
	virtual ~CloudViewer();

	void setSceneRoot(OctNode* root);
	void setCullingCamera(CullingCamera* camera)
	{
		m_cullingCamera = camera;
	}

protected:
	virtual void draw();
	virtual void init();
	virtual void postDraw();
	virtual void drawCoordinateAxis();
	virtual void drawAxis(float length,
		const QColor &charXColor, const QColor &charYColor, const QColor &charZColor,
		const QColor &axisXColor, const QColor &axisYColor, const QColor &axisZColor,
		float charWidthRatio, float charHeightRatio, float charShiftRatio, float arrowRadiusRatio, int arrowSubdivisions, float alphaRatio = 1.0f);


	enum CullingState
	{
		INTERSECT = 0,
		ALL_IN,
		ALL_OUT
	};


private:
	OctNode* m_root;
	void findLeafNodesRecurse(OctNode* node);
	std::vector<OctLeafNode*> m_leafNodeList;
	CullingCamera* m_cullingCamera;
	OrientationAxisDrawnArea orientation_axis_draw_area_;
	float orientation_axis_factor_;
	GLStater glstater_;
};


#endif