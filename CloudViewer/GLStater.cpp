#include <qgl.h>
#include "GLStater.h"

void GLStater::clear()
{
	_glStates.clear();
}

GLboolean GLStater::addState(GLenum cap)
{
	GLboolean flag = glIsEnabled(cap);
	_glStates[cap] = flag;
	return flag;	
}

void GLStater::removeState(GLenum cap)
{
	_glStates.remove(cap);
}

void GLStater::saveCurrent()
{
	QMap<GLenum, GLboolean>::iterator i;
	for (i = _glStates.begin(); i != _glStates.end(); ++i)
		glGetBooleanv(i.key(), &i.value());
}

void GLStater::disableAllStates()
{
	QMap<GLenum, GLboolean>::const_iterator i;
	for (i = _glStates.begin(); i != _glStates.end(); ++i)
		glDisable(i.key());
}

void GLStater::enableAllStates()
{
	QMap<GLenum, GLboolean>::const_iterator i;
	for (i = _glStates.begin(); i != _glStates.end(); ++i)
		glEnable(i.key());
}

void GLStater::restore()
{
	QMap<GLenum, GLboolean>::const_iterator i;
	for (i = _glStates.begin(); i != _glStates.end(); ++i)
	{
		if ( i.value() )
			glEnable(i.key());
		else
			glDisable(i.key());
	}
}

bool GLStater::restore(GLenum cap)
{
	QMap<GLenum, GLboolean>::const_iterator k = _glStates.constFind(cap);
	if ( k == _glStates.constEnd() )
		return false;
	if ( k.value() )
		glEnable(k.key());
	else
		glDisable(k.key());
	return true;
}

void GLStater::save(GLenum cap)
{
	addState(cap);
}