#ifndef CULLINGCAMERA_H
#define CULLINGCAMERA_H

#include <camera.h>
#include <manipulatedCameraFrame.h>
#include "common.h"

class CullingCamera: public qglviewer::Camera
{
public:

	enum CullingState
	{
		INTERSECT = 0,
		ALL_IN,
		ALL_OUT
	};

	void computeFrustumPlanesEquations() const;

	float distanceToFrustumPlane(int index, const qglviewer::Vec& pos) const;
	bool sphereIsVisible(const qglviewer::Vec& center, float radius) const;
	CullingState AABBCullingState(const BoundingBox& bbox) const;

private:
	mutable GLdouble m_frustrum[6][4];

};


#endif