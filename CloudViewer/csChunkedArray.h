#ifndef CSCHUNKEDARRAY_H
#define CSCHUNKEDARRAY_H

#ifdef _MSC_VER
#pragma warning( disable: 4251 )
#pragma warning( disable: 4530 )
#endif

#define CHUNK_INDEX_BIT_DEC 16
static const unsigned MAX_NUMBER_OF_ELEMENTS_PER_CHUNK = (1<<CHUNK_INDEX_BIT_DEC);
static const unsigned ELEMENT_INDEX_BIT_MASK = MAX_NUMBER_OF_ELEMENTS_PER_CHUNK-1;

#include <stdlib.h>
#include <string.h>
#include <algorithm>
#include <vector>

template <int D, class ElementType> class CSChunkedArray;

template <int D, class ElementType> 
class CSChunkedArray
{
public:

	CSChunkedArray()
		: m_count(0)
		, m_capacity(0)
	{
		memset(m_minVal,0,sizeof(ElementType)*D);
		memset(m_maxVal,0,sizeof(ElementType)*D);
	}

	virtual ~CSChunkedArray()
	{
		while (!m_theChunks.empty())
		{
			delete[] m_theChunks.back();
			m_theChunks.pop_back();
		}
	}

	inline unsigned elementCount() const {return m_count;}

	inline unsigned capacity() const {return m_capacity;}

	inline bool isAllocated() const {return (capacity()!=0);}

	inline unsigned dimension() const {return D;}

	inline unsigned memory() const
	{
		return D*capacity()*sizeof(ElementType)
				+ (unsigned)m_theChunks.capacity()*sizeof(ElementType*)
				+ (unsigned)m_perChunkCount.capacity()*sizeof(unsigned);
	}

	inline void freeRedundantMemory()
	{
		resize(m_count);
	}

	void clear(bool releaseMemory = true)
	{
		if (releaseMemory)
		{
			while (!m_theChunks.empty())
			{
				delete[] m_theChunks.back();
				m_theChunks.pop_back();
			}
			m_perChunkCount.clear();
			m_capacity=0;
		}

		m_count=0;
		memset(m_minVal,0,sizeof(ElementType)*D);
		memset(m_maxVal,0,sizeof(ElementType)*D);
	}


	void fill(const ElementType* fillValue=0)
	{
		if (m_theChunks.empty())
			return;

		if (!fillValue)
		{
			for (unsigned i=0;i<m_theChunks.size();++i)
				memset(m_theChunks[i],0,m_perChunkCount[i]*sizeof(ElementType)*D);
		}
		else
		{

			ElementType* _cDest = m_theChunks.front();
			const ElementType* _cSrc = _cDest;

			memcpy(_cDest,fillValue,D*sizeof(ElementType));
			_cDest += D;

			unsigned elemToFill = m_perChunkCount[0];
			unsigned elemFilled = 1;
			unsigned copySize = 1;

			//recurrence
			while (elemFilled<elemToFill)
			{
				unsigned cs = elemToFill-elemFilled;
				if (copySize<cs)
					cs=copySize;
				memcpy(_cDest,_cSrc,cs*sizeof(ElementType)*D);
				_cDest += cs*(unsigned)D;
				elemFilled += cs;
				copySize<<=1;
			}


			for (unsigned i=1;i<m_theChunks.size();++i)
				memcpy(m_theChunks[i],_cSrc,m_perChunkCount[i]*sizeof(ElementType)*D);
		}

		//done
		m_count=m_capacity;
	}

	bool reserve(unsigned newNumberOfElements)
	{
		while (m_capacity<newNumberOfElements)
		{
			if (m_theChunks.empty() || m_perChunkCount.back()==MAX_NUMBER_OF_ELEMENTS_PER_CHUNK)
			{
				m_theChunks.push_back(0);
				m_perChunkCount.push_back(0);
			}


			unsigned newNumberOfElementsForThisChunk = newNumberOfElements-m_capacity;

			unsigned freeSpaceInThisChunk = MAX_NUMBER_OF_ELEMENTS_PER_CHUNK-m_perChunkCount.back();

			if (freeSpaceInThisChunk < newNumberOfElementsForThisChunk)
				newNumberOfElementsForThisChunk = freeSpaceInThisChunk;


			void* newTable = realloc(m_theChunks.back(),(m_perChunkCount.back()+newNumberOfElementsForThisChunk)*D*sizeof(ElementType));
			if (!newTable)
			{

				if (m_perChunkCount.back()==0)
				{
					m_perChunkCount.pop_back();
					m_theChunks.pop_back();
				}
				return false;
			}
			m_theChunks.back() = (ElementType*)newTable;
			m_perChunkCount.back() += newNumberOfElementsForThisChunk;
			m_capacity += newNumberOfElementsForThisChunk;
		}

		return true;
	}

	bool resize(unsigned newNumberOfElements, bool initNewElements=false, const ElementType* valueForNewElements=0)
	{
		if (newNumberOfElements==0)
		{
			clear();
		}
		else if (newNumberOfElements>m_capacity)
		{
			if (!reserve(newNumberOfElements))
				return false;
			if (initNewElements)
			{
				for (unsigned i=m_count;i<m_capacity;++i)
					setValue(i,valueForNewElements);
			}
		}
		else
		{
			while (m_capacity > newNumberOfElements)
			{
				if (m_perChunkCount.empty())
					return true;

				unsigned spaceToFree = m_capacity-newNumberOfElements;
				unsigned numberOfElementsForThisChunk = m_perChunkCount.back();

				if (spaceToFree>=numberOfElementsForThisChunk)
				{

					m_capacity -= numberOfElementsForThisChunk;
					delete m_theChunks.back();
					m_theChunks.pop_back();
					m_perChunkCount.pop_back();
				}

				else
				{

					numberOfElementsForThisChunk -= spaceToFree;
					assert(numberOfElementsForThisChunk>0);
					void* newTable = realloc(m_theChunks.back(),numberOfElementsForThisChunk*D*sizeof(ElementType));

					if (!newTable)
						return false;
					m_theChunks.back() = (ElementType*)newTable;
					m_perChunkCount.back() = numberOfElementsForThisChunk;
					m_capacity -= spaceToFree;
				}
			}
		}

		m_count=m_capacity;

		return true;
	}


	void setCurrentSize(unsigned size)
	{
		if (size>=m_capacity)
		{
			assert(false);
			return;
		}

		m_count=size;
	}


	inline ElementType* operator[] (unsigned index) {return value(index);}


	inline void addElement(const ElementType* newElement)
	{
		assert(m_count<m_capacity);
		setValue(m_count++,newElement);
	}


	inline ElementType* value(unsigned index) const 
	{
		assert(index<m_capacity);
		return m_theChunks[index >> CHUNK_INDEX_BIT_DEC]+((index & ELEMENT_INDEX_BIT_MASK)*D);
	}


	inline void setValue(unsigned index, const ElementType* val) 
	{
		assert(index < m_capacity); 
		memcpy(value(index), val, sizeof(ElementType) * D);
	}


	inline ElementType* minVal() {return m_minVal;}

	inline const ElementType* minVal() const {return m_minVal;}


	inline ElementType* maxVal() {return m_maxVal;}

	inline const ElementType* maxVal() const {return m_maxVal;}

	inline void setMin(const ElementType* m) {memcpy(m_minVal,m,D*sizeof(ElementType));}


	inline void setMax(const ElementType* M) {memcpy(m_maxVal,M,D*sizeof(ElementType));}

	virtual void calculateMinMaxVals()
	{
		if (m_count==0)
		{
			//all boundaries to zero
			memset(m_minVal,0,sizeof(ElementType)*D);
			memset(m_maxVal,0,sizeof(ElementType)*D);
			return;
		}

		memcpy(m_minVal,value(0),sizeof(ElementType)*D);
		memcpy(m_maxVal,m_minVal,sizeof(ElementType)*D);

		for (unsigned i=1;i<m_count;++i)
		{
			const ElementType* val = value(i);
			for (unsigned j=0;j<D;++j)
			{
				if (val[j]<m_minVal[j])
					m_minVal[j]=val[j];
				else if (val[j]>m_maxVal[j])
					m_maxVal[j]=val[j];
			}
		}
	}

	void swap(unsigned firstElementIndex, unsigned secondElementIndex)
	{
		assert(firstElementIndex < m_count && secondElementIndex < m_count);
		ElementType* v1 = value(firstElementIndex);
		ElementType* v2 = value(secondElementIndex);

		ElementType tempVal[D];
		memcpy(tempVal, v1, D*sizeof(ElementType));
		memcpy(v1, v2, D*sizeof(ElementType));
		memcpy(v2, tempVal, D*sizeof(ElementType));
	}


	inline unsigned chunksCount() const { return (unsigned)m_theChunks.size(); }


	inline unsigned chunkSize(unsigned index) const { assert(index < m_theChunks.size()); return m_perChunkCount[index]; }


	inline ElementType* chunkStartPtr(unsigned index) const { assert(index < m_theChunks.size()); return m_theChunks[index]; }


	bool copy(CSChunkedArray<D,ElementType>& dest) const
	{
		unsigned count = elementCount();
		if (!dest.resize(count))
			return false;
		
		//copy content		
		unsigned copyCount = 0;
		assert(dest.m_theChunks.size() <= m_theChunks.size());
		for (unsigned i=0; i<dest.m_theChunks.size(); ++i)
		{
			unsigned toCopyCount = std::min<unsigned>(count-copyCount,m_perChunkCount[i]);
			assert(dest.m_perChunkCount[i] >= toCopyCount);
			memcpy(dest.m_theChunks[i],m_theChunks[i],toCopyCount*sizeof(ElementType)*D);
			copyCount += toCopyCount;
		}
		return true;
	}

protected:

	//! Minimum values stored in array (along each dimension)
	ElementType m_minVal[D];

	//! Maximum values stored in array (along each dimension)
	ElementType m_maxVal[D];

	//! Arrays 'chunks'
	std::vector<ElementType*> m_theChunks;
	//! Elements per chunk
	std::vector<unsigned> m_perChunkCount;
	//! Total number of elements
	unsigned m_count;
	//! Max total number of elements
	unsigned m_capacity;

};


template <class ElementType> 
class CSChunkedArray<1,ElementType>
{
public:

	CSChunkedArray()
		: m_minVal(0)
		, m_maxVal(0)
		, m_count(0)
		, m_capacity(0)
	{
	}

	virtual ~CSChunkedArray()
	{
		while (!m_theChunks.empty())
		{
			delete[] m_theChunks.back();
			m_theChunks.pop_back();
		}
	}

	inline unsigned elementCount() const {return m_count;}

	inline unsigned capacity() const {return m_capacity;}

	inline bool isAllocated() const {return (capacity()!=0);}

	inline unsigned dimension() const {return 1;}


	inline unsigned memory() const
	{
		return capacity()*sizeof(ElementType)
				+ (unsigned)m_theChunks.capacity()*sizeof(ElementType*)
				+ (unsigned)m_perChunkCount.capacity()*sizeof(unsigned);
	}

	void clear(bool releaseMemory = true)
	{
		if (releaseMemory)
		{
			while (!m_theChunks.empty())
			{
				delete[] m_theChunks.back();
				m_theChunks.pop_back();
			}
			m_perChunkCount.clear();
			m_capacity=0;
		}

		m_count=0;
		m_minVal=m_maxVal=0;
	}


	void fill(const ElementType& fillValue=0)
	{
		if (m_theChunks.empty())
			return;

		if (fillValue==0)
		{
			//default fill value = 0
			for (unsigned i=0;i<m_theChunks.size();++i)
				memset(m_theChunks[i],0,m_perChunkCount[i]*sizeof(ElementType));
		}
		else
		{
			//we initialize the first chunk properly
			//with a recursive copy of 2^k bytes (k=0,1,2,...)
			ElementType* _cDest = m_theChunks.front();
			const ElementType* _cSrc = _cDest;
			//we copy only the first element to init recurrence
			*_cDest++ = fillValue;

			unsigned elemToFill = m_perChunkCount[0];
			unsigned elemFilled = 1;
			unsigned copySize = 1;

			//recurrence
			while (elemFilled<elemToFill)
			{
				unsigned cs = elemToFill-elemFilled;
				if (copySize<cs)
					cs=copySize;
				memcpy(_cDest,_cSrc,cs*sizeof(ElementType));
				_cDest += cs;
				elemFilled += cs;
				copySize<<=1;
			}

			//then we simply have to copy the first chunk to the other ones
			for (unsigned i=1;i<m_theChunks.size();++i)
				memcpy(m_theChunks[i],_cSrc,m_perChunkCount[i]*sizeof(ElementType));
		}

		//done
		m_count=m_capacity;
	}

	bool reserve(unsigned newNumberOfElements)
	{
		while (m_capacity<newNumberOfElements)
		{
			if (m_theChunks.empty() || m_perChunkCount.back()==MAX_NUMBER_OF_ELEMENTS_PER_CHUNK)
			{
				m_theChunks.push_back(0);
				m_perChunkCount.push_back(0);
			}

			unsigned newNumberOfElementsForThisChunk = newNumberOfElements-m_capacity;

			unsigned freeSpaceInThisChunk = MAX_NUMBER_OF_ELEMENTS_PER_CHUNK-m_perChunkCount.back();

			if (freeSpaceInThisChunk < newNumberOfElementsForThisChunk)
				newNumberOfElementsForThisChunk = freeSpaceInThisChunk;


			void* newTable = realloc(m_theChunks.back(),(m_perChunkCount.back()+newNumberOfElementsForThisChunk)*sizeof(ElementType));

			if (!newTable)
			{

				if (m_perChunkCount.back()==0)
				{
					m_perChunkCount.pop_back();
					m_theChunks.pop_back();
				}
				return false;
			}

			m_theChunks.back() = (ElementType*)newTable;
			m_perChunkCount.back() += newNumberOfElementsForThisChunk;
			m_capacity += newNumberOfElementsForThisChunk;
		}

		return true;
	}


	bool resize(unsigned newNumberOfElements, bool initNewElements=false, const ElementType& valueForNewElements=0)
	{

		if (newNumberOfElements==0)
		{
			clear();
		}

		else if (newNumberOfElements>m_capacity)
		{
			if (!reserve(newNumberOfElements))
				return false;

			if (initNewElements)
			{

				for (unsigned i=m_count;i<m_capacity;++i)
					setValue(i,valueForNewElements);
			}
		}
		else
		{
			while (m_capacity > newNumberOfElements)
			{

				if (m_perChunkCount.empty())
					return true;


				unsigned spaceToFree = m_capacity-newNumberOfElements;

				unsigned numberOfElementsForThisChunk = m_perChunkCount.back();


				if (spaceToFree>=numberOfElementsForThisChunk)
				{

					m_capacity -= numberOfElementsForThisChunk;
					delete m_theChunks.back();
					m_theChunks.pop_back();
					m_perChunkCount.pop_back();
				}

				else
				{

					numberOfElementsForThisChunk -= spaceToFree;
					assert(numberOfElementsForThisChunk>0);
					void* newTable = realloc(m_theChunks.back(),numberOfElementsForThisChunk*sizeof(ElementType));

					if (!newTable)
						return false;
					m_theChunks.back() = (ElementType*)newTable;
					m_perChunkCount.back() = numberOfElementsForThisChunk;
					m_capacity -= spaceToFree;
				}
			}
		}

		m_count=m_capacity;

		return true;
	}

	void setCurrentSize(unsigned size)
	{
		if (size>=m_capacity)
		{
			assert(false);
			return;
		}

		m_count=size;
	}


	inline ElementType& operator[] (unsigned index)
	{
		assert(index<m_capacity);
		return m_theChunks[index >> CHUNK_INDEX_BIT_DEC][index & ELEMENT_INDEX_BIT_MASK];
	}

	inline void addElement(const ElementType& newElement)
	{
		assert(m_count<m_capacity);
		setValue(m_count++,newElement);
	}


	inline const ElementType& value(unsigned index) const
	{
		assert(index<m_capacity);
		return m_theChunks[index >> CHUNK_INDEX_BIT_DEC][index & ELEMENT_INDEX_BIT_MASK];
	}


	inline void setValue(unsigned index, const ElementType& value) {(*this)[index]=value;}

	inline ElementType minVal() {return m_minVal;}


	inline const ElementType minVal() const {return m_minVal;}

	inline ElementType maxVal() {return m_maxVal;}


	inline const ElementType maxVal() const {return m_maxVal;}

	inline void setMin(const ElementType& m) {m_minVal = m;}

	inline void setMax(const ElementType& M) {m_maxVal = M;}

	virtual void calculateMinMaxVals()
	{
		if (m_capacity==0)
		{
			m_minVal = m_maxVal = 0;
			return;
		}

		m_minVal = m_minVal = value(0);

		//we update boundaries with all other values
		for (unsigned i=1;i<m_capacity;++i)
		{
			const ElementType& val = value(i);
			if (val<m_minVal)
				m_minVal=val;
			else if (val>m_maxVal)
				m_maxVal=val;
		}
	}

	inline void swap(unsigned firstElementIndex, unsigned secondElementIndex)
	{
		assert(firstElementIndex < m_count && secondElementIndex < m_count);
		ElementType& v1 = (*this)[firstElementIndex];
		ElementType& v2 = (*this)[secondElementIndex];
		ElementType temp = v1;
		v1 = v2;
		v2 = temp;
	}

	inline unsigned chunksCount() const { return (unsigned)m_theChunks.size(); }


	inline unsigned chunkSize(unsigned index) const { assert(index < m_theChunks.size()); return m_perChunkCount[index]; }

	inline ElementType* chunkStartPtr(unsigned index) const { assert(index < m_theChunks.size()); return m_theChunks[index]; }


	bool copy(CSChunkedArray<1,ElementType>& dest) const
	{
		unsigned count = elementCount();
		if (!dest.resize(count))
			return false;
		
		//copy content		
		unsigned copyCount = 0;
		assert(dest.m_theChunks.size() <= m_theChunks.size());
		for (unsigned i=0; i<dest.m_theChunks.size(); ++i)
		{
			unsigned toCopyCount = std::min<unsigned>(count-copyCount,m_perChunkCount[i]);
			assert(dest.m_perChunkCount[i] >= toCopyCount);
			memcpy(dest.m_theChunks[i],m_theChunks[i],toCopyCount*sizeof(ElementType));
			copyCount += toCopyCount;
		}
		return true;
	}

protected:

	//! Minimum values stored in array (along each dimension)
	ElementType m_minVal;

	//! Maximum values stored in array (along each dimension)
	ElementType m_maxVal;

	//! Arrays 'chunks'
	std::vector<ElementType*> m_theChunks;
	//! Elements per chunk
	std::vector<unsigned> m_perChunkCount;
	//! Total number of elements
	unsigned m_count;
	//! Max total number of elements
	unsigned m_capacity;

};

#endif
