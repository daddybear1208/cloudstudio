#ifndef CONSOLE_H
#define CONSOLE_H

#include <stdlib.h>
#include <string>
#include <vector>

using namespace std;

extern int findArgument(int argc, char** argv, const char* arg);
extern int parseArguments(int argc, char** argv, const char* arg, std::string& val);
extern int parseArguments(int argc, char** argv, const char* arg, bool &val);
extern int parseArguments(int argc, char** argv, const char* arg, int& val);
extern int parseArguments(int argc, char** argv, const char* arg, float& val);
extern int parseArguments(int argc, char** argv, const char* arg, double& val);
extern int parseArguments(int argc, char** argv, const char* arg, char& val);
extern int parseArguments_2x(int argc, char** argv, const char* arg, int& val1, int& val2);
extern int parseArguments_2x(int argc, char** argv, const char* arg, float& val1, float& val2);
extern int parseArguments_2x(int argc, char** argv, const char* arg, double& val1, double& val2);
extern int parseArguments_3x(int argc, char** argv, const char* arg, int& val1, int& val2, int& val3);
extern int parseArguments_3x(int argc, char** argv, const char* arg, float& val1, float& val2, float& val3);
extern int parseArguments_3x(int argc, char** argv, const char* arg, double& val1, double& val2, double& val3);
extern int parseArguments_x(int argc, char** argv, const char* arg, std::vector<int>& vals);
extern int parseArguments_x(int argc, char** argv, const char* arg, std::vector<float>& vals);
extern int parseArguments_x(int argc, char** argv, const char* arg, std::vector<double>& vals);



#endif