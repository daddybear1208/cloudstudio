#include <iostream>
#include "CloudViewer.h"
#include <QtGui>
#include <QApplication>
#include <fstream>
#include <liblas/liblas.hpp>
#include <QFileInfo>
#include "Console.h"



void printHelpString(const char* progName)
{
	std::cout << "A las visualization tool based on frustrum culling and LOD\n"
		<< "\nUsage: " << progName << " -i [input file]\n\n"
		<< "Options:\n"
		<< "-------------------------------------------\n"
		<< "-h           help message\n"
		<< "-i			 input las file"
		<< "\n";
}

int main(int argc, char* argv[])
{
	QApplication app(argc, argv);
	if (findArgument(argc, argv, "-h") > 0)
	{
		printHelpString(argv[0]);
		return 0;
	}
	std::string fileName;
	if (findArgument(argc, argv, "-i") < 0)
	{
		cout << "invalid options";
		printHelpString(argv[0]);
		exit(-1);
	}
	else
	{
		if (parseArguments(argc, argv, "-i", fileName) == -1)
		{
			cout << "invalid options";
			printHelpString(argv[0]);
			exit(-1);
		}
	}
	
	// read las file
	std::ifstream ifs;
	ifs.open(fileName.c_str(), std::ios::in | std::ios::binary);
	if (!ifs)
	{
		cout << "error opening file for reading\n";
		exit(-1);
	}
	CSChunkedArray<3, double>* points = new CSChunkedArray<3, double>();
	liblas::ReaderFactory f;
	liblas::Reader reader = f.CreateWithStream(ifs);
	liblas::Header const& header = reader.GetHeader();
	
	int pointCount = header.GetPointRecordsCount();
	if (!points->reserve(pointCount))
	{
		points->clear();
		delete points;
		cout << "cannot allocate memory\n";
		ifs.close();
		exit(-1);
	}
	
	int progress = 0;
	int pointRead = 0;
	float progressStep = (float)pointCount / 100;
	
	QProgressDialog* progressDlg = new QProgressDialog();
	progressDlg->setLabelText("loading point cloud");
	// default range: 0-100
	progressDlg->setRange(0, 100);
	// no cancel button
	progressDlg->setCancelButton(0);
	// delete on close
	progressDlg->setAttribute(Qt::WA_DeleteOnClose);
	// show immediately
	progressDlg->setMinimumDuration(0);
	progressDlg->setWindowModality(Qt::WindowModal);
	progressDlg->setAutoClose(false);
	
	while (reader.ReadNextPoint())
	{
		liblas::Point const& p = reader.GetPoint();
		double data[3] = { p.GetX(), p.GetY(), p.GetZ() };
		points->addElement(data);
		if (progress == (int)(++pointRead / progressStep))
			continue;
		else
			progressDlg->setValue(++progress);
	}
	progressDlg->close();
	ifs.close();
	OctreeBuilder b;
	points->calculateMinMaxVals();
	const double* minVals = points->minVal();
	const double* maxVals = points->maxVal();
	cout << "building octree\n";
	OctNode* root = b.build(points, BoundingBox(minVals[0], minVals[1], minVals[2], maxVals[0], maxVals[1], maxVals[2]));
	if (!root)
	{
		delete points;
		cout << "cannot build octree\n";
		exit(-1);
	}

	cout << "visualization started\n";

	CloudViewer* viewer = new CloudViewer;

	qglviewer::Camera* c = viewer->camera();
	CullingCamera* cc = new CullingCamera();
	viewer->setCamera(cc);
	delete c;

	viewer->setCullingCamera(cc);
	viewer->setSceneRoot(root);
	viewer->show();

	return app.exec();
}

