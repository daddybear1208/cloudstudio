#include "Console.h"

#include <iostream>


int findArgument(int argc, char** argv, const char* arg)
{
	for (int i = 1; i < argc; ++i)
	{
		if (strcmp (argv[i], arg) == 0)
		{
			return (i);
		}
	}
	return (-1);
}

int parseArguments(int argc, char** argv, const char* arg, std::string& val)
{
	int index = findArgument(argc, argv, arg) + 1;
	if (index > 1 && index < argc )
		val = argv[index];
	else
		return -1;

	return (index-1);
}

int parseArguments(int argc, char** argv, const char* arg, bool &val)
{
	//use 1 or 0 to define the true or false
	int index = findArgument(argc, argv, arg) + 1;

	if (index > 1 && index < argc )
		val = atoi(argv[index]) == 1;
	else
		return -1;

	return (index-1);
}
int parseArguments(int argc, char** argv, const char* arg, int& val)
{
	int index = findArgument(argc, argv, arg) + 1;

	if (index > 1 && index < argc )
		val = atoi (argv[index]);
	else
		return -1;

	return (index-1);
}
int parseArguments(int argc, char** argv, const char* arg, float& val)
{
	int index = findArgument(argc, argv, arg) + 1;

	if (index > 1 && index < argc )
		val = static_cast<float> (atof (argv[index]));
	else
		return -1;

	return (index-1);
}
int parseArguments(int argc, char** argv, const char* arg, double& val)
{
	int index = findArgument(argc, argv, arg) + 1;

	if (index > 1 && index < argc )
		val = atof (argv[index]);
	else
		return -1;

	return (index-1);
}
int parseArguments(int argc, char** argv, const char* arg, char& val)
{
	int index = findArgument (argc, argv, arg) + 1;

	if (index > 1 && index < argc )
		val = argv[index][0];
	else
		return -1;

	return (index - 1);
}
int parseArguments_2x(int argc, char** argv, const char* arg, int& val1, int& val2)
{
	int index = findArgument(argc, argv, arg);
	if ( (index>0) && ((index+2) < argc) )
	{
		val1 = atoi(argv[index+1]);
		val2 = atoi(argv[index+2]);
	}
	return index;
}
int parseArguments_2x(int argc, char** argv, const char* arg, float& val1, float& val2)
{
	int index = findArgument(argc, argv, arg);
	if ( (index>0) && ((index+2) < argc) )
	{
		val1 = static_cast<float> (atof (argv[index+1]));
		val2 = static_cast<float> (atof (argv[index+2]));
	}
	return index;
}
int parseArguments_2x(int argc, char** argv, const char* arg, double& val1, double& val2)
{
	int index = findArgument(argc, argv, arg);
	if ( (index>0) && ((index+2) < argc) )
	{
		val1 = atof(argv[index+1]);
		val2 = atof(argv[index+2]);
	}
	return index;
}
int parseArguments_3x(int argc, char** argv, const char* arg, int& val1, int& val2, int& val3)
{
	int index = findArgument(argc, argv, arg);
	if ( (index>0) && ((index+3) < argc) )
	{
		val1 = atoi(argv[index+1]);
		val2 = atoi(argv[index+2]);
		val3 = atoi(argv[index+3]);
	}
	return index;
}
int parseArguments_3x(int argc, char** argv, const char* arg, float& val1, float& val2, float& val3)
{
	int index = findArgument(argc, argv, arg);
	if ( (index>0) && ((index+3) < argc) )
	{
		val1 = static_cast<float> (atof (argv[index+1]));
		val2 = static_cast<float> (atof (argv[index+2]));
		val3 = static_cast<float> (atof (argv[index+3]));
	}
	return index;
}
int parseArguments_3x(int argc, char** argv, const char* arg, double& val1, double& val2, double& val3)
{
	int index = findArgument(argc, argv, arg);
	if ( (index>0) && ((index+3) < argc) )
	{
		val1 = atof(argv[index+1]);
		val2 = atof(argv[index+2]);
		val3 = atof(argv[index+3]);
	}

	return index;
}
int parseArguments_x(int argc, char** argv, const char* arg, std::vector<int>& vals)
{
	int index = findArgument(argc, argv, arg);
	if ((index>0) && (index+vals.size()<argc))
	{
		for (int i=0;i<vals.size();++i)
		{
			vals[i] = atoi(argv[index+1+i]);
		}
	}

	return index;
}
int parseArguments_x(int argc, char** argv, const char* arg, std::vector<float>& vals)
{
	int index = findArgument(argc, argv, arg);
	if ((index>0) && (index+vals.size()<argc))
	{
		for (int i=0;i<vals.size();++i)
		{
			vals[i] = static_cast<float> (atof(argv[index+i+1]));
		}
	}

	return index;
}
int parseArguments_x(int argc, char** argv, const char* arg, std::vector<double>& vals)
{
	int index = findArgument(argc, argv, arg);
	if ((index>0) && (index+vals.size()<argc))
	{
		for (int i=0;i<vals.size();++i)
		{
			vals[i] = atof(argv[index+1+i]);
		}
	}

	return index;
}


