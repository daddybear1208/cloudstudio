#include "CullingCamera.h"


void CullingCamera::computeFrustumPlanesEquations() const
{
	getFrustumPlanesCoefficients(m_frustrum);
}

float CullingCamera::distanceToFrustumPlane(int index, const qglviewer::Vec& pos) const
{
	return pos * qglviewer::Vec(m_frustrum[index]) - m_frustrum[index][3];
}

bool CullingCamera::sphereIsVisible(const qglviewer::Vec& center, float radius) const
{
	for (int i(0); i < 6; ++i)
	{
		if (distanceToFrustumPlane(i, center) > radius)
			return false;
	}
	return true;
}

CullingCamera::CullingState CullingCamera::AABBCullingState(const BoundingBox& bbox) const
{
	int totalIn = 0;
	for (int i(0); i < 6; ++i)
	{
		int inCount = 8;
		for (unsigned int j(0); j< 8; ++j)
		{
			const qglviewer::Vec pos((j & 4) ? bbox.minCorner.x : bbox.maxCorner.x,
				(j & 2) ? bbox.minCorner.y : bbox.maxCorner.y, (j & 1) ? bbox.minCorner.z : bbox.maxCorner.z);
			if (distanceToFrustumPlane(i, pos) > 0.0)
			{
				--inCount;
			}
		}
		if (!inCount)
		{
			return ALL_OUT;
		}
		else if (inCount == 8)
		{
			++totalIn;
		}
	}

	if (totalIn == 6)
		return ALL_IN;
	return INTERSECT;
}


