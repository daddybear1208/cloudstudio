#include "CloudViewer.h"
#include "csChunkedArray.h"
#include <math.h>


CloudViewer::CloudViewer(QWidget* parent /*= 0*/, const QGLWidget* shareWidget /*= 0*/, Qt::WindowFlags flags /*= 0*/)
	:QGLViewer(parent, shareWidget, flags),
	m_root(0),
	orientation_axis_draw_area_(LeftBottomArea),
	orientation_axis_factor_(0.08f)
{
	setFPSIsDisplayed(true);
	setAttribute(Qt::WA_DeleteOnClose);
	setForegroundColor(Qt::white);
}

CloudViewer::~CloudViewer()
{
	if (m_root)
		delete m_root;
}

void CloudViewer::draw()
{
	if (!m_root)
		return;
	if (camera()->frame()->isManipulated() || camera()->frame()->isSpinning())
	{
		m_root->cullingDraw(m_cullingCamera, true, true);
	}
	else
	{
		m_root->cullingDraw(m_cullingCamera, true, false);
	}

	m_cullingCamera->computeFrustumPlanesEquations();


}

void CloudViewer::init()
{
	GLenum err = glewInit();
	if (GLEW_OK != err)
	{
		/* Problem: glewInit failed, something is seriously wrong. */
		fprintf(stderr, "Error: %s\n", glewGetErrorString(err));
	}
	else
	{
		fprintf(stdout, "Status: Using GLEW %s\n", glewGetString(GLEW_VERSION));
	}
	glClearColor(0.0, 0.0, 0.0, 0.0);
	glDisable(GL_LIGHTING);
}


void CloudViewer::setSceneRoot(OctNode* root)
{
	m_root = root;
 	setSceneBoundingBox(qglviewer::Vec(root->boundingBox().minCorner.x, root->boundingBox().minCorner.y, root->boundingBox().minCorner.z),
 		qglviewer::Vec(root->boundingBox().maxCorner.x, root->boundingBox().maxCorner.y, root->boundingBox().maxCorner.z));
 	showEntireScene();
}

void CloudViewer::postDraw()
{
	drawCoordinateAxis();
	QGLViewer::postDraw();
}

void CloudViewer::drawCoordinateAxis()
{
	int viewport[4];
	int scissor[4];

	// The viewport and the scissor are changed to fit the lower left
	// corner. Original values are saved.
	glGetIntegerv(GL_VIEWPORT, viewport);
	glGetIntegerv(GL_SCISSOR_BOX, scissor);

	// Axis viewport size, in pixels
	uint width = viewport[2];
	uint height = viewport[3];
	int maxSize = qMax<int>(int(qMax<int>(width, height) * orientation_axis_factor_), 50);
	int lbx = 0, lby = 0;
	orientation_axis_draw_area_ = LeftBottomArea;
	switch (orientation_axis_draw_area_)
	{
	case LeftTopArea:    	lby = height - maxSize; break;
	case LeftBottomArea:    break;
	case RightTopArea:      lbx = width - maxSize, lby = height - maxSize; break;
	case RightBottomArea:   lbx = width - maxSize; break;
	}
	glViewport(lbx, lby, maxSize, maxSize);
	glScissor(lbx, lby, maxSize, maxSize);

	// The Z-buffer is cleared to make the axis appear over the
	// original image.
	//glClear(GL_DEPTH_BUFFER_BIT);
	GLStater stater;
	stater << GL_DEPTH_TEST << GL_LIGHTING;
	//glDisable(GL_DEPTH_TEST);

	// Tune for best line rendering
	glDisable(GL_LIGHTING);
	//glLineWidth(3.0);

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	glOrtho(-1, 1, -1, 1, -1, 1);

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();
	glMultMatrixd(camera()->orientation().inverse().matrix());

	drawAxis(1.0*0.9f, QColor(255, 50, 50), QColor(50, 255, 50), QColor(50, 50, 255),
		QColor(255, 0, 0, 200), QColor(0, 255, 0, 200), QColor(0, 0, 255, 200),
		1.0f / 15.0f, 1.0f / 10.0f, 1.05f, 0.045f, 6);

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	//glEnable(GL_LIGHTING);

	stater.restore();
	// The viewport and the scissor are restored.
	//glScissor(scissor[0],scissor[1],scissor[2],scissor[3]);
	glScissor(viewport[0], viewport[1], viewport[2], viewport[3]);
	glViewport(viewport[0], viewport[1], viewport[2], viewport[3]);
}

void CloudViewer::drawAxis(float length, const QColor &charXColor, const QColor &charYColor, const QColor &charZColor, const QColor &axisXColor, const QColor &axisYColor, const QColor &axisZColor, float charWidthRatio, float charHeightRatio, float charShiftRatio, float arrowRadiusRatio, int arrowSubdivisions, float alphaRatio /*= 1.0f*/)
{
	if (length < 0.0f)
		length = sceneRadius();
	const float charWidth = length * charWidthRatio;
	const float charHeight = length * charHeightRatio;
	const float charShift = charShiftRatio * length;

	GLStater glStater;
	glStater << GL_LIGHTING << GL_COLOR_MATERIAL << GL_LINE_SMOOTH << GL_BLEND;

	glDisable(GL_LIGHTING);

	glEnable(GL_LINE_SMOOTH);
	glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);  // Antialias the lines   
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glBegin(GL_LINES);
	// The X
	glColor4f(charXColor.redF(), charXColor.greenF(), charXColor.blueF(), charXColor.alphaF()*alphaRatio);
	glVertex3f(charShift, charWidth, -charHeight);
	glVertex3f(charShift, -charWidth, charHeight);
	glVertex3f(charShift, -charWidth, -charHeight);
	glVertex3f(charShift, charWidth, charHeight);
	// The Y
	glColor4f(charYColor.redF(), charYColor.greenF(), charYColor.blueF(), charYColor.alphaF()*alphaRatio);
	glVertex3f(charWidth, charShift, charHeight);
	glVertex3f(0.0, charShift, 0.0);
	glVertex3f(-charWidth, charShift, charHeight);
	glVertex3f(0.0, charShift, 0.0);
	glVertex3f(0.0, charShift, 0.0);
	glVertex3f(0.0, charShift, -charHeight);
	// The Z
	glColor4f(charZColor.redF(), charZColor.greenF(), charZColor.blueF(), charZColor.alphaF()*alphaRatio);
	glVertex3f(-charWidth, charHeight, charShift);
	glVertex3f(charWidth, charHeight, charShift);
	glVertex3f(charWidth, charHeight, charShift);
	glVertex3f(-charWidth, -charHeight, charShift);
	glVertex3f(-charWidth, -charHeight, charShift);
	glVertex3f(charWidth, -charHeight, charShift);
	glEnd();

	glEnable(GL_LIGHTING);
	glDisable(GL_COLOR_MATERIAL);

	float color[4];
	color[0] = axisXColor.redF();  color[1] = axisXColor.greenF();  color[2] = axisXColor.blueF();  color[3] = axisXColor.alphaF()*alphaRatio;
	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, color);
	QGLViewer::drawArrow(length, arrowRadiusRatio*length, arrowSubdivisions);

	color[0] = axisYColor.redF();  color[1] = axisYColor.greenF();  color[2] = axisYColor.blueF();  color[3] = axisYColor.alphaF()*alphaRatio;
	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, color);
	glPushMatrix();
	glRotatef(90.0, 0.0, 1.0, 0.0);
	QGLViewer::drawArrow(length, arrowRadiusRatio*length, arrowSubdivisions);
	glPopMatrix();

	color[0] = axisZColor.redF();  color[1] = axisZColor.greenF();  color[2] = axisZColor.blueF();  color[3] = axisZColor.alphaF()*alphaRatio;
	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, color);
	glPushMatrix();
	glRotatef(-90.0, 1.0, 0.0, 0.0);
	QGLViewer::drawArrow(length, arrowRadiusRatio*length, arrowSubdivisions);
	glPopMatrix();

	glStater.restore();
}



