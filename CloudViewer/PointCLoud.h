#ifndef POINTCLOUD_H
#define POINTCLOUD_H

#include <GL/glew.h>

#include <QObject>
#include <QtOpenGL/QGLBuffer>

#ifdef  Q_OS_WIN32
#include <windows.h>
#endif //  _WIN32

#ifdef Q_OS_MAC
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#else
#include <GL/gl.h>
#include <GL/glu.h>
#endif

#include <assert.h>
#include <vector>
using namespace std;

#include "csChunkedArray.h"
#include "CullingCamera.h"

typedef unsigned char uchar;
typedef CSChunkedArray<3, double> Array3d;
//typedef bool(*callback)(const OctNode&, void*);

#define BUFFER_OFFSET(i) ((char*)NULL + (i))

class VBO :public QGLBuffer
{
public:

	enum VboState
	{
		INITIALIZED = 0,
		INITIALIZE_FAILED,
		UNINITIALIZED
	};


	VBO()
		:QGLBuffer(QGLBuffer::VertexBuffer),
		state(UNINITIALIZED)
	{

	}

	int init(int count, bool* reallocated = 0)
	{
		int totalSize = sizeof(double) * 3 * count;
		if (!isCreated())
		{
			if (!create())
			{
				state = INITIALIZE_FAILED;
				cout << "vbo creation failed!\n";
				return -1;
			}
			setUsagePattern(QGLBuffer::DynamicDraw);
		}
		if (!bind())
		{
			state = INITIALIZE_FAILED;
			cout << "vbo binding failed!\n";
			destroy();
			return -1;
		}
		if (totalSize != size())
		{
			allocate(totalSize);
			if (reallocated)
			{
				*reallocated = true;
			}
			if (totalSize!= size())
			{
				state = INITIALIZE_FAILED;
				cout << "not enough GPU memory!\n";
				release();
				destroy();
				return -1;
			}
		}
		release();
		state = INITIALIZED;
		return totalSize;
	}

	VboState state;
};


class OctreeBuilder;
class CloudViewer;

class OctNode
{
	friend class OctreeBuilder;
public:
	explicit OctNode() {}
	virtual ~OctNode() {}


	enum NODE_TYPE : uchar
	{ 
		LEAF = 0,
		BRANCH
	};

	bool traverse(bool(*proc)(OctNode&, void*), void* data);

	bool traverse(bool(CloudViewer::*proc)(OctNode&));

	virtual NODE_TYPE type() const = 0;

	const BoundingBox& boundingBox() const
	{
		return m_boundingBox;
	}

	virtual void cullingDraw(const CullingCamera* camera, bool cullingCheck, bool vacuation) const = 0;
	
protected:
	BoundingBox m_boundingBox;
	
};

class OctLeafNode :public OctNode
{
	friend class OctreeBuilder;
public:

	explicit OctLeafNode(CSChunkedArray<3,double>* pts) 
		: OctNode(),
		m_pointArray(pts)
	{
		m_vbo = new VBO;
	}

	~OctLeafNode()
	{
		if (m_vbo)
		{
			if (m_vbo->state == VBO::INITIALIZED)
			{
				m_vbo->release();
				m_vbo->destroy();
			}
			delete m_vbo;
			m_vbo = 0;
		}
		delete m_pointArray;
		m_pointArray = 0;
	}

	virtual NODE_TYPE type() const
	{
		return LEAF;
	}

	void drawSelf(int level = 0) const
	{
		glColor3f(1.0, 1.0, 1.0);

		switch (m_vbo->state)
		{
		case VBO::INITIALIZED:
		{
			m_vbo->bind();
			glVertexPointer(3, GL_DOUBLE, level * 3 * sizeof(double), BUFFER_OFFSET(0));
			m_vbo->release();
			break;
		}

		case VBO::UNINITIALIZED:
		{
			// try to initialize vbo
			m_vbo->init(m_pointArray->elementCount());
			if (m_vbo->state != VBO::INITIALIZED)
			{
				glVertexPointer(3, GL_DOUBLE, level * 3 * sizeof(double), m_pointArray->chunkStartPtr(0));
			}
			else
			{
				m_vbo->bind();
				m_vbo->write(0, m_pointArray->chunkStartPtr(0), m_pointArray->elementCount()*3*sizeof(double));
				glVertexPointer(3, GL_DOUBLE, level * 3 * sizeof(double), BUFFER_OFFSET(0));
				m_vbo->release();
			}
			break;
		}
		
		case VBO::INITIALIZE_FAILED:
		{
			glVertexPointer(3, GL_DOUBLE, level * 3 * sizeof(double), m_pointArray->chunkStartPtr(0));
			break;
		}

		default:
			break;
		}

		glEnableClientState(GL_VERTEX_ARRAY);
		unsigned int size = m_pointArray->elementCount();
		if (level)
		{
			size = static_cast<unsigned>(floor(static_cast<float>(m_pointArray->elementCount()) / level));
		}
		glDrawArrays(GL_POINTS, 0, size);
		glDisableClientState(GL_VERTEX_ARRAY);

		//m_boundingBox.drawSelf();
	}


	// cullingCheck is useless
	void cullingDraw(const CullingCamera* camera, bool cullingCheck, bool vacuation) const
	{
		if (vacuation)
			drawSelf(VacuationLevel);
		else
			drawSelf();
	}

	int pointCount() const
	{
		return m_pointArray->elementCount();
	}

private:
	CSChunkedArray<3, double>* m_pointArray;
	enum { VacuationLevel = 10 };
	VBO* m_vbo;
};

class OctBranchNode :public OctNode
{
	friend class OctreeBuilder;
public:
	explicit OctBranchNode() : OctNode()
	{
		memset(m_childs, 0, sizeof(m_childs));
	}

	~OctBranchNode()
	{
		for (int i(0); i < 8; ++i)
		{
			if (m_childs[i])
			{
				delete m_childs[i];
				m_childs[i] = 0;
			}
				
		}
	}

	virtual NODE_TYPE type() const
	{
		return BRANCH;
	}

	OctNode* childAt(int index)
	{
		assert(index < 8 && index >= 0);
		return m_childs[index];
	}

	void setChild(int index, OctNode* child)
	{
		assert(index < 8 && index >= 0);
		m_childs[index] = child;
	}

	void cullingDraw(const CullingCamera* camera, bool cullingCheck, bool vacuation) const
	{
		if (!cullingCheck)
		{
			// directly draw all
			for (int i(0); i < 8; ++i)
			{
				if (m_childs[i])
				{
					m_childs[i]->cullingDraw(camera, false, vacuation);
				}
			}
		}
		else
		{
			CullingCamera::CullingState state = camera->AABBCullingState(m_boundingBox);
			if (state == CullingCamera::ALL_OUT)
			{
				return;
			}
			else if (state == CullingCamera::ALL_IN)
			{
				for (int i(0); i < 8; ++i)
				{
					if (m_childs[i])
					{
						m_childs[i]->cullingDraw(camera, false, vacuation);
					}
				}
			}
			else
			{
				for (int i(0); i < 8; ++i)
				{
					if (m_childs[i])
					{
						m_childs[i]->cullingDraw(camera, true, vacuation);
					}
				}
			}
		}


	}



private:
	OctNode* m_childs[8];
};


class OctPointCloud
{
public:
	explicit OctPointCloud()
		:m_rootNode(0)
	{

	}

	~OctPointCloud()
	{
		if (m_rootNode)
		{
			delete m_rootNode;
			m_rootNode = 0;
		}
	}

	void setOctRoot(OctNode* root)
	{
		m_rootNode = root;
	}

	

private:
	OctNode* m_rootNode;
};


class OctreeBuilder
{
public: 
	OctreeBuilder() :m_maxLeafPointCount(1000)
	{
	}
	

	OctNode* build(Array3d*& points, const BoundingBox& bbox);

	void setMaxLeafPointCount(int i)
	{
		m_maxLeafPointCount = i;
	}
	int maxLeafPointCount() const
	{
		return m_maxLeafPointCount;
	}

private:
	int m_maxLeafPointCount;
};






#endif