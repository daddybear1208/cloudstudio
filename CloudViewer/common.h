#ifndef COMMON_H
#define COMMON_H

#ifdef  Q_OS_WIN32
#include <windows.h>
#endif //  _WIN32

#ifdef Q_OS_MAC
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#else
#include <GL/gl.h>
#include <GL/glu.h>
#endif

#include <stdlib.h>
#include <iostream>


using namespace std;
union Point
{
	Point()
		:x(0), y(0), z(0)
	{

	}

	Point(const Point& pt)
	{
		memcpy(data, pt.data, 3 * sizeof(double));
	}

	Point& operator=(const Point& pt)
	{
		memcpy(data, pt.data, 3 * sizeof(double));
		return (*this);
	}

	bool operator==(const Point& pt)
	{
		for (int i(0); i < 3; ++i)
		{
			if (data[i] != pt.data[i])
			{
				return false;
			}
		}
		return true;
	}

	Point(double x_, double y_, double z_)
		:x(x_), y(y_), z(z_)
	{}

	Point(double* d)
	{
		memcpy(data, d, 3 * sizeof(double));
	}

	double data[3];

	struct
	{
		double x;
		double y;
		double z;
	};
};

struct BoundingBox
{
	BoundingBox()
		:minCorner(), maxCorner()
	{

	}

	BoundingBox(double xMin, double yMin, double zMin, double xMax, double yMax, double zMax)
	{
		minCorner.x = xMin;
		minCorner.y = yMin;
		minCorner.z = zMin;
		maxCorner.x = xMax;
		maxCorner.y = yMax;
		maxCorner.z = zMax;
	}

	BoundingBox(Point min, Point max)
		:minCorner(min), maxCorner(max)
	{

	}

	Point center() const
	{
		Point c;
		c.x = 0.5 * (minCorner.x + maxCorner.x);
		c.y = 0.5 * (minCorner.y + maxCorner.y);
		c.z = 0.5 * (minCorner.z + maxCorner.z);
		return c;
	}

	double radius() const
	{
		return sqrt((maxCorner.x - minCorner.x)*(maxCorner.x - minCorner.x) +
			(maxCorner.y - minCorner.y)*(maxCorner.y - minCorner.y) +
			(maxCorner.z - minCorner.z)*(maxCorner.z - minCorner.z));
	}

	void drawSelf() const
	{
		glColor3f(1.0, 0.0, 0.0);
		glBegin(GL_LINE_LOOP);
		glVertex3d((GLdouble)minCorner.x, (GLdouble)minCorner.y, (GLdouble)minCorner.z);
		glVertex3d((GLdouble)minCorner.x, (GLdouble)minCorner.y, (GLdouble)maxCorner.z);
		glVertex3d((GLdouble)minCorner.x, (GLdouble)maxCorner.y, (GLdouble)maxCorner.z);
		glVertex3d((GLdouble)minCorner.x, (GLdouble)maxCorner.y, (GLdouble)minCorner.z);
		glEnd();

		glBegin(GL_LINE_LOOP);
		glVertex3d((GLdouble)maxCorner.x, (GLdouble)maxCorner.y, (GLdouble)maxCorner.z);
		glVertex3d((GLdouble)maxCorner.x, (GLdouble)minCorner.y, (GLdouble)maxCorner.z);
		glVertex3d((GLdouble)maxCorner.x, (GLdouble)minCorner.y, (GLdouble)minCorner.z);
		glVertex3d((GLdouble)maxCorner.x, (GLdouble)maxCorner.y, (GLdouble)minCorner.z);
		glEnd();

		glBegin(GL_LINES);
		glVertex3d((GLdouble)minCorner.x, (GLdouble)minCorner.y, (GLdouble)minCorner.z);
		glVertex3d((GLdouble)maxCorner.x, (GLdouble)minCorner.y, (GLdouble)minCorner.z);

		glVertex3d((GLdouble)minCorner.x, (GLdouble)minCorner.y, (GLdouble)maxCorner.z);
		glVertex3d((GLdouble)maxCorner.x, (GLdouble)minCorner.y, (GLdouble)maxCorner.z);

		glVertex3d((GLdouble)minCorner.x, (GLdouble)maxCorner.y, (GLdouble)maxCorner.z);
		glVertex3d((GLdouble)maxCorner.x, (GLdouble)maxCorner.y, (GLdouble)maxCorner.z);

		glVertex3d((GLdouble)minCorner.x, (GLdouble)maxCorner.y, (GLdouble)minCorner.z);
		glVertex3d((GLdouble)maxCorner.x, (GLdouble)maxCorner.y, (GLdouble)minCorner.z);
		glEnd();
	}

	Point minCorner;
	Point maxCorner;
};


#endif