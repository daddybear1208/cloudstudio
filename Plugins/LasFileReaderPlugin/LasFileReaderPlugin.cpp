#include "LasFileReaderPlugin.h"
#include <iostream>
#include <liblas/liblas.hpp>
#include <CS_CORE/csPointCloud.h>


CSLasFileReaderPlugin::CSLasFileReaderPlugin()
{
	author_ = "Hongxiong Li";
	description_ = "Las point cloud reader plugin";
	detailed_description_ = "A las point cloud reader";
	version_ = CSVersion(1, 0, 0);
}

bool CSLasFileReaderPlugin::readFile(CSHierarchicalObject* object, const QString& filename)
{
	CSPointCloud* pointCloud = dynamic_cast<CSPointCloud*>(object);
	if (!pointCloud)
	{
		error_ = "cannot read file into a non-pointcloud object";
		return false;
	}
	std::ifstream ifs;
	ifs.open(filename.toStdString().c_str(), std::ios::in | std::ios::binary);
	if (!ifs) 
	{
		error_ = "cannot open file: '" + filename + "' for reading";
		return false;
	}
	liblas::ReaderFactory f;
	liblas::Reader reader = f.CreateWithStream(ifs);
	liblas::Header const& header = reader.GetHeader();
	
	int pointCount = header.GetPointRecordsCount();
	if (!pointCloud->reservePoints(pointCount))
	{
		pointCloud->clear();
		error_ = "not enough memory to reserve points data array";
		return false;
	}

	int progress = 0;
	int pointRead = 0;
	float progressStep = (float)pointCount / 100;
	while (reader.ReadNextPoint())
	{
		liblas::Point const& p = reader.GetPoint();
		pointCloud->addPoint(p.GetX(), p.GetY(), p.GetZ());
		if (progress == (int)(++pointRead / progressStep))
			continue;
		emit progressChanged(++progress);
	}
	ifs.close();
	pointCloud->setValidity(true);
	return true;

}

bool CSLasFileReaderPlugin::writeFile(CSHierarchicalObject* object, const QString& filename)
{
	return true;
}

CS_FILE_TYPES CSLasFileReaderPlugin::fileType() const
{
	return LAS;
}



QT_BEGIN_NAMESPACE
Q_EXPORT_PLUGIN2(LasFileReaderPlugin, CSLasFileReaderPlugin)
QT_END_NAMESPACE



