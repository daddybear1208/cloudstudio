#ifndef LASPCREADERPLUGIN_H
#define LASPCREADERPLUGIN_H

#include <stdlib.h>
#include <CS_CORE/csFileReaderInterface.h>

class CSLasFileReaderPlugin :public CSFileReaderInterface
{
	Q_OBJECT
	Q_INTERFACES(CSFileReaderInterface)

public:
	CSLasFileReaderPlugin();
	virtual bool readFile(CSHierarchicalObject* object, const QString& filename);
	virtual bool writeFile(CSHierarchicalObject* object, const QString& filename);
	virtual CS_FILE_TYPES fileType() const;

private:
	Q_DISABLE_COPY(CSLasFileReaderPlugin)
};

#endif