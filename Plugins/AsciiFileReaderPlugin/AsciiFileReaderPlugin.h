#ifndef ASCIIFILEREADERPLUGIN_H
#define ASCIIFILEREADERPLUGIN_H
#include <stdlib.h>
#include <CS_CORE/csFileReaderInterface.h>

class CSAsciiFileReaderPlugin :public CSFileReaderInterface
{
	Q_OBJECT
	Q_INTERFACES(CSFileReaderInterface)

public:
	CSAsciiFileReaderPlugin();
	virtual bool readFile(CSHierarchicalObject* object, const QString& filename);
	virtual bool writeFile(CSHierarchicalObject* object, const QString& filename);
	virtual CS_FILE_TYPES fileType() const;

private:
	Q_DISABLE_COPY(CSAsciiFileReaderPlugin)
};

#endif