#include "AsciiFileReaderPlugin.h"
#include <boost/iostreams/device/mapped_file.hpp>
#include <QProgressDialog>
#include <CS_CORE/csPointCloud.h>




QT_BEGIN_NAMESPACE
Q_EXPORT_PLUGIN2(AsciiFileReaderPlugin, CSAsciiFileReaderPlugin)
QT_END_NAMESPACE



CSAsciiFileReaderPlugin::CSAsciiFileReaderPlugin()
{
	author_ = "Hongxiong Li";
	description_ = "Ascii point cloud reader plugin";
	detailed_description_ = "An ascii point cloud reader, support coordinates and color, line separator could be space, comma or semicolon.";
	version_ = CSVersion(1,0,0);
}

bool CSAsciiFileReaderPlugin::readFile(CSHierarchicalObject* object, const QString& filename)
{
	CSPointCloud* pointCloud = dynamic_cast<CSPointCloud*>(object);
	if (!pointCloud)
	{
		error_ = "cannot read file into a non-pointcloud object";
		return false;
	}
	// calculate row number of ascii file
	boost::iostreams::mapped_file mmap(filename.toStdString(), boost::iostreams::mapped_file::readonly);
	const char* f = mmap.const_data();
	const char* l = f + mmap.size();

	int numLines = 0;
	while (f&&f != l)
	{
		if ((f = static_cast<const char*>(memchr(f, '\n', l - f))))
		{
			++numLines; ++f;
		}
	}

	if (numLines == 0)
	{
		pointCloud->setValidity(true);
		return true;
	}

	if (!pointCloud->reservePoints(numLines))
	{
		pointCloud->clear();
		error_ = "not enough memory to reserve points data array";
		return false;
	}

	QFile fi(filename);
	if (!fi.open(QIODevice::ReadOnly)) 
	{
		error_ = "cannot open file: '" + filename + "' for reading";
		pointCloud->clear();
		return false;
	}


	bool loadNormal = false;
	bool loadColor = false;

	QTextStream stream(&fi);
	QString firstLine = stream.readLine();
	QChar sep(' ');
	bool sepDecide = false;
	QList<QChar> separators;
	separators << QChar(' ') << QChar(',') << QChar(';') << QChar('\t');
	foreach(QChar separator, separators)
	{
		QStringList subs = firstLine.split(separator, QString::SkipEmptyParts);
		int subSize = subs.size();
		if (subSize > 2)
		{
			sep = separator;
			sepDecide = true;
			if (subSize == 6)
			{
				loadColor = true;
			}
			break;
		}

	}

	if (!sepDecide)
	{
		pointCloud->clear();
		error_ = "unknown separator in file: '" + filename + "'";
		return false;
	}

	if (loadColor)
	{
		if (!pointCloud->setupColorField())
		{
			loadColor = false;
		}
	}

	stream.seek(0);
	int pointRead = 0;
	float progressStep = (float)numLines / 100;
	int progress = 0;
	if (loadColor)
	{
		QString line = stream.readLine();
		while (!line.isNull())
		{
			QStringList pts = line.split(sep);
			if (pts.size() < 3)
				continue;
			pointCloud->addPoint(pts[0].toDouble(), pts[1].toDouble(), pts[2].toDouble());
			pointCloud->addColorField(uchar(pts[3].toUInt()), uchar(pts[4].toUInt()), uchar(pts[5].toUInt()));
			line = stream.readLine();
			if (progress == (int)(++pointRead / progressStep))
				continue;
			emit progressChanged(++progress);

		}
	}
	else
	{
		QString line = stream.readLine();
		while (!line.isNull())
		{
			QStringList pts = line.split(sep);
			if (pts.size() < 3)
				continue;
			pointCloud->addPoint(pts[0].toDouble(), pts[1].toDouble(), pts[2].toDouble());
			line = stream.readLine();
			if (progress == (int)(++pointRead / progressStep))
				continue;
			emit progressChanged(++progress);

		}
	}

	fi.close();
	pointCloud->setValidity(true);
	return true;
}

bool CSAsciiFileReaderPlugin::writeFile(CSHierarchicalObject* object, const QString& filename)
{
	return true;
}

CS_FILE_TYPES CSAsciiFileReaderPlugin::fileType() const
{
	return ASCII;
}
