#ifndef FAKEALGORITHMPLUGIN_H
#define FAKEALGORITHMPLUGIN_H

#include <CS_CORE/csAlgorithmInterface.h>

class CSFakeAlgorithmPlugin:public CSAlgorithmInterface
{
	Q_OBJECT
	Q_INTERFACES(CSAlgorithmInterface)

public:
	CSFakeAlgorithmPlugin();
	~CSFakeAlgorithmPlugin() {}

	virtual void compute();

private:
	Q_DISABLE_COPY(CSFakeAlgorithmPlugin);

};


#endif