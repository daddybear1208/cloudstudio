#include "FakeAlgorithmPlugin.h"
#include <iostream>
using namespace std;

#include <csLogger.h>
#include <CS_CORE/csGroupObject.h>


QT_BEGIN_NAMESPACE
Q_EXPORT_PLUGIN2(FakeAlgorithmPlugin, CSFakeAlgorithmPlugin)
QT_END_NAMESPACE


CSFakeAlgorithmPlugin::CSFakeAlgorithmPlugin()
{
	author_ = "Hongxiong Li";
	description_ = "Fake algorithm plugin";
	detailed_description_ = "A fake algorithm plugin to test CloudStudio algorithm plugin framework";
	version_ = CSVersion(1, 0, 0);

	m_action->setText("FakeAlg");
	m_action->setToolTip(description_);
	addParameter("Para_int", QVariant(0));
	addParameter("Para_bool", QVariant(false));
	addParameter("Para_float", QVariant(100.20));
}


void CSFakeAlgorithmPlugin::compute()
{

	CSLogHandler::instance()
		->reportInfo(QString("Fake algorithm plugin computing with parameters: var0=%1 var1=%2 var2=%3")
		.arg(parameterValue("var0").toInt()).arg(parameterValue("var1").toBool()).arg(parameterValue("var2").toDouble()));
	if (m_input.size())
	{
		CSLogHandler::instance()
			->reportInfo(QString("Input object: %1").arg(m_input[0]->name()));
	}
	m_output = new CSGroupObject("output group");
	
	emit computeFinished(true);
}


