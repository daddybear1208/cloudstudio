#include "OctreeBuilderPlugin.h"
#include <iostream>
using namespace std;

#include <csLogger.h>
#include <CS_CORE/csGroupObject.h>
#include <CS_CORE/csOctreePointCloud.h>


QT_BEGIN_NAMESPACE
Q_EXPORT_PLUGIN2(OctreeBuilderPlugin, CSOctreeBuilderPlugin)
QT_END_NAMESPACE


CSOctreeBuilderPlugin::CSOctreeBuilderPlugin()
{
	author_ = "Hongxiong Li";
	description_ = "An octree builder plugin";
	detailed_description_ = "A plugin to construct octree";
	version_ = CSVersion(1, 0, 0);

	m_action->setText("Octree Builder");
	m_action->setToolTip(description_);
	m_action->setIcon(QIcon(":/res/octree.png"));

	addParameter("Max Leaf", QVariant(1000));
	addParameter("Depth limited", QVariant(false));
	addParameter("Max depth", QVariant(10));
}


void CSOctreeBuilderPlugin::compute()
{
	if (!m_input.size())
	{
		//CSLogHandler::instance()->reportError("Octree builder plugin: Cannot working without input point cloud");
		m_error = "no input point cloud";
		emit computeFinished(false);
		return;
	}
	//else if (m_input.size()>1)
	//{
	//	CSLogHandler::instance()->reportWarning("Octree builder plugin: too much input point cloud");
	//}
	CSGroupObject* group = new CSGroupObject("Octree result");
	group->setValidity(true);
	CSOctreePointCloud* octPointCloud = 0;
	for (int i(0); i < m_input.size(); ++i)
	{
		CSPointCloud* inputPointCloud = dynamic_cast<CSPointCloud*>(m_input[i]);
		if (!inputPointCloud)
		{
			m_error = "convert input object to point cloud failed";
			emit computeFinished(false);
			return;
		}

		octPointCloud = new CSOctreePointCloud;
		octPointCloud->setAssociatePointCloud(inputPointCloud);
		unsigned int maxLeaf = (unsigned int)parameterValue("Max Leaf").toInt();
		octPointCloud->setMaxLeafPointCount(maxLeaf);
		CSChunkedArray<1, unsigned>* indexArray = new CSChunkedArray < 1, unsigned > ;
		indexArray->reserve(inputPointCloud->pointsCount());
		for (int i(0); i < inputPointCloud->pointsCount(); ++i)
		{
			indexArray->addElement(i);
		}
		CSOctreeNode* root = buildOctnodeRecurse(inputPointCloud, indexArray, inputPointCloud->getBoundingBox(), maxLeaf);
		if (root)
		{
			octPointCloud->setRootNode(root);
			octPointCloud->setValidity(true);
			group->appendChild(octPointCloud);
		}
		else
		{
			delete octPointCloud;
			octPointCloud = 0;
		}
	}
	if (group->childCount())
	{
		this->m_output = group;
		emit computeFinished(true);
	}
	else
	{
		delete group;
		emit computeFinished(false);
	}

	
	//if (root)
	//{
	//	octPointCloud->setRootNode(root);
	//	this->m_output = octPointCloud;
	//	emit computeFinished(true);
	//}
	//else
	//{
	//	CSLogHandler::instance()->reportError("Octree builder plugin: build failed");
	//	emit computeFinished(false);
	//}

}

CSOctreeNode* CSOctreeBuilderPlugin::buildOctnodeRecurse(CSPointCloud* pointcloud, CSChunkedArray<1, unsigned>*& indexes, const CSBoundingBox& bbox, unsigned int maxLeafPointCount)
{
	if (indexes->elementCount() == 0)
		return 0;
	if (indexes->elementCount() <= maxLeafPointCount)
	{
		CSOctreeLeafNode* leaf = new CSOctreeLeafNode;
		leaf->pointIndexVector().reserve(indexes->elementCount());
		leaf->setBoundingBox(bbox);
		for (int i(0); i < indexes->elementCount(); ++i)
		{
			leaf->addPointIndex(indexes->value(i));
		}
		delete indexes;
		indexes = 0;
		return leaf;
	}
	else
	{
		// split into eight
		CSOctreeBranchNode* branch = new CSOctreeBranchNode();
		branch->setBoundingBox(bbox);
		CSChunkedArray<1, unsigned>* subIndexes[8];
		for (int i(0); i < 8; ++i)
		{
			subIndexes[i] = new CSChunkedArray<1, unsigned>();
			subIndexes[i]->reserve(indexes->elementCount());
		}

		CSBoundingBox* subBoundingBox[8];
		// calculate sub bounding boxes

		GLC_Point3d lowerPoint = bbox.lowerCorner();
		GLC_Point3d upperPoint = bbox.upperCorner();
		GLC_Point3d center = bbox.center();

		subBoundingBox[0] = new CSBoundingBox(lowerPoint, center);

		subBoundingBox[1] = new CSBoundingBox(center.x(), lowerPoint.y(), lowerPoint.z(), upperPoint.x(), center.y(), center.z());

		subBoundingBox[2] = new CSBoundingBox(lowerPoint.x(), center.y(), lowerPoint.z(), center.x(), upperPoint.y(), center.z());

		subBoundingBox[3] = new CSBoundingBox(center.x(), center.y(), lowerPoint.z(), upperPoint.x(), upperPoint.y(), center.z());

		subBoundingBox[4] = new CSBoundingBox(lowerPoint.x(), lowerPoint.y(), center.z(), center.x(), center.y(), upperPoint.z());

		subBoundingBox[5] = new CSBoundingBox(center.x(), lowerPoint.y(), center.z(), upperPoint.x(), center.y(), upperPoint.z());

		subBoundingBox[6] = new CSBoundingBox(lowerPoint.x(), center.y(), center.z(), center.x(), upperPoint.y(), upperPoint.z());

		subBoundingBox[7] = new CSBoundingBox(center, upperPoint);

		for (int i(0); i < indexes->elementCount(); ++i)
		{
			uchar bboxIndex = 0;
			double* data = pointcloud->getPointData(indexes->value(i));

			if (*data > center.x())
			{
				bboxIndex |= 1;
			}
			if (*(data + 1) > center.y())
			{
				bboxIndex |= 2;
			}
			if (*(data + 2) > center.z())
			{
				bboxIndex |= 4;
			}
			subIndexes[bboxIndex]->addElement(indexes->value(i));
		}

		for (int i(0); i < 8; ++i)
		{
			branch->setChild(i, buildOctnodeRecurse(pointcloud, subIndexes[i], *subBoundingBox[i], maxLeafPointCount));
			if (subIndexes[i])
			{
				delete subIndexes[i];
				subIndexes[i] = 0;
			}

		}
		return branch;
	}
}





