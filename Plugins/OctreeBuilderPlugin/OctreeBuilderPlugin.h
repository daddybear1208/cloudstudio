#ifndef FAKEALGORITHMPLUGIN_H
#define FAKEALGORITHMPLUGIN_H

#include <CS_CORE/csAlgorithmInterface.h>
#include <CS_CORE/csOctreeNode.h>
#include <CS_CORE/csOctreePointCloud.h>

//typedef CSChunkedArray<1,unsigned> IndexArray;

class CSOctreeBuilderPlugin:public CSAlgorithmInterface
{
	Q_OBJECT
	Q_INTERFACES(CSAlgorithmInterface)

public:
	CSOctreeBuilderPlugin();
	~CSOctreeBuilderPlugin() {}

	virtual void compute();
	CSOctreeNode* buildOctnodeRecurse(CSPointCloud* pointcloud, CSChunkedArray<1, unsigned>* & indexes, const CSBoundingBox& bbox, unsigned int maxLeafPointCount);

private:
	Q_DISABLE_COPY(CSOctreeBuilderPlugin);
};


#endif